<section class="content">




    <div class="alert alert-danger" style="font-size: 14px;">
        RECUERDE QUE SI EL SRI LE NOTIFICO QUE DEBE CONSTAR EN SUS DOCUMENTOS ELECTRONICOS LAS LEYENDAS REGIMEN
        MICROEMPRESAS O AGENTE DE RETENCION POR RESOLUCION # XXX DEBE SOLICITARLO A SOPORTE TECNICO 0939286419 .
        <br>
        ESTA INFORMACION DEBE ESTAR EN TODOS LOS DOCUMENTOS ELECTRONICOS EN LA PARTE INFERIOR EN DATOS ADICIONALES
        HASTA QUE EL SRI ACTUALICE SUS SISTEMAS Y MANUAL TECNICO Y CREE UN CAMPO ESPECIAL DESIGNADO PARA ESTO.

    </div>



    <nav class="navbar navbar-default navbarmio">
        <div class="">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs" href="#">Filtros de Búsqueda</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form class="">
                    <input type="hidden" name="global" value="">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Búsqueda</span>
                                <input class="form-control mr-sm-2" name="busqueda" type="search" placeholder="Ingrese su Búsqueda" aria-label="Search" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Fecha Inicio</span>
                                <input class="form-control" type="text" name="fecha_inicio" id="fechainicio" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Fecha Fin</span>
                                <input type="text" class="form-control" name="fecha_fin" id="fechafin" value="">

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Estado</span>
                                <select class="form-control" name="estadocomprobante"><option value="0">Procesando</option><option value="1">Generado</option><option value="2">Firmado</option><option value="3">Recibido</option><option value="4">Autorizado</option><option value="5">No Autorizado</option><option value="6">Devuelto</option><option value="7">Error en Firmado</option><option value="8">Pendiente Autorización</option><option value="9">Anulado</option><option selected="true" value="99">Todos</option><option value="999">Eliminados</option></select>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Ordenar Por</span>
                                <select class="form-control" name="ordenar_por"><option value="all">Defecto</option><option value="fecha">Fecha</option><option value="secuencial">Secuencial</option></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Orden</span>
                                <select class="form-control" name="orden"><option value="asc">Ascendente</option><option value="desc">Descendente</option></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Ver</span>
                                <select class="form-control" name="paginacion"><option selected="true" value="0">Defecto</option><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option><option value="all">Todos</option></select>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class=" col-md-4">
                            <button class="btn bg-maroon btn-flat btn-block" name="accion" value="filtrar" type="submit">
                                Aplicar Filtros
                            </button>
                        </div>

                        <div class=" col-md-4">
                            <button class="btn bg-olive btn-flat btn-block" name="accion" value="excel" type="submit">
                                Descargar en Excel
                            </button>
                        </div>
                        <div class=" col-md-4">
                            <button class="btn bg-purple btn-flat btn-block" name="accion" value="exceldetalle" type="submit">
                                Descargar en Excel Detallado
                            </button>
                        </div>
                    </div>
                </form>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <!-- /.box -->

    <div class="box box-primary">

        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover ">
                    <thead class="thead-dark">
                    <tr>
                        <th><i class="icmn-key2 opc" aria-hidden="true"></i></th>
                        <th>Fecha Emisión</th>
                        <th>Estab-PtoEmi</th>
                        <th>Secuencial</th>
                        <th>Cliente</th>
                        <th>Ambiente</th>
                        <th>Vendedor</th>
                        <th>Subtotal</th>
                        <th>ICE</th>
                        <th>IVA</th>
                        <th>Total</th>
                        <th>Estado</th>

                        <th>Pago</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody><tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5395" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-03-07</td>
                        <td>001-002</td>
                        <td>000000821



                        </td>
                        <td>AGENCIA DE VIAJES G-1 C....</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                211.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">25.320</th>
                        <th class="estilovalor">236.32</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pagado
                        </td>


                        <td>
                            <a onclick="verpagocxc('5395','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5395" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5395" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5395" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5394" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-03-06</td>
                        <td>001-002</td>
                        <td>000000820



                        </td>
                        <td>CONSUMIDOR FINAL</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                3.780
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.450</th>
                        <th class="estilovalor">4.23</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pagado
                        </td>


                        <td>
                            <a onclick="verpagocxc('5394','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5394" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5394" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5394" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5392" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-03-04</td>
                        <td>001-002</td>
                        <td>000000819



                        </td>
                        <td>ISABEL EVELYN ZAMBRANO SA...</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                12905.360
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">1548.640</th>
                        <th class="estilovalor">14454.00</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5392','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5392" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5392" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5392" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5388" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-03-02</td>
                        <td>001-002</td>
                        <td>000000818



                        </td>
                        <td>CONSUMIDOR FINAL</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                14.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">1.680</th>
                        <th class="estilovalor">15.68</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5388','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5388" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5388" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5388" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5335" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-25</td>
                        <td>001-002</td>
                        <td>000000812



                        </td>
                        <td>HANWA CO. LTD</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                7000.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">7000.00</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5335','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5335" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5335" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5335" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5336" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-25</td>
                        <td>001-002</td>
                        <td>000000813



                        </td>
                        <td>SALAZAR ABRIL PEDRO WILLI...</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                3.125
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.380</th>
                        <th class="estilovalor">3.51</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5336','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5336" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5336" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5336" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5338" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-25</td>
                        <td>001-002</td>
                        <td>000000814



                        </td>
                        <td>DARWIN REINOSO</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                1.890
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.230</th>
                        <th class="estilovalor">2.12</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5338','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5338" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5338" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5338" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5339" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-25</td>
                        <td>001-002</td>
                        <td>000000815



                        </td>
                        <td>DARWIN REINOSO</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                0.750
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.75</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5339','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5339" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5339" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5339" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr class="tr_error">
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5340" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-25</td>
                        <td>001-002</td>
                        <td>000000816



                        </td>
                        <td>DARWIN REINOSO</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                70.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">8.400</th>
                        <th class="estilovalor">78.40</th>
                        <td>

                            Devuelto
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5340','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>





                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5340" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5340" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5341" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-25</td>
                        <td>001-002</td>
                        <td>000000817



                        </td>
                        <td>DARWIN REINOSO</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                160.720
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">19.290</th>
                        <th class="estilovalor">180.01</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5341','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5341" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5341" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5341" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr class="tr_error">
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5330" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-24</td>
                        <td>001-002</td>
                        <td>000000810



                        </td>
                        <td>MARIA JOSE CARRILLO CABRE...</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                10.100
                            </div>
                        </th>

                        <th class="estilovalor">0.505</th>
                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">10.61</th>
                        <td>

                            No Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5330','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>





                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5330" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5330" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5331" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-24</td>
                        <td>001-002</td>
                        <td>000000811



                        </td>
                        <td>TEST</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                1.790
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.210</th>
                        <th class="estilovalor">2.00</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5331','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5331" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5331" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5331" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5315" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-22</td>
                        <td>001-002</td>
                        <td>000000809



                        </td>
                        <td>ANDRES GARCIA</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                98.220
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">11.790</th>
                        <th class="estilovalor">110.01</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5315','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5315" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5315" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5315" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5314" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-20</td>
                        <td>001-002</td>
                        <td>000000808



                        </td>
                        <td>CONSUMIDOR FINAL</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                2.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.240</th>
                        <th class="estilovalor">2.24</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5314','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5314" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5314" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5314" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5309" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-15</td>
                        <td>001-002</td>
                        <td>000000807



                        </td>
                        <td>AGENCIA DE VIAJES ROSETUR...</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                4.464
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.540</th>
                        <th class="estilovalor">5.00</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pagado
                        </td>


                        <td>
                            <a onclick="verpagocxc('5309','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5309" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5309" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5309" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5288" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-08</td>
                        <td>001-002</td>
                        <td>000000805



                        </td>
                        <td>AGENCIA DE VIAJES G-1 C....</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                1.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.120</th>
                        <th class="estilovalor">1.12</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5288','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5288" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5288" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5288" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5289" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-08</td>
                        <td>001-002</td>
                        <td>000000806



                        </td>
                        <td>MARTIN QUINTERO</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                10.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">1.200</th>
                        <th class="estilovalor">11.20</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5289','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5289" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5289" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5289" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5271" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-02</td>
                        <td>001-002</td>
                        <td>000000804



                        </td>
                        <td>MARIELA MORA</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                1.990
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">0.240</th>
                        <th class="estilovalor">2.23</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5271','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5271" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5271" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5271" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5237" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-01</td>
                        <td>001-002</td>
                        <td>000000802



                        </td>
                        <td>MARTIN QUINTERO</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                10.000
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">1.200</th>
                        <th class="estilovalor">11.20</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pendiente
                        </td>


                        <td>
                            <a onclick="verpagocxc('5237','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5237" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5237" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5237" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><i data-toggle="tooltip" data-placement="right" data-original-title="5238" class="icmn-key2 opc" aria-hidden="true"></i></td>
                        <td>2021-02-01</td>
                        <td>001-002</td>
                        <td>000000803



                        </td>
                        <td>GEORGE BRYAN COBEÑA ZAMBR...</td>
                        <td>PRUEBAS</td>
                        <td>Demo</td>
                        <th class="estilovalor">
                            <div data-toggle="tooltip" data-html="true" title="" data-original-title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                9.800
                            </div>
                        </th>

                        <th class="estilovalor">0.000</th>
                        <th class="estilovalor">1.180</th>
                        <th class="estilovalor">10.98</th>
                        <td>

                            Autorizado
                        </td>

                        <td>
                            Pagado
                        </td>


                        <td>
                            <a onclick="verpagocxc('5238','01');">
                                <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                            </a>




                            <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5238" data-toggle="tooltip" data-placement="top" data-original-title="Crear Guia">
                                <i class="fa fa-truck opc" aria-hidden="true"></i>
                            </a>

                            <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5238" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                <i class="icmn-printer2 opc" aria-hidden="true"></i>
                            </a>
                            <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5238" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                <i class="icmn-printer opc" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>

                    </tbody></table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <!-- Previous Page Link -->
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">«</a>
                    </li>

                    <!-- Pagination Elements -->
                    <!-- "Three Dots" Separator -->

                    <!-- Array Of Links -->
                    <li class="page-item active">
                        <a class="page-link" href="#">
                            <span class="">1</span>
                        </a>
                    </li>
                    <li><a class="page-link" href="https://azur.com.ec/plataforma/factura/vertodos?page=2">2</a></li>

                    <!-- Next Page Link -->
                    <li><a class="page-link" href="https://azur.com.ec/plataforma/factura/vertodos?page=2" rel="next">»</a></li>
                </ul>
            </nav>
        </div>
        <!-- box-footer -->
    </div>
    <!-- /.box -->

    <div class="modal fade in" id="modal-pagos">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">
                        Pagos
                        <span id="id_texto_pago"></span>

                        <button onclick="nuevopago()" type="button" data-toggle="tooltip" data-original-title="Nuevo Pago" aria-hidden="true" class="btn btn-primary ">
                            <i class="fa fa-money "></i> &nbsp;Nuevo Pago
                        </button>

                        <button onclick="nuevaretencionrecibida()" type="button" data-toggle="tooltip" data-original-title="Nueva Retención Recibida" aria-hidden="true" class="btn btn-primary ">
                            <i class="fa fa-money "></i> &nbsp;Nueva Retención Recibida
                        </button>
                    </h4>

                </div>
                <div class="modal-body " id="animacionpagos">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover ">
                            <thead class="thead-dark">
                            <tr><td>Id</td>
                                <td>Fecha</td>
                                <td>Fecha Vencimiento</td>
                                <td>Concepto</td>
                                <td>Débito</td>
                                <td>Crédito</td>
                                <td>X</td>
                            </tr></thead>
                            <tbody id="id_tabla_pagos">

                            </tbody>
                            <tfoot id="id_tabla_pagosfooter">

                            </tfoot>
                        </table>
                    </div>

                    <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade in" id="modal-nuevopago">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">
                        Nuevo Pago
                    </h4>

                </div>
                <div class="modal-body " id="animacionpagos">
                    <form id="formulariopago" enctype="multipart/form-data">
                        <input type="hidden" id="pago_id_comprobante">
                        <input type="hidden" id="pago_tipo_comprobante">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    Fecha
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="pago_fecha" value="2021-03-26">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Concepto
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="pago_concepto">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Imagen
                                    <small>(Opcional)</small>
                                </div>
                                <div class="col-md-9">
                                    <input type="file" id="pago_archivo" name="pago_archivo" accept="image/*" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Forma de Pago
                                    <small>(Opcional)</small>
                                </div>
                                <div class="col-md-9">
                                    <select id="pago_formapago" class="form-control" name="pago_formapago">
                                        <option value="01">Sin utilizacion del sistema financiero</option>
                                        <option value="16">Tarjetas de Debito</option>
                                        <option value="17">Dinero Electronico</option>
                                        <option value="18">Tarjeta Prepago</option>
                                        <option value="19">Tarjeta de Credito</option>
                                        <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>
                                        <option value="21">Endoso de Titulos</option>
                                        <option value="15">COMPENSACIÓN DE DEUDAS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Valor
                                </div>
                                <div class="col-md-9">
                                    <input type="text" id="pago_valor" class="form-control validador_numero2 usd2">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Observacion
                                    <small>(Opcional)</small>
                                </div>
                                <div class="col-md-9">
                                    <textarea type="text" class="form-control" id="pago_observacion"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>


                    <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                    <button onclick="guardarpagocxc()" type="button" data-toggle="tooltip" data-original-title="Guardar Pago" aria-hidden="true" class="btn btn-primary pull-right">
                        <i class="fa fa-money "></i> &nbsp;Guardar
                    </button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->


    <div class="modal fade in" id="modal-nuevoretencionrecibida">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">
                        Nueva Retención Recibida
                    </h4>

                </div>
                <div class="modal-body " id="animacionretencionrecibida">
                    <form id="formularioretencionrecibida" enctype="multipart/form-data">
                        <input type="hidden" id="retencion_id_comprobante">
                        <input type="hidden" id="retencion_tipo_comprobante">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    Fecha
                                </div>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="retencion_fecha" value="2021-03-26">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Tipo Impuesto
                                </div>
                                <div class="col-md-9">
                                    <select name="retencion_tipo" id="retencion_tipo" class="form-control width-middle">
                                        <option selected="true" value="2">IVA</option>
                                        <option value="1">RENTA</option>
                                        <option value="6">ISD</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="animacionrenta">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Codigo Retencion
                                    </div>
                                    <div class="col-md-9">
                                        <select id="codigoretencion" name="codigoretencion" class="form-control">


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="animacioncodigo">
                                <div class="row">
                                    <div class="col-md-9">
                                        Porcentaje
                                    </div>
                                    <div class="col-md-3">
                                        <div id="porcentajediv">
                                            <div class="input-group">
                                                <input type="number" class="form-control usd5 " id="porcentaje" name="porcentaje" value="0">
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Base Imponible
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" id="retencion_base" class="form-control validador_numero2 usd2">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Valor
                                            <button type="button" class="btn-primary btn-sm" onclick="calcularvaloretenido()"><i class="fa fa-fw fa-refresh"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" id="retencion_valor" class="form-control validador_numero2 usd2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Nro Comprobante Retención
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="retencion_secuencial" placeholder="___-___-_________">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    Observacion
                                    <small>(Opcional)</small>
                                </div>
                                <div class="col-md-9">
                                    <textarea type="text" class="form-control" id="retencion_observacion"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>

                    <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                    <button onclick="guardarretencionrecibidacxc()" type="button" data-toggle="tooltip" data-original-title="Guardar Retención Recibida" aria-hidden="true" class="btn btn-primary pull-right">
                        <i class="fa fa-money "></i> &nbsp;Guardar
                    </button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>


    <div class="modal fade in" id="modal-fotopago">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">
                        Foto
                    </h4>

                </div>
                <div class="modal-body ">
                    <img id="cargafoto" src="" style="width: 100%; height: 100%" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-dialog -->

    <script>


        var retencionbaseiva = "";
        var retencionbaserenta = "";

        $('#retencion_tipo').on('change', function () {

            if (this.value == 2) {
                $("#retencion_base").val(retencionbaseiva);
            } else if (this.value == 1) {
                $("#retencion_base").val(retencionbaserenta);
            } else {
                $("#retencion_base").val("");
            }

        });


        function guardarretencionrecibidacxc() {
            var id_comprobante = $("#pago_id_comprobante").val();
            obtenercodigo();
            swal({
                    title: "Esta Seguro que desea Retención Recibida",
                    text: "Guardar Retención Recibida",
                    type: "info",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {
                    $.ajax({
                        type: 'get',
                        url: "https://azur.com.ec/plataforma/guardarretencionrecibidacxc",
                        data: {
                            id_comprobante: $("#pago_id_comprobante").val(),
                            tipo_comprobante: $("#pago_tipo_comprobante").val(),
                            retencion_fecha: $("#retencion_fecha").val(),
                            retencion_tipo: $("#retencion_tipo").val(),
                            retencion_base: $("#retencion_base").val(),
                            retencion_valor: $("#retencion_valor").val(),
                            retencion_observacion: $("#retencion_observacion").val(),
                            retencion_secuencial: $("#retencion_secuencial").val(),
                            retencion_porcentaje: $("#porcentaje").val(),
                            retencion_codigoretencion: $("#codigoretencion").attr("codigo"),
                            api_key2: "API_1_2_5a4492f2d5137"
                        },
                        success: function (resp) {

                            if (resp.respuesta == true) {
                                swal({
                                    title: "Excelente!",
                                    text: "Guardado",
                                    type: "success",
                                    showConfirmButton: false,
                                    timer: 1000
                                });
                                $('#modal-nuevoretencionrecibida').modal('hide');
                                verpagocxc(resp.id_comprobante, resp.tipo_comprobante);

                            } else {
                                swal({
                                    title: "ERROR!",
                                    text: "Error en el proceso Vuelva a intentarlo",
                                    type: "error",
                                    showConfirmButton: false,
                                    timer: 1000
                                });
                            }
                        }, error: function (xhr) {
                            erroresenajax(xhr);
                        },
                    });
                });

        }


        $(function () {
            $('#retencion_secuencial').mask("000-000-000000000", {placeholder: "___-___-_________"});
        })

        function nuevaretencionrecibida() {
            $('#modal-nuevoretencionrecibida').modal('show');
            $("#retencion_base").val(retencionbaseiva);
            $("#retencion_valor").val("");
            $("#retencion_observacion").val("");
            codigosretencion();
        }

        function guardarpagocxc() {
            var id_comprobante = $("#pago_id_comprobante").val();
            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('foto', $('#pago_archivo')[0].files[0]);
            paqueteDeDatos.append('pago_archivo', $('#pago_archivo')[0].files[0]);
            paqueteDeDatos.append('id_comprobante', $("#pago_id_comprobante").val());
            paqueteDeDatos.append('tipo_comprobante', $("#pago_tipo_comprobante").val());
            paqueteDeDatos.append('pago_fecha', $("#pago_fecha").val());
            paqueteDeDatos.append('pago_concepto', $("#pago_concepto").val());
            paqueteDeDatos.append('pago_formapago', $("#pago_formapago").val());
            paqueteDeDatos.append('pago_valor', $("#pago_valor").val());
            paqueteDeDatos.append('pago_observacion', $("#pago_observacion").val());
            paqueteDeDatos.append('api_key2', "API_1_2_5a4492f2d5137");

            swal({
                    title: "Esta Seguro que desea Guardar el Pago",
                    text: "Guardar Pago",
                    type: "info",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {

                    $.ajax({
                        type: 'POST',
                        url: "https://azur.com.ec/plataforma/guardarpagosdecxc",
                        contentType: false,
                        processData: false,
                        cache: false,
                        data: paqueteDeDatos,
                        success: function (resp) {

                            if (resp.respuesta == true) {
                                swal({
                                    title: "Excelente!",
                                    text: "Guardado",
                                    type: "success",
                                    showConfirmButton: false,
                                    timer: 1000
                                });
                                $('#modal-nuevopago').modal('hide');
                                verpagocxc(resp.id_comprobante, resp.tipo_comprobante);

                            } else {
                                swal({
                                    title: "ERROR!",
                                    text: "Error en el proceso Vuelva a intentarlo",
                                    type: "error",
                                    showConfirmButton: false,
                                    timer: 1000
                                });
                            }
                        }, error: function (xhr) {
                            erroresenajax(xhr);
                        },
                    });
                });


        }

        function nuevopago() {
            $('#modal-nuevopago').modal('show');
        }

        function verpagocxc(id_comprobante, tipo_comprobante) {

            retencionbaseiva = 0;
            retencionbaserenta = 0;
            $('#id_tabla_pagos').empty();
            $('#pago_id_comprobante').val("");
            $('#pago_tipo_comprobante').val("");
            $('#retencion_id_comprobante').val("");
            $('#retencion_tipo_comprobante').val("");
            $('#modal-pagos').modal('show');
            $("#retencion_valor").val("");
            $("#retencion_observacion").val("");
            $("#retencion_secuencial").val("");
            $("#animacionpagos").LoadingOverlay("show");
            $('#pago_id_comprobante').val(id_comprobante);
            $('#retencion_id_comprobante').val(id_comprobante);
            $('#pago_tipo_comprobante').val(tipo_comprobante);
            $('#retencion_tipo_comprobante').val(tipo_comprobante);

            var auxtexto = "";
            if (tipo_comprobante == "01" || tipo_comprobante == "1") {
                auxtexto = "Factura";
            } else if (tipo_comprobante == "02" || tipo_comprobante == "2") {
                auxtexto = "Nota de Venta";
            } else if (tipo_comprobante == "05" || tipo_comprobante == "5") {
                auxtexto = "Nota de Debito";
            } else {
                auxtexto = "otro";
            }

            $.ajax({
                type: 'POST',
                url: "https://azur.com.ec/plataforma/pagosdecomprobantecxc",
                data: {
                    id_comprobante: id_comprobante,
                    tipo_comprobante: tipo_comprobante,
                    api_key2: "API_1_2_5a4492f2d5137"
                },
                success: function (resp) {
                    if (resp.respuesta == true) {
                        $('#id_texto_pago').html(auxtexto + " # " + resp.numero);
                        $('#id_tabla_pagos').empty();
                        $('#id_tabla_pagosfooter').empty();
                        $("#pago_concepto").val("");
                        $("#pago_valor").val("");
                        $(resp.transacciones).each(function (i, v) {
                            var debito = 0;
                            var credito = 0;
                            if (v.tipo == 2) {
                                credito = v.valor;
                                debito = 0;
                            } else if (v.tipo == 1) {
                                credito = 0;
                                debito = v.valor;
                            }

                            var botoneliminar = "";

                            if (v.tipo == 2) {
                                botoneliminar = '<a id="eliminarpagocxc" id_pago="' + v.id + '"><i class="fa fa-times-circle anadir"></i></a>';
                            } else if (v.tipo == 1) {
                                botoneliminar = '';
                            }

                            var fotopago = '';

                            if (v.foto == "" || v.foto == null) {
                                var fotopago = '';
                            } else if (v.foto != "") {
                                var fotopago = '<button onclick="verfotopago(' + v.id + ')" type="button" data-toggle="tooltip" data-original-title="Ver Pago" aria-hidden="true" class="btn btn-primary ">  <i class="fa fa-image "></i> </button>';
                            }

                            if (v.observacion == "" || v.observacion == null) {
                                var observacion = '';
                            } else if (v.observacion != "") {
                                var observacion =  v.observacion;
                            }


                            $("#id_tabla_pagos").append('<tr>' +
                                '<td>' + v.id + fotopago + '</td>' +
                                '<td>' + v.fecha + '</td>' +
                                '<td>' + v.vencimiento + '</td>' +
                                '<td>' + v.concepto +'<br>'+observacion+ '</td>' +
                                '<td style="text-align: right">' + debito + '</td>' +
                                '<td style="text-align: right">' + credito + '</td>' +
                                '<td>' + botoneliminar + '</td>' +
                                '</tr>');
                        })
                        $("#id_tabla_pagosfooter").append('<tr>' +
                            '<td colspan="4"></td>' +
                            '<td><b>Saldo</b></td>' +
                            '<td style="text-align: right">' + resp.saldo + '</td>' +
                            '</tr>');

                        $("#pago_concepto").val("Pago " + auxtexto + " # " + resp.numero);
                        $("#pago_valor").val(resp.saldo);
                        retencionbaseiva = resp.baseiva;
                        retencionbaserenta = resp.baserenta;
                        $("#animacionpagos").LoadingOverlay("hide");

                    } else {
                        $("#animacionpagos").LoadingOverlay("hide");

                    }
                }, error: function (xhr) {
                    erroresenajax(xhr);
                },
            })
        }


        function eliminarpagocxc(id_pago) {
            $.ajax({
                type: 'POST',
                url: "https://azur.com.ec/plataforma/eliminarpagosdecxc",
                data: {
                    id_pago: id_pago,
                    api_key2: "API_1_2_5a4492f2d5137"
                },
                success: function (resp) {
                    if (resp.respuesta == true) {
                        if (resp.id_comprobante != "") {
                            verpagocxc(resp.id_comprobante, resp.tipo_comprobante)
                        }
                        swal({
                            title: "Excelente!",
                            text: "Pago Eliminado",
                            type: "success",
                            showConfirmButton: false,
                            timer: 1000
                        });

                    } else {
                        swal({
                            title: "ERROR!",
                            text: "Error en el proceso Vuelva a intentarlo",
                            type: "error",
                            showConfirmButton: false,
                            timer: 1000
                        });
                    }
                }, error: function (xhr) {
                    erroresenajax(xhr);
                },
            })
        }

        $(function () {
            $('#pago_fecha').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });
        })

        $('#id_tabla_pagos').on('click', '#eliminarpagocxc', function () {
            var id_pago = this.getAttribute("id_pago");
            var fila = $(this).parents('tr');
            fila.remove();
            eliminarpagocxc(id_pago);
        })


        function verfotopago(id_pago) {
            $.ajax({
                type: 'POST',
                url: "https://azur.com.ec/plataforma/verpagofotocxc",
                data: {
                    id_pago: id_pago,
                    api_key2: "API_1_2_5a4492f2d5137"
                },
                success: function (resp) {
                    if (resp.respuesta == true) {
                        $('#cargafoto').attr('src', 'data:image/jpeg;base64,' + resp.foto);
                        $('#modal-fotopago').modal('show');
                    } else {
                    }
                }
            })
        }


        $('#retencion_tipo').change(function () {

            codigosretencion();
        });

        function codigosretencion() {
            $(".animacionrenta").LoadingOverlay("show");
            var codigoimpuesto = $('#retencion_tipo').val();
            $.ajax({
                type: 'POST',
                url: "https://azur.com.ec/plataforma/impuestos",
                data: {
                    codigo: codigoimpuesto,
                    api_key2: "API_1_2_5a4492f2d5137",
                },
                success: function (resp) {
                    if (resp.respuesta == true) {
                        $('#codigoretencion').empty();
                        $(resp.datos).each(function (i, v) {
                            $('#codigoretencion').append(' <option value=' + v.id + '>' + v.codigoretencion + " - " + v.descripcion.substr(0, 100) + '</option>');
                        });
                        obtenercodigo();
                        porcentajeretencion();
                        $(".animacionrenta").LoadingOverlay("hide");
                    } else {
                        $(".animacionrenta").LoadingOverlay("hide");
                    }
                }
            })
        }

        function porcentajeretencion() {
            $(".animacioncodigo").LoadingOverlay("show");
            var codigo = $('#codigoretencion').val();

            $.ajax({
                type: 'POST',
                url: "https://azur.com.ec/plataforma/impuestosporcentaje",
                data: {
                    codigo: codigo,
                    api_key2: "API_1_2_5a4492f2d5137",
                },
                success: function (resp) {
                    $('#porcentaje').val("");
                    if (resp.respuesta == true) {
                        $('#porcentaje').val(resp.valor);
                        calcularvaloretenido();
                        $(".animacioncodigo").LoadingOverlay("hide");
                    } else {
                        $(".animacioncodigo").LoadingOverlay("hide");
                    }
                }
            })
        }

        $('#codigoretencion').change(function () {
            porcentajeretencion();
        });

        function calcularvaloretenido() {
            var baseimponible = $('#retencion_base').val();
            var porcentaje = $('#porcentaje').val();
            var total = redondear2(parseFloat(baseimponible) * (parseFloat(porcentaje) / 100));
            $("#retencion_valor").val(total);
        }


        $('#codigoretencion').change(function () {
            obtenercodigo();
        });

        function obtenercodigo() {
            $(".animacionrenta").LoadingOverlay("show");
            var id = $('#codigoretencion').val();
            ;
            $.ajax({
                type: 'POST',
                url: "https://azur.com.ec/plataforma/impuestoscodigo",
                data: {
                    id: id,
                    api_key2: "API_1_2_5a4492f2d5137",
                },
                success: function (resp) {
                    if (resp.respuesta == true) {

                        $('#codigoretencion').attr("codigo", resp.valor);
                        $(".animacionrenta").LoadingOverlay("hide");
                    } else {
                        $(".animacionrenta").LoadingOverlay("hide");
                    }
                }
            })
        }
    </script>

    <script>



        $(function () {

            $('#fechainicio').datetimepicker({
                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $('#fechafin').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

        })

    </script>
</section>
