@extends('app')
@section('content')




    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Nueva Retencion en AZUR FACTURACIÓN ELECTRÓNICA Codigo Estab. 001

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Azur Facturación Electronica </a>
            </li>
            <li class="active">Retencion</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">







        <div class="box">
            <div class="box-body">
                <div class="row">

                    <div class="col-md-2">
                        <b>Fecha</b>
                        <input type="text" class="form-control" id="fecha" value="2021-04-19">
                    </div>
                    <div class="col-md-2">
                        <b>Tipo</b>
                        <select class="form-control" name="tipo_tecnologia" disabled="" id="tipo_tecnologia"><option selected="true" value="1">Electrónica</option></select>
                    </div>
                    <div class="col-md-2">
                        <b>Punto Emisión</b>
                        <select class="form-control" disabled="true" id="punto_emision" name="punto_emision"><option value="1">003-Otros</option><option selected="true" value="16">002-Caja 2</option><option value="35">001-General</option><option value="1505">900-Caja 2</option><option value="2752">901-901</option></select>

                    </div>
                    <div class="col-md-2">
                        <b>Secuencial</b>
                        <input class="form-control validador_numeroentero" type="number" value="" id="secuencial">
                    </div>

                    <div class="col-md-2">
                        <b>Ambiente</b>
                        <select class="form-control" name="ambiente" id="ambiente"><option selected="true" value="1">Pruebas</option></select>
                    </div>
                </div>


            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Datos del Proveedor</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col col-md-3">
                        Proveedor
                        <div class="input-group">

                            <input id="criterioproveedor" name="criterioproveedor" class="form-control" type="text" value="" placeholder="Ingrese su Busqueda">
                            <span class="input-group-btn">
                            <button id="botonbuscarextraboton" type="button" class="btn btn-primary btn-flat"><i class="fa  fa-search"></i></button>
                          </span>
                        </div>

                    </div>
                    <div class="col col-md-3">
                        Seleccione un Resultado
                        <select class="form-control" id="proveedores">
                        </select>
                    </div>
                    <div class="col col-md-3">
                        Sucursal
                        <select class="form-control" id="sucursalesproveedores">
                        </select>
                    </div>
                    <div class="col col-md-3">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-sm" id="botonnuevoproveedor"><i class="fa fa-plus-square"></i> &nbsp;Nuevo
                            </button>
                            <button type="button" disabled="disabled" class="btn btn-warning btn-sm" id="botoneditarproveedor">
                                <i class="fa fa-edit"></i>&nbsp;Editar
                            </button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" id="contenedorproveedor">
                <div class="col col-md-12">
                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <input type="hidden" id="tipo_evento_proveedor" value="final">
                            <input type="hidden" id="id_proveedor" value="">
                            <input type="hidden" id="id_sucursal" value="">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Tipo Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control requerido" name="tipoidentificacion" id="tipoidentificacion" disabled="disabled">
                                    <option selected="" value="05">Cedula</option>
                                    <option value="04">Ruc</option>
                                    <option value="06">Pasaporte</option>
                                    <option value="08">Identificacion del Exterior</option>
                                    <option value="09">Placa</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label class="form-control-label " for="l0">Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <input name="identificacion" type="text" class="form-control requerido " placeholder="9999999999999" id="identificacion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Nombres/Razon Social</label>
                            </div>
                            <div class="col-md-4">
                                <input name="nombrerazonsocial" type="text" class="form-control requerido mayuscula" placeholder="Consumidor Final" id="nombrerazonsocial" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Dirección</label>
                            </div>
                            <div class="col-md-4">
                                <input name="direccion" type="text" class="form-control requerido mayuscula" placeholder="N/D" id="direccion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">


                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Telefono</label>
                            </div>
                            <div class="col-md-2">
                                <input name="telefono" type="text" class="form-control requerido" placeholder="N/D" id="telefono" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Celular</label>
                            </div>
                            <div class="col-md-2">
                                <input name="celular" type="text" class="form-control" placeholder="N/D" id="celular" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Correo</label>
                            </div>
                            <div class="col-md-4">
                                <input name="correo" type="text" class="form-control requerido" placeholder="N/D" id="correo" disabled="disabled">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <div class="col-md-1">
                                <label class="form-control-label">Sucursal</label>
                            </div>
                            <div class="col-md-2  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="sucursal" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label">Codigo Sucursal</label>
                            </div>
                            <div class="col-md-1  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="codigosucursal" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Provincia</label>
                            </div>
                            <div class="col-md-2">

                                <select class="form-control" id="provincia" name="provincia" disabled="disabled">
                                    <option value="1064">Azuay</option>
                                    <option value="1061">Bolivar</option>
                                    <option value="1063">Cañar</option>
                                    <option value="1056">Carchi</option>
                                    <option value="1062">Chimborazo</option>
                                    <option value="1059">Cotopaxi</option>
                                    <option value="1055">El Oro</option>
                                    <option value="1053">Esmeraldas</option>
                                    <option value="888">Exterior</option>
                                    <option value="1072">Galapagos</option>
                                    <option value="1050">Guayas</option>
                                    <option value="1057">Imbabura</option>
                                    <option value="1065">Loja</option>
                                    <option value="1051">Los Rios</option>
                                    <option value="1052">Manabi</option>
                                    <option value="1070">Morona Santiago</option>
                                    <option value="1067">Napo</option>
                                    <option value="1068">Orellana</option>
                                    <option value="1069">Pastaza</option>
                                    <option value="1058">Pichincha</option>
                                    <option value="1054">Santa Elena</option>
                                    <option value="1074">Santo Domingo</option>
                                    <option selected="true" value="999">Sin Especificar</option>
                                    <option value="1066">Sucumbios</option>
                                    <option value="1060">Tungurahua</option>
                                    <option value="1071">Zamora Chinchipe</option>
                                </select>

                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Ciudad</label>
                            </div>
                            <div class="col-md-2">
                                <select name="ciudad" class="form-control" id="ciudad" disabled="disabled">
                                    <option selected="true" value="999">Sin Especificar</option>
                                </select>

                            </div>
                        </div>
                    </div>



                    <script>
                        $('#provincia').change(function(){
                            buscarciudad();
                        })
                        function buscarciudad(){
                            var provincia=$('#provincia').val();
                            $.ajax({
                                type: 'POST',
                                url:"https://azur.com.ec/plataforma/ciudades",
                                data: {
                                    id_provincia:provincia,
                                },
                                success: function(resp) {
                                    if (resp.respuesta == true) {
                                        $('#ciudad').empty();
                                        $(resp.datos).each(function(i,v){
                                            $('#ciudad').append(' <option value='+v.id+'>'+v.nombre+'</option>');
                                        });
                                    }
                                }
                            })
                        }

                    </script>
                </div>
                <div class="col-md-12">
                    &nbsp; *Si desea Crear Otro Sucursal Primero Crearlo en Menú-&gt;proveedores-&gt;Editar Sucursales.
                </div>
            </div>
        </div>


        <script>
            $(document).ready(function () {
                bloquearproveedor();
                $('#botonnuevoproveedor').click(function () {
                    nuevoproveedor();
                    if ($("#criterioproveedor").val() != "" && isNaN($("#criterioproveedor").val()) == false) {
                        $("#identificacion").val($("#criterioproveedor").val());
                        if ($('#identificacion').val().length == 10) {
                            $('#tipoidentificacion').val("05");
                        } else if ($('#identificacion').val().length == 13) {
                            $('#tipoidentificacion').val("04");
                        }
                        buscardatosensri($("#identificacion").val());
                    }
                    $("#criterioproveedor").val("");
                });
                $('#botoneditarproveedor').click(function () {
                    editarproveedor();
                });






                var inputcriterioproveedor = document.getElementById("criterioproveedor"),
                    intervaloproveedor;
                inputcriterioproveedor.addEventListener("keyup", function () {
                    clearInterval(intervaloproveedor);
                    intervaloproveedor = setInterval(function () { //Y vuelve a iniciar
                        limpiarproveedor();
                        buscarproveedor();
                        clearInterval(intervaloproveedor); //Limpio el intervalo
                    }, 600);
                }, false);


                // $('#criterioproveedor').keyup(function () {
                //     limpiarproveedor();
                //     if($(this).val().length>=3){
                //         buscarproveedor();
                //     }
                // })

                $('#botonbuscarextraboton').click(function () {
                    var criterio = $('#criterioproveedor').val();
                    if (criterio != "") {
                        buscarproveedor();
                    } else {
                        aviso("error", "Debe Ingresar por lo menos un digito para buscar.", "", "");
                    }
                });


                function buscarproveedor() {
                    $("#tipo_evento_proveedor").val("");
                    var criterio = $('#criterioproveedor').val();
                    bloquearproveedor();
                    $.ajax({
                        type: 'get',
                        url: "https://azur.com.ec/plataforma/listadoproveedores",
                        data: {criterio: criterio, api_key2: "API_1_2_5a4492f2d5137", demo: "1"},
                        success: function (resp) {
                            if (resp.respuesta == true) {
                                $('#proveedores').empty();
                                $(resp.datos).each(function (i, v) {
                                    $('#proveedores').append('<option value=' + v.id + '>' + v.nombrerazonsocial + '</option>');
                                });
                                buscarsucursalproveedor();
                            }
                        }
                    })
                }

                $('#proveedores').change(function () {
                    limpiarproveedor();
                    bloquearproveedor();
                    buscarsucursalproveedor();
                });
                $('#sucursalesproveedores').change(function () {
                    buscardatosproveedor();
                });

                $('#identificacion').keyup(function () {
                    if ($('#identificacion').val().length == 10) {
                        $('#tipoidentificacion').val("05");
                    } else if ($('#identificacion').val().length == 13) {
                        $('#tipoidentificacion').val("04");
                    }
                });

                $('#identificacion').change(function () {
                    var cedula = $(this).val();
                    var tipoidentificacion = $("#tipoidentificacion").val();

                    if (tipoidentificacion == "05") {
                        //Preguntamos si la cedula consta de 10 digitos
                        if (cedula.length == 10) {

                            //Obtenemos el digito de la region que sonlos dos primeros digitos
                            var digito_region = cedula.substring(0, 2);

                            //Pregunto si la region existe ecuador se divide en 24 regiones
                            if (digito_region >= 1 && digito_region <= 24) {

                                // Extraigo el ultimo digito
                                var ultimo_digito = cedula.substring(9, 10);

                                //Agrupo todos los pares y los sumo
                                var pares = parseInt(cedula.substring(1, 2)) + parseInt(cedula.substring(3, 4)) + parseInt(cedula.substring(5, 6)) + parseInt(cedula.substring(7, 8));

                                //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
                                var numero1 = cedula.substring(0, 1);
                                var numero1 = (numero1 * 2);
                                if (numero1 > 9) {
                                    var numero1 = (numero1 - 9);
                                }

                                var numero3 = cedula.substring(2, 3);
                                var numero3 = (numero3 * 2);
                                if (numero3 > 9) {
                                    var numero3 = (numero3 - 9);
                                }

                                var numero5 = cedula.substring(4, 5);
                                var numero5 = (numero5 * 2);
                                if (numero5 > 9) {
                                    var numero5 = (numero5 - 9);
                                }

                                var numero7 = cedula.substring(6, 7);
                                var numero7 = (numero7 * 2);
                                if (numero7 > 9) {
                                    var numero7 = (numero7 - 9);
                                }

                                var numero9 = cedula.substring(8, 9);
                                var numero9 = (numero9 * 2);
                                if (numero9 > 9) {
                                    var numero9 = (numero9 - 9);
                                }

                                var impares = numero1 + numero3 + numero5 + numero7 + numero9;

                                //Suma total
                                var suma_total = (pares + impares);

                                //extraemos el primero digito
                                var primer_digito_suma = String(suma_total).substring(0, 1);

                                //Obtenemos la decena inmediata
                                var decena = (parseInt(primer_digito_suma) + 1) * 10;

                                //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
                                var digito_validador = decena - suma_total;

                                //Si el digito validador es = a 10 toma el valor de 0
                                if (digito_validador == 10)
                                    var digito_validador = 0;

                                //Validamos que el digito validador sea igual al de la cedula
                                if (digito_validador == ultimo_digito) {
                                    aviso("ok", "Cedula Correcta", "", "");
                                    if (proveedornoexiste(cedula) == true) {
                                        buscardatosensri(cedula);
                                    }

                                    // alert("la Cedula es correcta");
                                } else {
                                    //  alert("la cedula es incorrecta");
                                }

                            } else {
                                // imprimimos en consola si la region no pertenece
                                // console.log('Esta cedula no pertenece a ninguna region');
                            }
                        } else {
                            //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
                            //  alert("faltan digitos en la cedula");
                        }
                    } else if (tipoidentificacion == "04") {
                        if (cedula.length == 13) {
                            var number = cedula;
                            var dto = cedula.length;
                            var valor;
                            var acu = 0;

                            for (var i = 0; i < dto; i++) {
                                valor = number.substring(i, i + 1);
                                if (valor == 0 || valor == 1 || valor == 2 || valor == 3 || valor == 4 || valor == 5 || valor == 6 || valor == 7 || valor == 8 || valor == 9) {
                                    acu = acu + 1;
                                }
                            }
                            if (acu == dto) {
                                while (number.substring(10, 13) != "001") {
                                    alert('Los tres últimos dígitos no tienen el código del RUC 001.');
                                    return;
                                }
                                while (number.substring(0, 2) > 24) {
                                    alert('Los dos primeros dígitos no pueden ser mayores a 24.');
                                    return;
                                }

                                var porcion1 = number.substring(2, 3);
                                if (porcion1 < 6) {
                                }
                                else {
                                    if (porcion1 == 6) {
                                    }
                                    else {
                                        if (porcion1 == 9) {
                                        }
                                    }
                                }
                            }

                            if (proveedornoexiste(cedula) == true) {
                                buscardatosensri(cedula);
                            }

                            //alert("Ruc correcto");
                        } else {
                            //   alert("faltan digitos en el ruc");
                        }
                    }

                })


                function proveedornoexiste(dato) {
                    if (dato != "") {
                        var tipo = $("#tipoidentificacion").val();
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/acciones/proveedornoexiste",
                            data: {identificacion: dato, tipo: tipo, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    return true;
                                } else if (resp.respuesta == "existe") {
                                    $.notify({
                                        title: "<strong>Error</strong> <br>",
                                        message: "<ul>" +
                                            "<li>El proveedor ya Existe</li>" +
                                            "<li>Si desea crear una sucursal lo puede hacer en la opcion proveedores</li>" +
                                            "</ul>",
                                    }, {
                                        type: 'error',
                                        mouse_over: 'pause'
                                    });
                                    limpiarproveedor();
                                    bloquearproveedor();
                                    $("#criterioproveedor").val(dato);
                                    buscarproveedor();
                                    return false;
                                } else {
                                    return false;
                                }
                            }
                        })
                    } else {
                        return false;
                    }
                }

                function buscardatosensri(dato) {
                    if (dato != "") {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/acciones/datosdelgobierno",
                            data: {ruc: dato, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {

                                    if ($("#nombrerazonsocial").val() == "") {
                                        $("#nombrerazonsocial").val(resp.datos.razon_social);
                                    }

                                }
                            }
                        })
                    }
                }

                function buscarsucursalproveedor() {
                    $("#tipo_evento_proveedor").val("");
                    var aux = $('#proveedores').val();
                    if (aux != 0) {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/listadoproveedoressucursales",
                            data: {criterio: aux, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    $('#sucursalesproveedores').empty();
                                    $(resp.datos).each(function (i, v) {
                                        if (v.defecto == true) {
                                            $('#sucursalesproveedores').append(' <option selected="true" value=' + v.id + '>' + v.nombresucursal + '</option>');
                                        } else {
                                            $('#sucursalesproveedores').append(' <option value=' + v.id + '>' + v.nombresucursal + '</option>');
                                        }
                                    });
                                    buscardatosproveedor();
                                }
                            }
                        })
                    }
                }


                function buscarciudadproveedor(id_ciudad) {
                    var provincia = $('#provincia').val();
                    $.ajax({
                        type: 'POST',
                        url: "https://azur.com.ec/plataforma/ciudades",
                        data: {
                            id_provincia: provincia,
                        },
                        success: function (resp) {
                            if (resp.respuesta == true) {
                                $('#ciudad').empty();
                                $(resp.datos).each(function (i, v) {
                                    if (v.id == id_ciudad) {
                                        $('#ciudad').append(' <option selected="true" value=' + v.id + '>' + v.nombre + '</option>');
                                    } else {
                                        $('#ciudad').append(' <option value=' + v.id + '>' + v.nombre + '</option>');
                                    }

                                });
                            }
                        }
                    });
                }

                function buscardatosproveedor() {
                    $("#contenedorproveedor").LoadingOverlay("show");
                    var aux = $('#proveedores').val();
                    var aux2 = $('#sucursalesproveedores').val();
                    if (aux != 0 && aux2 != 0) {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/datosdelproveedor",
                            data: {id_proveedor: aux, id_sucursal: aux2, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    limpiarproveedor();
                                    $("#botoneditarproveedor").removeAttr("disabled");
                                    $('#id_sucursal').val(aux2);
                                    $('#id_proveedor').val(aux);
                                    $('#tipoidentificacion').val(resp.datos.tipoidentificacion);
                                    $("#identificacion").val(resp.datos.identificacion);
                                    $("#nombrerazonsocial").val(resp.datos.nombrerazonsocial);
                                    $("#direccion").val(resp.datos.direccion);
                                    $("#telefono").val(resp.datos.telefono);
                                    $("#celular").val(resp.datos.celular);
                                    $("#correo").val(resp.datos.correo);
                                    $('#sucursal').val(resp.datos.nombresucursal);
                                    $('#codigosucursal').val(resp.datos.codigosucursal);
                                    $('#provincia').val(resp.datos.id_provincia);
                                    buscarciudadproveedor(resp.datos.id_ciudad);

                                    $("#contenedorproveedor").LoadingOverlay("hide");

                                } else {
                                    limpiarproveedor();
                                    $("#contenedorproveedor").LoadingOverlay("hide");
                                }
                            }
                        })
                    } else {
                        limpiarproveedor();
                    }
                }

                function nuevoproveedor() {
                    $("#tipo_evento_proveedor").val("nuevo");
                    $("#botoneditarproveedor").attr("disabled", "disabled");
                    $('#proveedores').empty();
                    limpiarproveedor();
                    desbloquearproveedor();
                    $('#identificacion').val("");
                    $('#identificacion').focus();
                }

                function editarproveedor() {
                    $("#tipo_evento_proveedor").val("editar");
                    $("#botoneditarproveedor").attr("disabled", "disabled");
                    desbloquearproveedor();
                    $('#identificacion').focus();
                }


                function limpiarproveedor() {
                    $("#id_proveedor").val('');
                    $("#id_sucursal").val('');
                    $('#tipoidentificacion').val('05');
                    $("#identificacion").val('');
                    $("#nombrerazonsocial").val('');
                    $("#direccion").val('');
                    $("#telefono").val('');
                    $("#celular").val('');
                    $("#correo").val('');
                    $('#sucursal').val('Matriz');
                    $('#codigosucursal').val('001');
                    $('#provincia').val('999');
                    $('#ciudad').empty();
                    $('#ciudad').append('<option selected="true" value="999">Sin Especificar</option>');
                }

                function desbloquearproveedor() {

                    $('#tipoidentificacion').removeAttr('disabled');
                    $("#identificacion").removeAttr('disabled');
                    $("#nombrerazonsocial").removeAttr('disabled');
                    $("#direccion").removeAttr('disabled');
                    $("#telefono").removeAttr('disabled');
                    $("#celular").removeAttr('disabled');
                    $("#correo").removeAttr('disabled');
                    $('#sucursal').removeAttr('disabled');
                    $('#codigosucursal').removeAttr('disabled');
                    $('#provincia').removeAttr('disabled');
                    $('#ciudad').removeAttr('disabled');
                }

                function bloquearproveedor() {

                    $('#tipoidentificacion').attr('disabled', 'disabled');
                    $("#identificacion").attr('disabled', 'disabled');
                    $("#nombrerazonsocial").attr('disabled', 'disabled');
                    $("#direccion").attr('disabled', 'disabled');
                    $("#telefono").attr('disabled', 'disabled');
                    $("#celular").attr('disabled', 'disabled');
                    $("#correo").attr('disabled', 'disabled');
                    $('#sucursal').attr('disabled', 'disabled');
                    $('#codigosucursal').attr('disabled', 'disabled');
                    $('#provincia').attr('disabled', 'disabled');
                    $('#ciudad').attr('disabled', 'disabled');
                }
            });
        </script>    <div class="box">
            <div class="box-header with-border">

                <h3 class="box-title">Impuestos</h3>
                <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-retencion">
                    <i class="fa fa-search"></i> Añadir
                </button>

            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Cod.Reten.</th>
                            <th>Impuesto</th>
                            <th>Base Imponible</th>
                            <th>Porcentaje</th>
                            <th>Total</th>
                            <th>Documento</th>
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>X</th>
                        </tr>
                        </thead>
                        <tbody id="contenedorretenciones">

                        </tbody>
                    </table>
                </div>
            </div>

        </div>


        <div class="modal fade" id="modal-retencion" style="display: none;">
            <div class="modal-dialog modal-size-small">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Nuevo Impuesto</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-control-label" for="l0">Tipo de Documento</label>
                            </div>
                            <div class="col-md-6">
                                <select id="tipodocumento" name="tipodocumento" class="form-control">
                                    <option value="01" selected="selected">Factura</option>
                                    <option value="03">Liq de Compras</option>
                                    <option value="05">Nota de Debito</option>
                                    <option value="19">COMPROBANTE DE PAGO APORTES</option>
                                    <option value="11">Pasajes emitidos por empresas de aviación</option>
                                    <option value="12">Documentos emitidos por instituciones financieras</option>
                                    <option value="13">Documentos emitidos por compañias de seguros</option>
                                    <option value="15">Comprobantes de venta emitidos en el exterior</option>
                                    <option value="18">Documentos autorizados en ventas excepto ND y NC</option>
                                    <option value="21">Carta de porte aereo</option>
                                    <option value="02">Nota o Boleta de Venta</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-control-label" for="l0">Nro.Comprobante:</label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control validador_secuencialdocumento" name="documento_sustento" id="documento_sustento" placeholder="___-___-_________">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-control-label" for="l0">Fecha de Emision</label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="documento_fecha" id="documento_fecha">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-control-label" for="l0">Impuesto</label>
                            </div>
                            <div class="col-md-6">
                                <select name="codigoimpuesto" id="codigoimpuesto" class="form-control width-middle">
                                    <option value="0" default="">Seleccione una Opcion</option>
                                    <option value="2">IVA</option>
                                    <option value="1">RENTA</option>
                                    <option value="6">ISD</option>
                                </select>
                            </div>
                        </div>
                        <div class="animacionrenta">
                            <div class="row">
                                <div class="col-md-12">
                                    <select id="codigoretencion" name="codigoretencion" class="form-control">
                                        <option value="0" default="">Seleccione un Porcentaje</option>
                                    </select>
                                </div>
                            </div>

                            <div class="animacioncodigo">
                                <div class="row">
                                    <div class="col-md-9">
                                        Porcentaje
                                    </div>
                                    <div class="col-md-3">
                                        <div id="porcentajediv">
                                            <div class="input-group">
                                                <input type="number" class="form-control usd5 " id="porcentaje" name="porcentaje" value="0">
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        Base Imponible
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control usd2" id="baseimponible" name="baseimponible" value="" placeholder="0.00">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="botonanadirimpuesto" class="btn btn-primary" onclick="anadirimpuesto()">
                            Añadir
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <script>

            function anadirimpuesto() {
                if ($('#documento_sustento').val() == "") {
                    aviso("error", "El Numero de Comprobante no Puede estar Vacio", "", "");
                    $('#documento_sustento').focus();
                } else {
                    if ($('#documento_sustento').val().length != "17") {
                        aviso("error", "Secuencial Invalido", ["El Numero de Documento sustento Debe de tener 15 Digitos", "Si el secuencial no tiene los sufientes digitos llenar con ceros al inicio", "3 digitos establecimiento 3 digitos punto emision y 9 digitos secuencial Ejem: 001-001-00000001"], "");
                        $('#documento_sustento').focus();
                    } else {

                        if ($('#documento_fecha').val() == "") {
                            aviso("error", "La fecha del documento no puede estar vacia", "", "");
                            $('#documento_sustento').focus();
                        } else {
                            if ($('#codigoimpuesto').val() == 0) {
                                aviso("error", "El Tipo de Impuesto no puede estar vacio", "", "");
                                $('#codigoimpuesto').focus();
                            } else {
                                if ($('#codigoretencion').val() == 0) {
                                    aviso("error", "El Codigo de Impuesto no puede estar vacio", "", "");
                                    $('#codigoretencion').focus();
                                } else {
                                    if ($('#porcentaje').val() < 0) {
                                        aviso("error", "El porcentaje de retener no puede estar vacio o menor a 0", "", "");
                                        $('#porcentaje').focus();
                                    } else {
                                        if ($('#baseimponible').val() == 0 || $('#baseimponible').val() <= 0) {
                                            aviso("error", "La base a retener no puede estar vacio o menor a 0", "", "");
                                            $('#baseimponible').focus();
                                        } else {
                                            obtenercodigo();
                                            var textotipodocumento = $('select[name="tipodocumento"] option:selected').text();
                                            var tipodocumento = $('#tipodocumento').val();
                                            var documento_sustento = $('#documento_sustento').val();
                                            var documento_fecha = $('#documento_fecha').val();
                                            var textoimpuesto = $('select[name="codigoimpuesto"] option:selected').text();
                                            var tipoimpuesto = $('#codigoimpuesto').val();
                                            var codigoretencion = $('#codigoretencion').attr("codigo");
                                            var porcentaje = $('#porcentaje').val();
                                            var baseimponible = $('#baseimponible').val();
                                            var total = 0;
                                            total = redondear2(parseFloat(baseimponible) * (parseFloat(porcentaje) / 100));

                                            $("#contenedorretenciones").append('<tr>' +
                                                '<td>' +
                                                '<input type="hidden" id="v_tipodocumento" value="' + tipodocumento + '">' +
                                                '<input type="hidden" id="v_tipoimpuesto" value="' + tipoimpuesto + '">' +
                                                '<input type="hidden" id="v_codigoretencion" value="' + codigoretencion + '">' +
                                                '<input type="text" class="form-control"  disabled="true" value="' + codigoretencion + '">' +
                                                '</td>' +
                                                '<td>' +
                                                '<input type="text" id="v_textoimpuesto" class="form-control" disabled="true" value="' + textoimpuesto + '">' +
                                                '</td>' +
                                                '<td>' +
                                                '<input type="text" id="v_baseimponible" class="form-control" disabled="true" value="' + baseimponible + '">' +
                                                '</td>' +
                                                '<td>' +
                                                '<input type="text"  id="v_porcentaje" class="form-control" disabled="true" value="' + porcentaje + '">' +
                                                '</td>' +
                                                '<td>' +
                                                '<input type="text" id="v_total" class="form-control" disabled="true" value="' + total + '">' +
                                                '</td>' +
                                                '<td>' +
                                                '<input type="text" id="v_documento_sustento" class="form-control" disabled="true" value="' + documento_sustento + '">' +
                                                '</td>' +
                                                '<td>' +
                                                '<input type="text" id="v_documento_fecha" class="form-control" disabled="true" value="' + documento_fecha + '">' +
                                                '</td>' +
                                                '<td>' +
                                                '<input type="text" id="v_textotipodocumento" class="form-control" disabled="true" value="' + textotipodocumento + '">' +
                                                '</td>' +
                                                '<td><a id="eliminaritem"><i class="fa fa-times-circle anadir"></i></a></td>' +
                                                '</tr>');

                                            $('#codigoimpuesto').val(0);
                                            $('#codigoretencion').val(0);
                                            $('#porcentaje').val(0);
                                            $('#baseimponible').val(0);
                                            $('#modal-retencion').modal('toggle');
                                            aviso("ok", "Añadido", "", "");
                                            actualizartotales();
                                        }
                                    }
                                }
                            }
                        }
                    }//
                }
            }

            $('#contenedorretenciones').on('click', '#eliminaritem', function () {
                var fila = $(this).parents('tr');
                fila.remove();
                actualizartotales();
            });

            $('#codigoimpuesto').change(function () {
                $(".animacionrenta").LoadingOverlay("show");
                var codigoimpuesto = $('#codigoimpuesto').val();
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/impuestos",
                    data: {
                        codigo: codigoimpuesto,
                        api_key2: "API_1_2_5a4492f2d5137",
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            $('#codigoretencion').empty();
                            $(resp.datos).each(function (i, v) {
                                $('#codigoretencion').append(' <option value=' + v.id + '>' + v.codigoretencion + " - " + v.descripcion.substr(0,100) + '</option>');
                            });
                            obtenercodigo();

                            porcentajeretencion();
                            $(".animacionrenta").LoadingOverlay("hide");
                        } else {
                            $(".animacionrenta").LoadingOverlay("hide");
                        }
                    }
                })
            });



            function porcentajeretencion() {
                $(".animacioncodigo").LoadingOverlay("show");
                var codigo = $('#codigoretencion').val();
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/impuestosporcentaje",
                    data: {
                        codigo: codigo,
                        api_key2: "API_1_2_5a4492f2d5137",
                    },
                    success: function (resp) {
                        $('#porcentaje').val("");
                        if (resp.respuesta == true) {
                            $('#porcentaje').val(resp.valor);
                            $(".animacioncodigo").LoadingOverlay("hide");
                        } else {
                            $(".animacioncodigo").LoadingOverlay("hide");
                        }
                    }
                })
            }

            $('#codigoretencion').change(function () {
                porcentajeretencion();
            });


            $('#documento_fecha').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $(function () {
                $('#documento_sustento').mask("000-000-000000000", {placeholder: "___-___-_________"});
            })

            $('#codigoretencion').change(function () {
                obtenercodigo();
            });

            function obtenercodigo() {
                $(".animacionrenta").LoadingOverlay("show");
                var id = $('#codigoretencion').val();
                ;
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/impuestoscodigo",
                    data: {
                        id: id,
                        api_key2: "API_1_2_5a4492f2d5137",
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {

                            $('#codigoretencion').attr("codigo", resp.valor);
                            $(".animacionrenta").LoadingOverlay("hide");
                        } else {
                            $(".animacionrenta").LoadingOverlay("hide");
                        }
                    }
                })
            }
        </script>
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Datos adicionales (Opcional) </h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-default1">
                            <i class="fa fa-plus"></i> Añadir
                        </button>
                    </div>
                    <div class="box-body ">
                        <table class=" table ">
                            <thead>
                            <tr><th>Nombre</th>
                                <th>Detalle</th>
                            </tr></thead>
                            <tbody id="tabladatosadicianales" class="tabladatosadicianales">

                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="modal-default1" style="display: none;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Datos Adicionales</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-2"><b>Nombre</b></div>
                                        <div class="col-md-4">
                                            <input id="nombre_adicional" name="nombre_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: Orden">
                                        </div>
                                        <div class="col-md-2"><b>Descripción</b></div>
                                        <div class="col-md-4">
                                            <input id="detalle_adicional" name="detalle_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: ABCD123">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

                                    <button class="btn  btn-primary " id="botonanadiracional"> Añadir Adicional</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    $( document ).ready(function() {
                        $("#botonanadiracional").click(function (){
                            var nombre=$("#nombre_adicional").val();
                            var detalle=$("#detalle_adicional").val();

                            var contador=0;
                            $('#tabladatosadicianales tr').each(function (index) {
                                contador=contador+1;
                            });

                            if(contador >9){
                                swal({
                                    type: 'error',
                                    title: 'Maximo se puede añadir 10 datos adicionales',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                return ;
                            }

                            if(nombre=="" && detalle==""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar Llenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else if(nombre=="" && detalle!=""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar LLenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else if(nombre!="" && detalle==""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar LLenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else{
                                anadir_informacionadicional(nombre,detalle);
                                swal({
                                    type: 'success',
                                    title: 'Correcto',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                limpiar_informacionadicional();
                                $('#modal-default1').modal('toggle');
                            }
                        })

                        function limpiar_informacionadicional(){
                            $("#nombre_adicional").val("");
                            $("#detalle_adicional").val("");
                        }

                        function anadir_informacionadicional(nombre,detalle){


                            $(".tabladatosadicianales").append('<tr>'+
                                '<td><input type="text" disabled="true" class="form-control" value="'+ nombre +'" id="nombre"></td>'+
                                '<td><input type="text" disabled="true" class="form-control" value="'+ detalle +'" id="detalle"></td>'+
                                '<td><a id="eliminar_informacionadicional"><i class="fa fa-times-circle anadir"></i></a></td>'+
                                '</tr>');




                        }

                        $('.tabladatosadicianales').on('click','#eliminar_informacionadicional', function() {
                            var fila = $(this).parents('tr');
                            fila.remove();
                        })
                    });
                </script>        </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Total Retenido</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    IVA
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" disabled="true" id="totaliva">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    RENTA
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" disabled="true" id="totalrenta">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    ISD
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" disabled="true" id="totalisd">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    TOTAL
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" disabled="true" id="totalretencion">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>





        <center>
            <button type="button" class="btn btn-primary " onclick="enviar()"><i class="fa fa-save"></i> Guardar, Firmar y
                Enviar
            </button>
        </center>

        <script>

            secuencialpuntoemision();

            function secuencialpuntoemision() {
                var tipo = $("#tipo_tecnologia").val();
                var id_empresa = "1";
                var id_establecimiento = "1";
                var id_puntoemision="16";
                var ambiente="1";
                var secuencial = $("#secuencial").val();
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/acciones/secuencialpuntoemision",
                    data: {
                        tipo: tipo,
                        id_empresa: id_empresa,
                        id_establecimiento: id_establecimiento,
                        id_puntoemision: id_puntoemision,
                        ambiente: ambiente,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            $(resp.datos).each(function (index, element) {
                                var secuencialenbase = "";
                                if (ambiente == 1) {
                                    secuencialenbase = element.secuencia_retencion + 1;
                                } else if (ambiente == 2) {
                                    secuencialenbase = element.secuencia_retencion2 + 1;
                                }

                                if (secuencial == secuencialenbase) {

                                } else {
                                    if (secuencial == "" || secuencial == null) {
                                        $("#secuencial").val(secuencialenbase);
                                    } else {
                                        if (secuencial > secuencialenbase) {
                                            $("#secuencial").val(secuencialenbase);
                                            $.notify({
                                                title: "<strong>Cambio de Secuencial</strong> <br>",
                                                message: "<ul>" +
                                                    "<li>El Secuencial Ingresado no puede ser mayor al Registrado en la base de datos, Le asignamos un nuevo secuencial.</li>" +
                                                    "<li>Para Cambiar el Secuencial Inicial debe ir a Configuracion->Empresas </li>" +
                                                    "</ul>",
                                            }, {
                                                type: 'warning',
                                                mouse_over: 'pause'
                                            });
                                        } else {

                                        }


                                    }
                                }
                            });
                        } else {
                            console.log(resp);
                        }
                    },
                    error : function(xhr) {
                        swal({
                            title: "Oops!",
                            text: "Error en Guardado Vuelva a Intentarlo",
                            type: "error"
                        });
                    },
                })
            }

            var secuencial_puntoemision = setInterval('secuencialpuntoemision()', 60000);

            function actualizartotales() {
                var acumuladoriva = 0;
                var acumuladorrenta = 0;
                var acumuladorisd = 0;
                var acumuladortotal = 0;
                var tipoimpuesto, total;
                $('#contenedorretenciones tr').each(function (index) {
                    tipoimpuesto = $(this).find('#v_tipoimpuesto').val();
                    total = parseFloat($(this).find('#v_total').val());
                    if (tipoimpuesto == 2) {
                        acumuladoriva = acumuladoriva + total;
                    } else if (tipoimpuesto == 1) {
                        acumuladorrenta = acumuladorrenta + total;
                    } else if (tipoimpuesto == 6) {
                        acumuladorisd = acumuladorisd = total;
                    }
                    acumuladortotal = acumuladortotal + total;
                });

                $('#totaliva').val(redondear2(acumuladoriva));
                $('#totalrenta').val(redondear2(acumuladorrenta));
                $('#totalisd').val(redondear2(acumuladorisd));
                $('#totalretencion').val(redondear2(acumuladortotal));
            }

            function enviar() {
                actualizartotales();
                var cabecera = {
                    "fecha": $("#fecha").val(),
                    "tipo": $("#tipo_tecnologia").val(),
                    "id_empresa": "1",
                    "id_vendedor": "3",
                    "id_usuario": "1",
                    "id_establecimiento": "1",
                    "id_puntoemision": $("#punto_emision").val(),
                    "secuencial": $("#secuencial").val(),
                    "ambiente": $("#ambiente").val(),
                    "editar": "NO",
                };


                var proveedor = {
                    "tipoidentificacion": $('#tipoidentificacion').val(),
                    "identificacion": $("#identificacion").val(),
                    "nombrerazonsocial": $("#nombrerazonsocial").val(),
                    "direccion": $("#direccion").val(),
                    "telefono": $("#telefono").val(),
                    "celular": $("#celular").val(),
                    "correo": $("#correo").val(),
                    "nombresucursal": $('#sucursal').val(),
                    "codigosucursal": $('#codigosucursal').val(),
                    "id_provincia": $('#provincia').val(),
                    "id_ciudad": $('#ciudad').val(),
                    "id_proveedor": $('#id_proveedor').val(),
                    "id_sucursal": $('#id_sucursal').val(),
                    "tipo_evento_proveedor": $("#tipo_evento_proveedor").val(),
                    "demo": "1",
                };


                //console.log(proveedor);

                var elementos = [];

                var tipodocumento, tipoimpuesto, codigoretencion, baseimponible, porcentaje, total, documento_sustento,
                    documento_fecha, codigoretencion;
                $('#contenedorretenciones tr').each(function (index) {
                    tipodocumento = $(this).find('#v_tipodocumento').val();
                    tipoimpuesto = $(this).find('#v_tipoimpuesto').val();
                    codigoretencion = $(this).find('#v_codigoretencion').val();
                    baseimponible = $(this).find('#v_baseimponible').val();
                    porcentaje = $(this).find('#v_porcentaje').val();
                    total = $(this).find('#v_total').val();
                    documento_sustento = $(this).find('#v_documento_sustento').val();
                    documento_fecha = $(this).find('#v_documento_fecha').val();
                    codigoretencion = $(this).find('#v_codigoretencion').val();

                    elementos.push({
                        "tipodocumento": tipodocumento,
                        "tipoimpuesto": tipoimpuesto,
                        "codigoretencion": codigoretencion,
                        "baseimponible": baseimponible,
                        "porcentaje": porcentaje,
                        "total": total,
                        "documento_sustento": documento_sustento,
                        "documento_fecha": documento_fecha,
                    });

                });

                var totales = {
                    "totaliva": $('#totaliva').val(),
                    "totalrenta": $('#totalrenta').val(),
                    "totalisd": $('#totalisd').val(),
                    "totalretencion": $('#totalretencion').val(),
                };


                var adicionales = [];
                var nombre, detalle;
                $('#tabladatosadicianales tr').each(function (index) {
                    nombre = $(this).find('#nombre').val();
                    detalle = $(this).find('#detalle').val();
                    adicionales.push({
                        "nombre": nombre,
                        "detalle": detalle,
                    });
                });


                if ($('#tipo_evento_cliente').val() == "final"
                    || $("#identificacion").val() == "9999999999999"
                    || $("#identificacion").val() == "") {
                    aviso("error", "ERROR", ["Las Retenciones no pueden ser a Consumidor Final"], "");
                } else if (elementos.length == 0) {
                    aviso("error", "ERROR", ["No Puede estar Vacía la Retencion"], "");
                } else {

                    swal({
                            title: "Esta Seguro que desea Procesar la Retención",
                            text: "Guardar, Firmar, Enviar",
                            type: "info",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                        },
                        function () {
                            $.ajax({
                                type: 'POST',
                                url: "https://azur.com.ec/plataforma/retencion/guardar",
                                data: {
                                    cabecera: cabecera,
                                    proveedor: proveedor,
                                    retenciones: elementos,
                                    totales: totales,
                                    adicionales: adicionales,
                                    api_key2: "API_1_2_5a4492f2d5137"
                                },
                                success: function (resp) {
                                    if (resp.respuesta == true) {
                                        swal({title: "Excelente!",text: "Guardado y Procesando",type: "success"});
                                        location.href = "https://azur.com.ec/plataforma/enviados";
                                    } else if (resp.respuesta == false) {
                                        console.log(resp);
                                        swal({title: "Oops!",text: resp.error,type: "error"});
                                        aviso("error", "ERROR", [resp.error], ["Contacte con Soporte Tecnico"]);
                                    } else {
                                        console.log(resp);
                                        swal({title: "Oops!",text: "Error en Guardado Vuelva a Intentarlo",type: "error"});
                                    }
                                },
                                error : function(xhr) {

                                    erroresenajax(xhr);

                                },
                            })
                        });


                }
            }


            $('#fecha').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $("#fecha").focusout(function () {
                var fechadelafactura = document.getElementById("fecha").value;
                swal({
                        title: "Esta seguro que desea cambiar la fecha de la Factura ?",
                        text: "Recuerde que la fecha no puede ser mayor a 30 días , tampoco ser una fecha futura.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, Cambiar Fecha!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({title: "Excelente!", text: "Fecha del Comprobante Cambiado: ", type: "success", timer: 4000});
                    })
            })


        </script>
    </section>
    <!-- /.content -->

</div>

@endsection
@section('scripts')
    @include('invoices.partials._invoices_js')
@endsection