@extends('app')
@section('content')
<section class="content">
   <div class="box">
      <div class="box-header with-border">
         <form method="get" action="">
            <div class="col-md-3"> <input type="text" value="" class="form-control" name="criterio"> </div>
            <div class="col-md-3"> <button type="submit" class="btn btn-success "><i class="fa fa-search"></i> &nbsp;Buscar</button> </div>
            <div class="col-md-3"> <button name="accion" value="excel" type="submit" class="btn btn-success "><i class="fa fa-download"></i> &nbsp;Exportar</button> </div>
         </form>
         <div class="box-tools pull-right"> <a href="https://sistema.factexp.site/plataforma/proveedores/importar"> <button type="button" class="btn bg-navy margin"><i class="fa fa-upload"></i> &nbsp;Importar </button> </a> <a class="box-tools pull-right" href="https://sistema.factexp.site/plataforma/proveedores/nuevo"> <button type="button" class="btn btn-success margin"><i class="fa fa-plus-square"></i> &nbsp; Nuevo Proveedor </button> </a> </div>
      </div>
      <div class="box-body">
         <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover ">
               <thead class="thead-dark">
                  <tr>
                     <th>Identificación</th>
                     <th>Nombre o Razon Social</th>
                     <th>Tipo</th>
                     <th>Acciones</th>
                  </tr>
               </thead>
               <tbody> </tbody>
            </table>
         </div>
      </div>
      <div class="box-footer">
         <ul class="pagination">
            <!-- Previous Page Link --> 
            <li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1">«</a> </li>
            <!-- Pagination Elements --> <!-- "Three Dots" Separator --> <!-- Array Of Links --> 
            <li class="page-item active"> <a class="page-link" href="#"> <span class="">1</span> </a> </li>
            <!-- Next Page Link --> 
            <li class="disabled page-item"><span>»</span></li>
         </ul>
      </div>
   </div>
</section>
@endsection


