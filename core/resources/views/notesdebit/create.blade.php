@extends('app')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Nueva Nota de Debito en AZUR FACTURACIÓN ELECTRÓNICA Codigo Estab. 001

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Azur Facturación Electronica </a>
            </li>
            <li class="active">Nota de Debito</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div class="box">
            <div class="box-body">
                <div class="row">

                    <div class="col-md-2">
                        <b>Fecha</b>
                        <input type="text" class="form-control" id="fecha" value="2021-04-19">
                    </div>
                    <div class="col-md-2">
                        <b>Tipo</b>
                        <select class="form-control" name="tipo_tecnologia" disabled="" id="tipo_tecnologia"><option selected="true" value="1">Electrónica</option></select>
                    </div>
                    <div class="col-md-2">
                        <b>Punto Emisión</b>
                        <select class="form-control" disabled="true" id="punto_emision" name="punto_emision"><option value="1">003-Otros</option><option selected="true" value="16">002-Caja 2</option><option value="35">001-General</option><option value="1505">900-Caja 2</option><option value="2752">901-901</option></select>
                    </div>
                    <div class="col-md-2">
                        <b>Secuencial</b>
                        <input class="form-control validador_numeroentero" type="number" value="" id="secuencial">
                    </div>

                    <div class="col-md-2">
                        <b>Ambiente</b>
                        <select class="form-control" name="ambiente" id="ambiente"><option selected="true" value="1">Pruebas</option></select>
                    </div>
                </div>


            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Datos del Comprador</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col col-md-3">
                        Cliente
                        <div class="input-group">
                            <input id="criteriocliente" name="criteriocliente" class="form-control" type="text" value="" placeholder="Ingrese su Busqueda">
                            <span class="input-group-btn">
                            <button id="botonbuscarextraboton" type="button" class="btn btn-primary btn-flat"><i class="fa  fa-search"></i></button>
                          </span>
                        </div>



                    </div>
                    <div class="col col-md-3">
                        Seleccione un Resultado
                        <select class="form-control" id="clientes">
                        </select>
                    </div>

                    <div class="col col-md-3">
                        Sucursal
                        <select class="form-control" id="sucursalesclientes">
                        </select>
                    </div>
                    <div class="col col-md-3">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-sm" id="botonnuevocliente"> <i class="fa fa-plus-square"></i> &nbsp;Nuevo</button>
                            <button type="button" disabled="disabled" class="btn btn-warning btn-sm" id="botoneditarcliente"> <i class="fa fa-edit"></i>&nbsp;Editar</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" id="contenedorcliente">
                <div class="col col-md-12">
                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <input type="hidden" id="tipo_evento_cliente" value="final">
                            <input type="hidden" id="id_cliente" value="">
                            <input type="hidden" id="id_sucursal" value="">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Tipo Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control requerido" name="tipoidentificacion" id="tipoidentificacion" disabled="disabled">
                                    <option selected="" value="05">Cedula</option>
                                    <option value="04">Ruc</option>
                                    <option value="06">Pasaporte</option>
                                    <option value="08">Identificacion del Exterior</option>
                                    <option value="09">Placa</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label class="form-control-label " for="l0">Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <input name="identificacion" type="text" class="form-control requerido " placeholder="9999999999999" id="identificacion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Nombres/Razon Social</label>
                            </div>
                            <div class="col-md-4">
                                <input name="nombrerazonsocial" type="text" class="form-control requerido mayuscula" placeholder="Consumidor Final" id="nombrerazonsocial" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Dirección</label>
                            </div>
                            <div class="col-md-4">
                                <input name="direccion" type="text" class="form-control requerido mayuscula" placeholder="N/D" id="direccion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">


                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Telefono</label>
                            </div>
                            <div class="col-md-2">
                                <input name="telefono" type="text" class="form-control requerido" placeholder="N/D" id="telefono" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Celular</label>
                            </div>
                            <div class="col-md-2">
                                <input name="celular" type="text" class="form-control" placeholder="N/D" id="celular" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Correo</label>
                            </div>
                            <div class="col-md-4">
                                <input name="correo" type="text" class="form-control requerido" placeholder="N/D" id="correo" disabled="disabled">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <div class="col-md-1">
                                <label class="form-control-label">Sucursal</label>
                            </div>
                            <div class="col-md-2  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="sucursal" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label">Codigo Sucursal</label>
                            </div>
                            <div class="col-md-1  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="codigosucursal" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Provincia</label>
                            </div>
                            <div class="col-md-2">

                                <select class="form-control" id="provincia" name="provincia" disabled="disabled">
                                    <option value="1064">Azuay</option>
                                    <option value="1061">Bolivar</option>
                                    <option value="1063">Cañar</option>
                                    <option value="1056">Carchi</option>
                                    <option value="1062">Chimborazo</option>
                                    <option value="1059">Cotopaxi</option>
                                    <option value="1055">El Oro</option>
                                    <option value="1053">Esmeraldas</option>
                                    <option value="888">Exterior</option>
                                    <option value="1072">Galapagos</option>
                                    <option value="1050">Guayas</option>
                                    <option value="1057">Imbabura</option>
                                    <option value="1065">Loja</option>
                                    <option value="1051">Los Rios</option>
                                    <option value="1052">Manabi</option>
                                    <option value="1070">Morona Santiago</option>
                                    <option value="1067">Napo</option>
                                    <option value="1068">Orellana</option>
                                    <option value="1069">Pastaza</option>
                                    <option value="1058">Pichincha</option>
                                    <option value="1054">Santa Elena</option>
                                    <option value="1074">Santo Domingo</option>
                                    <option selected="true" value="999">Sin Especificar</option>
                                    <option value="1066">Sucumbios</option>
                                    <option value="1060">Tungurahua</option>
                                    <option value="1071">Zamora Chinchipe</option>
                                </select>

                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Ciudad</label>
                            </div>
                            <div class="col-md-2">
                                <select name="ciudad" class="form-control" id="ciudad" disabled="disabled">
                                    <option selected="true" value="999">Sin Especificar</option>
                                </select>

                            </div>
                        </div>
                    </div>



                    <script>
                        $('#provincia').change(function(){

                            buscarciudad();

                        })
                        function buscarciudad(){
                            var provincia=$('#provincia').val();
                            $.ajax({
                                type: 'POST',
                                url:"https://azur.com.ec/plataforma/ciudades",
                                data: {
                                    id_provincia:provincia,
                                },
                                success: function(resp) {
                                    console.log(provincia);
                                    if (resp.respuesta == true) {
                                        $('#ciudad').empty();
                                        $(resp.datos).each(function(i,v){
                                            $('#ciudad').append(' <option value='+v.id+'>'+v.nombre+'</option>');
                                        });
                                    }


                                }
                            })

                        }

                    </script>
                </div>
                <div class="col-md-12">
                    &nbsp; *Si desea Crear Otro Sucursal Primero Crearlo en Menú-&gt;Clientes-&gt;Editar Sucursales.
                </div>
            </div>
        </div>





        <script>
            $(document).ready(function () {
                bloquearcliente();
                $('#botonnuevocliente').click(function () {
                    nuevocliente();
                    if($("#criteriocliente").val()!="" && isNaN($("#criteriocliente").val())==false){
                        $("#identificacion").val($("#criteriocliente").val());
                        if($('#identificacion').val().length==10){
                            $('#tipoidentificacion').val("05");
                        }else if($('#identificacion').val().length==13){
                            $('#tipoidentificacion').val("04");
                        }
                        buscardatosensri($("#identificacion").val());
                    }
                    $("#criteriocliente").val("");
                });
                $('#botoneditarcliente').click(function () {
                    editarcliente();
                });

                var inputcriteriocliente = document.getElementById("criteriocliente"),
                    intervalocliente;
                inputcriteriocliente.addEventListener("keyup", function () {
                    clearInterval(intervalocliente);
                    intervalocliente = setInterval(function () { //Y vuelve a iniciar
                        limpiarcliente();
                        buscarcliente();
                        clearInterval(intervalocliente); //Limpio el intervalo
                    }, 600);
                }, false);




                // $('#criteriocliente').keyup(function () {
                //     limpiarcliente();
                //     if($(this).val().length>=4){
                //         buscarcliente();
                //     }
                // })

                $('#botonbuscarextraboton').click(function () {
                    var criterio = $('#criteriocliente').val();
                    if(criterio!=""){
                        buscarcliente();
                    }else{
                        aviso("error","Debe Ingresar por lo menos un digito para buscar.","","");
                    }
                });

                function buscarcliente() {
                    $("#tipo_evento_cliente").val("");
                    var criterio = $('#criteriocliente').val();
                    bloquearcliente();
                    $.ajax({
                        type: 'get',
                        url: "https://azur.com.ec/plataforma/listadoclientes",
                        data: {criterio: criterio, api_key2: "API_1_2_5a4492f2d5137", demo: "1"},
                        success: function (resp) {
                            if (resp.respuesta == true) {
                                $('#clientes').empty();
                                $(resp.datos).each(function (i, v) {
                                    $('#clientes').append(' <option value=' + v.id + '>' + v.nombrerazonsocial + '</option>');
                                });

                                buscarsucursalcliente();
                            }
                        }
                    })
                }

                $('#clientes').change(function () {
                    limpiarcliente();
                    bloquearcliente();
                    buscarsucursalcliente();
                });
                $('#sucursalesclientes').change(function (){

                    buscardatoscliente();
                });

                $('#identificacion').keyup(function (){
                    if($('#identificacion').val().length==10){
                        $('#tipoidentificacion').val("05");
                    }else if($('#identificacion').val().length==13){
                        $('#tipoidentificacion').val("04");
                    }
                });

                $('#identificacion').change(function () {
                    var cedula = $(this).val();
                    var tipoidentificacion = $("#tipoidentificacion").val();

                    if (tipoidentificacion == "05") {
                        //Preguntamos si la cedula consta de 10 digitos
                        if (cedula.length == 10) {

                            //Obtenemos el digito de la region que sonlos dos primeros digitos
                            var digito_region = cedula.substring(0, 2);

                            //Pregunto si la region existe ecuador se divide en 24 regiones
                            if (digito_region >= 1 && digito_region <= 24) {

                                // Extraigo el ultimo digito
                                var ultimo_digito = cedula.substring(9, 10);

                                //Agrupo todos los pares y los sumo
                                var pares = parseInt(cedula.substring(1, 2)) + parseInt(cedula.substring(3, 4)) + parseInt(cedula.substring(5, 6)) + parseInt(cedula.substring(7, 8));

                                //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
                                var numero1 = cedula.substring(0, 1);
                                var numero1 = (numero1 * 2);
                                if (numero1 > 9) {
                                    var numero1 = (numero1 - 9);
                                }

                                var numero3 = cedula.substring(2, 3);
                                var numero3 = (numero3 * 2);
                                if (numero3 > 9) {
                                    var numero3 = (numero3 - 9);
                                }

                                var numero5 = cedula.substring(4, 5);
                                var numero5 = (numero5 * 2);
                                if (numero5 > 9) {
                                    var numero5 = (numero5 - 9);
                                }

                                var numero7 = cedula.substring(6, 7);
                                var numero7 = (numero7 * 2);
                                if (numero7 > 9) {
                                    var numero7 = (numero7 - 9);
                                }

                                var numero9 = cedula.substring(8, 9);
                                var numero9 = (numero9 * 2);
                                if (numero9 > 9) {
                                    var numero9 = (numero9 - 9);
                                }

                                var impares = numero1 + numero3 + numero5 + numero7 + numero9;

                                //Suma total
                                var suma_total = (pares + impares);

                                //extraemos el primero digito
                                var primer_digito_suma = String(suma_total).substring(0, 1);

                                //Obtenemos la decena inmediata
                                var decena = (parseInt(primer_digito_suma) + 1) * 10;

                                //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
                                var digito_validador = decena - suma_total;

                                //Si el digito validador es = a 10 toma el valor de 0
                                if (digito_validador == 10)
                                    var digito_validador = 0;

                                //Validamos que el digito validador sea igual al de la cedula
                                if (digito_validador == ultimo_digito) {
                                    aviso("ok","Cedula Correcta","","");
                                    if(clientenoexiste(cedula)==true){
                                        buscardatosensri(cedula);
                                    }

                                    // alert("la Cedula es correcta");
                                } else {
                                    //  alert("la cedula es incorrecta");
                                }

                            } else {
                                // imprimimos en consola si la region no pertenece
                                // console.log('Esta cedula no pertenece a ninguna region');
                            }
                        } else {
                            //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
                            //  alert("faltan digitos en la cedula");
                        }
                    } else if (tipoidentificacion == "04") {
                        if (cedula.length == 13) {
                            var number = cedula;
                            var dto = cedula.length;
                            var valor;
                            var acu = 0;

                            for (var i = 0; i < dto; i++) {
                                valor = number.substring(i, i + 1);
                                if (valor == 0 || valor == 1 || valor == 2 || valor == 3 || valor == 4 || valor == 5 || valor == 6 || valor == 7 || valor == 8 || valor == 9) {
                                    acu = acu + 1;
                                }
                            }
                            if (acu == dto) {
                                while (number.substring(10, 13) != 001) {
                                    alert('Los tres últimos dígitos no tienen el código del RUC 001.');
                                    return;
                                }
                                while (number.substring(0, 2) > 24) {
                                    alert('Los dos primeros dígitos no pueden ser mayores a 24.');
                                    return;
                                }

                                var porcion1 = number.substring(2, 3);
                                if (porcion1 < 6) {
                                }
                                else {
                                    if (porcion1 == 6) {
                                    }
                                    else {
                                        if (porcion1 == 9) {
                                        }
                                    }
                                }
                            }

                            if(clientenoexiste(cedula)==true){
                                buscardatosensri(cedula);
                            }

                            //alert("Ruc correcto");
                        } else {
                            //   alert("faltan digitos en el ruc");
                        }
                    }

                })


                function clientenoexiste(dato){
                    if (dato != ""){
                        var tipo=$("#tipoidentificacion").val();
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/acciones/clientenoexiste",
                            data: {identificacion:dato,tipo:tipo ,api_key2:"API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    return true;
                                }else if(resp.respuesta == "existe"){
                                    $.notify({
                                        title: "<strong>Error</strong> <br>",
                                        message: "<ul>" +
                                            "<li>El Cliente ya Existe</li>" +
                                            "<li>Si desea crear una sucursal lo puede hacer en la opcion Clientes</li>" +
                                            "</ul>",
                                    }, {
                                        type: 'error',
                                        mouse_over: 'pause'
                                    });
                                    limpiarcliente();
                                    bloquearcliente();
                                    $("#criteriocliente").val(dato);
                                    buscarcliente();
                                    return false;
                                }else{
                                    return false;
                                }
                            }
                        })
                    }else{
                        return false;
                    }
                }
                function buscardatosensri(dato) {
                    if (dato != ""){
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/acciones/datosdelgobierno",
                            data: {ruc:dato ,api_key2:"API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {

                                    if($("#nombrerazonsocial").val()==""){
                                        $("#nombrerazonsocial").val(resp.datos.razon_social);
                                    }

                                }
                            }
                        })
                    }
                }

                function buscarsucursalcliente() {
                    $("#tipo_evento_cliente").val("");
                    var aux = $('#clientes').val();
                    if (aux != 0) {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/listadoclientessucursales",
                            data: {criterio: aux, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {

                                    $('#sucursalesclientes').empty();
                                    $(resp.datos).each(function (i, v) {
                                        if (v.defecto == true) {
                                            $('#sucursalesclientes').append(' <option selected="true" value=' + v.id + '>' + v.nombresucursal + '</option>');
                                        } else {
                                            $('#sucursalesclientes').append(' <option value=' + v.id + '>' + v.nombresucursal + '</option>');
                                        }
                                    });

                                    buscardatoscliente();
                                }
                            }
                        })
                    }
                }


                function buscarciudadcliente(id_ciudad) {
                    var provincia = $('#provincia').val();
                    $.ajax({
                        type: 'POST',
                        url: "https://azur.com.ec/plataforma/ciudades",
                        data: {
                            id_provincia: provincia,
                        },
                        success: function (resp) {
                            if (resp.respuesta == true) {
                                $('#ciudad').empty();
                                $(resp.datos).each(function (i, v) {
                                    if(v.id==id_ciudad){
                                        $('#ciudad').append(' <option selected="true" value=' + v.id + '>' + v.nombre + '</option>');
                                    }else{
                                        $('#ciudad').append(' <option value=' + v.id + '>' + v.nombre + '</option>');
                                    }

                                });
                            }
                        }
                    });
                }


                function buscardatoscliente() {
                    $("#contenedorcliente").LoadingOverlay("show");
                    var aux = $('#clientes').val();
                    var aux2 = $('#sucursalesclientes').val();
                    if (aux != 0 && aux2 != 0) {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/datosdelcliente",
                            data: {id_cliente: aux, id_sucursal: aux2, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {

                                if (resp.respuesta == true) {
                                    limpiarcliente();


                                    $("#botoneditarcliente").removeAttr("disabled");
                                    $('#id_sucursal').val(aux2);
                                    $('#id_cliente').val(aux);
                                    $('#tipoidentificacion').val(resp.datos.tipoidentificacion);
                                    $("#identificacion").val(resp.datos.identificacion);
                                    $("#nombrerazonsocial").val(resp.datos.nombrerazonsocial);
                                    $("#direccion").val(resp.datos.direccion);
                                    $("#telefono").val(resp.datos.telefono);
                                    $("#celular").val(resp.datos.celular);
                                    $("#correo").val(resp.datos.correo);
                                    $('#sucursal').val(resp.datos.nombresucursal);
                                    $('#codigosucursal').val(resp.datos.codigosucursal);
                                    $('#provincia').val(resp.datos.id_provincia);
                                    buscarciudadcliente(resp.datos.id_ciudad);

                                    // $('#ciudad').val(resp.datos.id_ciudad);

                                    $("#contenedorcliente").LoadingOverlay("hide");
                                }else{
                                    limpiarcliente();
                                    $("#contenedorcliente").LoadingOverlay("hide");
                                }
                            }
                        })
                    } else {
                        limpiarcliente();
                    }
                }

                function nuevocliente() {
                    $("#tipo_evento_cliente").val("nuevo");
                    $("#botoneditarcliente").attr("disabled","disabled");
                    $('#clientes').empty();
                    limpiarcliente();
                    desbloquearcliente();
                    $('#identificacion').val("");
                    $('#identificacion').focus();
                }

                function editarcliente() {
                    $("#tipo_evento_cliente").val("editar");
                    $("#botoneditarcliente").attr("disabled","disabled");
                    desbloquearcliente();
                    $('#identificacion').focus();
                }


                function limpiarcliente() {
                    $("#id_cliente").val('');
                    $("#id_sucursal").val('');
                    $('#tipoidentificacion').val('05');
                    $("#identificacion").val('');
                    $("#nombrerazonsocial").val('');
                    $("#direccion").val('');
                    $("#telefono").val('');
                    $("#celular").val('');
                    $("#correo").val('');
                    $('#sucursal').val('Matriz');
                    $('#codigosucursal').val('001');
                    $('#provincia').val('999');
                    $('#ciudad').empty();
                    $('#ciudad').append('<option selected="true" value="999">Sin Especificar</option>');
                }

                function desbloquearcliente() {

                    $('#tipoidentificacion').removeAttr('disabled');
                    $("#identificacion").removeAttr('disabled');
                    $("#nombrerazonsocial").removeAttr('disabled');
                    $("#direccion").removeAttr('disabled');
                    $("#telefono").removeAttr('disabled');
                    $("#celular").removeAttr('disabled');
                    $("#correo").removeAttr('disabled');
                    $('#sucursal').removeAttr('disabled');
                    $('#codigosucursal').removeAttr('disabled');
                    $('#provincia').removeAttr('disabled');
                    $('#ciudad').removeAttr('disabled');
                }


                function bloquearcliente() {

                    $('#tipoidentificacion').attr('disabled', 'disabled');
                    $("#identificacion").attr('disabled', 'disabled');
                    $("#nombrerazonsocial").attr('disabled', 'disabled');
                    $("#direccion").attr('disabled', 'disabled');
                    $("#telefono").attr('disabled', 'disabled');
                    $("#celular").attr('disabled', 'disabled');
                    $("#correo").attr('disabled', 'disabled');
                    $('#sucursal').attr('disabled', 'disabled');
                    $('#codigosucursal').attr('disabled', 'disabled');
                    $('#provincia').attr('disabled', 'disabled');
                    $('#ciudad').attr('disabled', 'disabled');
                }
            });



        </script>





        <div class="box">
            <div class="box-header with-border">

                <h3 class="box-title">Detalles Notas de Debito</h3>
                <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-detalles">
                    <i class="fa fa-search"></i> Añadir
                </button>
                <div class="modal fade" id="modal-detalles" style="display: none;">
                    <div class="modal-dialog modal-xs">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Detalle</h4>
                            </div>
                            <div class="modal-body">


                                <div class="row">
                                    <div class="col-md-5"><b>Razon Moficación</b></div>
                                    <div class="col-md-7">
                                        <input id="razonmodificacion" name="razonmodificacion" type="text" class="form-control" placeholder="Ejm: Error en Ingreso">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><b>Valor de la Modificación</b></div>
                                    <div class="col-md-7">
                                        <input id="valormodificacion" name="valormodificacion" type="text" class="form-control validador_numero2 usd2">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><b>Iva</b></div>
                                    <div class="col-md-7">
                                        <select class="form-control requerido" name="tipoiva" id="tipoiva">
                                            <option value="2" selected=""> 12%</option>
                                            <option value="0"> 0%</option>
                                            <option value="6"> No Objeto de Impuesto</option>
                                            <option value="7"> Exento de Iva</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>

                                <button type="button" id="botonanadiralcarritoycerrar" class="btn btn-success"> Añadir
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead style="width:100%;">
                            <tr>
                                <th>Razón de la Modificación</th>
                                <th>Valor de la Modificación</th>
                                <th>IVA</th>
                                <th>Valor IVA</th>
                                <th style="width:20px;"><i class="fa fa-times-circle anadir"></i></th>
                            </tr>

                            </thead>
                            <tbody id="detalletabla">


                            </tbody>
                        </table>
                    </div>
                </div>


                <script>


                    $(document).ready(function () {


                        $("#botonanadiralcarritoycerrar").click(function () {
                            if (anadirdetalle() == true) {
                                limpiardetalle();
                                actualizartotales();
                                swal({
                                    type: 'success',
                                    title: 'Agregado',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                $('#modal-detalles').modal('toggle');


                            }

                        });


                        function anadirdetalle() {


                            var razonmodificacion = $('#razonmodificacion').val();
                            var valormodificacion = $('#valormodificacion').val();
                            var tipoiva = $('#tipoiva').val();


                            if ($("#razonmodificacion").val() == "" || $("#razonmodificacion").val() == null) {
                                aviso("error", "Razon Modificación", ["La Razon de Modificación no Puede estar Vacio"], "");
                                $("#razonmodificacion").focus();
                                return;
                            }

                            if ($("#valormodificacion").val() == "" || $("#valormodificacion").val() == null) {
                                aviso("error", "Valor Modificación", ["El valor de Modificación no Puede estar Vacio"], "");
                                $("#valormodificacion").focus();
                                return;
                            }

                            if ($("#valormodificacion").val() <= 0) {
                                aviso("error", "Valor Modificación", ["El valor de Modificación no Puede ser negativo o 0"], "");
                                $("#valormodificacion").focus();
                                $("#valormodificacion").val(0);
                                return;
                            }


                            var porc_iva = funcion_tipoiva(tipoiva);
                            var iva = parseFloat(valormodificacion) * (parseFloat(porc_iva) / 100);
                            var total = parseFloat(valormodificacion) + iva;


                            $("#detalletabla").append('<tr>' +
                                '<td><input type="text" disabled="true" class="form-control " value="' + razonmodificacion + '" id="v_razonmodificacion"></td>' +
                                '<td><input  type="text" disabled="true"  class="form-control validador_numero2 usd2" value="' + valormodificacion + '" id="v_valormodificacion"></td>' +
                                '<td><input type="text"  disabled="true"  class="form-control " value="' + redondear2(iva) + '" id="v_iva"><input type="hidden"  class="form-control " value="' + tipoiva + '" id="v_tipoiva"></td>' +
                                '<td><input type="text" disabled="true" class="form-control" value="' + redondear2(total) + '" id="v_total"></td>' +
                                '<td><a id="eliminaritem"><i class="fa fa-times-circle anadir"></i></a></td>' +
                                '</tr>');

                            limpiardetalle();

                            return true;
                        }


                        function limpiardetalle() {
                            $('#razonmodificacion').val("");
                            $('#valormodificacion').val("");
                            $('#tipoiva').val(2);
                        }



                        $('#detalletabla').on('click', '#eliminaritem', function () {
                            var fila = $(this).parents('tr');
                            fila.remove();
                            actualizartotales();
                        })


                    })
                </script>


            </div>

        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Comprobante Sustento</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <b>Tipo de Comprobante</b>
                            </div>
                            <div class="col-md-8">
                                <select id="sustento_tipocomprobante" class="form-control" name="sustento_tipocomprobante">
                                    <option value="01" selected="true">FACTURA</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <b>Fecha de Emisión</b>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="sustento_fecha" class="form-control" name="sustento_fecha">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <b>Nro Comprobante</b>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="sustento_secuencial" class="form-control validador_secuencialdocumento" name="sustento_secuencial" placeholder="___-___-_________">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Forma de Pago</h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" onclick="anadir_formapago()">
                            <i class="fa fa-plus"></i> Añadir
                        </button>
                    </div>
                    <div class="box-body">
                        <table>
                            <tbody class="tabladatosformadepago">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Forma de Pago</b>
                                        </div>
                                        <div class="col-md-8">
                                            <select id="formadepago" class="form-control" name="formadepago">
                                                <option value="01">Sin utilizacion del sistema financiero</option>
                                                <option value="16">Tarjetas de Debito</option>
                                                <option value="17">Dinero Electronico</option>
                                                <option value="18">Tarjeta Prepago</option>
                                                <option value="19">Tarjeta de Credito</option>
                                                <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>
                                                <option value="21">Endoso de Titulos</option>
                                                <option value="15">COMPENSACIÓN DE DEUDAS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Plazo</b>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input id="plazo" name="plazo" type="number" class="form-control" placeholder="Ejm: 30">
                                                </div>
                                                <div class="col-md-6">
                                                    <select id="tiempo" class="form-control" name="tiempo">
                                                        <option selected="" value="ninguno">ninguno</option>
                                                        <option value="dias">Dias</option>
                                                        <option value="meses">Meses</option>
                                                        <option value="anios">Años</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Valor</b>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control usd2" value="0.00" type="text" placeholder="" id="totalformapago">
                                        </div>
                                    </div>
                                </td>
                                <td><a id="eliminar_formapago"><i class="fa fa-times-circle anadir"></i></a></td>
                            </tr>
                            </tbody>

                        </table>


                    </div>
                </div>

                <script>
                    function anadir_formapago(){
                        $(".tabladatosformadepago").append('<tr><td>'+
                            '  <div class="row">\n' +
                            '                    <div class="col-md-4">\n' +
                            '                        <b>Forma de Pago</b>\n' +
                            '                    </div>\n' +
                            '                    <div class="col-md-8">\n' +
                            '                        <select id="formadepago" class="form-control" name="formadepago">\n' +
                            '\n' +
                            '                            <option value="01">Sin utilizacion del sistema financiero</option>\n' +
                            '                            <option value="16">Tarjetas de Debito</option>\n' +
                            '                            <option value="17">Dinero Electronico</option>\n' +
                            '                            <option value="18">Tarjeta Prepago</option>\n' +
                            '                            <option value="19">Tarjeta de Credito</option>\n' +
                            '                            <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>\n' +
                            '                            <option value="21">Endoso de Titulos</option>\n' +
                            '<option value="15">COMPENSACIÓN DE DEUDAS</option>\n'+
                            '                        </select>\n' +
                            '                    </div>\n' +
                            '                </div>\n' +
                            '                <div class="row">\n' +
                            '                    <div class="col-md-4">\n' +
                            '                        <b>Plazo</b>\n' +
                            '                    </div>\n' +
                            '\n' +
                            '                    <div class="col-md-8">\n' +
                            '                        <div class="row">\n' +
                            '                            <div class="col-md-6">\n' +
                            '                                <input id="plazo" name="plazo" type="number" class="form-control"\n' +
                            '                                       placeholder="Ejm: 30 dias">\n' +
                            '                            </div>\n' +
                            '                            <div class="col-md-6">\n' +
                            '                                <select id="tiempo" class="form-control" name="tiempo">\n' +
                            '                                    <option selected="" value="ninguno">ninguno</option>\n' +
                            '                                    <option value="dias">Dias</option>\n' +
                            '                                    <option value="meses">Meses</option>\n' +
                            '                                    <option value="anios">Años</option>\n' +
                            '                                </select>\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '\n' +
                            '                </div>\n' +
                            '                <div class="row">\n' +
                            '                    <div class="col-md-4">\n' +
                            '                        <b>Valor</b>\n' +
                            '                    </div>\n' +
                            '                    <div class="col-md-8">\n' +
                            '                        <input class="form-control usd2"  value="0.00" type="text"\n' +
                            '                               placeholder="" id="totalformapago">\n' +
                            '                    </div>\n' +
                            '                </div>'+
                            '</td>' +
                            '<td><a id="eliminar_formapago"><i class="fa fa-times-circle anadir"></i></a></td>'+
                            '</tr>');
                    }

                    $('.tabladatosformadepago').on('click','#eliminar_formapago', function() {
                        var fila = $(this).parents('tr');
                        fila.remove();
                    })
                </script>            <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Datos adicionales (Opcional) </h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-default1">
                            <i class="fa fa-plus"></i> Añadir
                        </button>
                    </div>
                    <div class="box-body ">
                        <table class=" table ">
                            <thead>
                            <tr><th>Nombre</th>
                                <th>Detalle</th>
                            </tr></thead>
                            <tbody id="tabladatosadicianales" class="tabladatosadicianales">

                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="modal-default1" style="display: none;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Datos Adicionales</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-2"><b>Nombre</b></div>
                                        <div class="col-md-4">
                                            <input id="nombre_adicional" name="nombre_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: Orden">
                                        </div>
                                        <div class="col-md-2"><b>Descripción</b></div>
                                        <div class="col-md-4">
                                            <input id="detalle_adicional" name="detalle_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: ABCD123">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

                                    <button class="btn  btn-primary " id="botonanadiracional"> Añadir Adicional</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    $( document ).ready(function() {
                        $("#botonanadiracional").click(function (){
                            var nombre=$("#nombre_adicional").val();
                            var detalle=$("#detalle_adicional").val();

                            var contador=0;
                            $('#tabladatosadicianales tr').each(function (index) {
                                contador=contador+1;
                            });

                            if(contador >9){
                                swal({
                                    type: 'error',
                                    title: 'Maximo se puede añadir 10 datos adicionales',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                return ;
                            }

                            if(nombre=="" && detalle==""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar Llenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else if(nombre=="" && detalle!=""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar LLenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else if(nombre!="" && detalle==""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar LLenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else{
                                anadir_informacionadicional(nombre,detalle);
                                swal({
                                    type: 'success',
                                    title: 'Correcto',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                limpiar_informacionadicional();
                                $('#modal-default1').modal('toggle');
                            }
                        })

                        function limpiar_informacionadicional(){
                            $("#nombre_adicional").val("");
                            $("#detalle_adicional").val("");
                        }

                        function anadir_informacionadicional(nombre,detalle){


                            $(".tabladatosadicianales").append('<tr>'+
                                '<td><input type="text" disabled="true" class="form-control" value="'+ nombre +'" id="nombre"></td>'+
                                '<td><input type="text" disabled="true" class="form-control" value="'+ detalle +'" id="detalle"></td>'+
                                '<td><a id="eliminar_informacionadicional"><i class="fa fa-times-circle anadir"></i></a></td>'+
                                '</tr>');




                        }

                        $('.tabladatosadicianales').on('click','#eliminar_informacionadicional', function() {
                            var fila = $(this).parents('tr');
                            fila.remove();
                        })
                    });
                </script>        </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Totales</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal sin
                                        Impuestos</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_sinimpuestos"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">


                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal Iva
                                        12%</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_iva12"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal 0%</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_ivacero"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal no objeto
                                        de iva</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_noobjeto"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal Exento de
                                        iva</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_exento"></div>
                            </div>
                        </div>




                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor ICE</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="totales_ice" id="totales_ice"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor irbpnr</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_irbpnr"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Iva 12%</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_iva12"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Propina 10%</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_propina"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor Total</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_valortotal"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <center>
            <button type="button" class="btn btn-primary " onclick="enviar()"><i class="fa fa-save"></i> Guardar, Firmar y
                Enviar
            </button>
        </center>

        <script>


            secuencialpuntoemision();

            function secuencialpuntoemision() {
                var tipo = $("#tipo_tecnologia").val();
                var id_empresa = "1";
                var id_establecimiento = "1";
                var id_puntoemision = "16";
                var ambiente = "1";
                var secuencial = $("#secuencial").val();
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/acciones/secuencialpuntoemision",
                    data: {
                        tipo: tipo,
                        id_empresa: id_empresa,
                        id_establecimiento: id_establecimiento,
                        id_puntoemision: id_puntoemision,
                        ambiente: ambiente,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            $(resp.datos).each(function (index, element) {
                                var secuencialenbase = "";
                                if (ambiente == 1) {
                                    secuencialenbase = element.secuencia_debito + 1;
                                } else if (ambiente == 2) {
                                    secuencialenbase = element.secuencia_debito2 + 1;
                                }

                                if (secuencial == secuencialenbase) {

                                } else {
                                    if (secuencial == "" || secuencial == null) {
                                        $("#secuencial").val(secuencialenbase);
                                    } else {
                                        if (secuencial > secuencialenbase) {
                                            $("#secuencial").val(secuencialenbase);
                                            $.notify({
                                                title: "<strong>Cambio de Secuencial</strong> <br>",
                                                message: "<ul>" +
                                                    "<li>El Secuencial Ingresado no puede ser mayor al Registrado en la base de datos, Le asignamos un nuevo secuencial.</li>" +
                                                    "<li>Para Cambiar el Secuencial Inicial debe ir a Configuracion->Empresas </li>" +
                                                    "</ul>",
                                            }, {
                                                type: 'warning',
                                                mouse_over: 'pause'
                                            });
                                        } else {

                                        }


                                    }
                                }
                            });
                        } else {
                            console.log(resp);
                        }
                    }
                })
            }

            var secuencial_puntoemision = setInterval('secuencialpuntoemision()', 60000);

            function actualizartotales() {
                var subtotal_sinimpuestos = 0;
                var subtotal_cero = 0;
                var subtotal_noobjeto = 0;
                var subtotal_exento = 0;
                var subtotal_doce = 0;

                var acumulador_ice = 0;
                var acumulador_iva = 0;
                var acumulador_valortotal = 0;
                var totaliva12 = 0;

                $('#detalletabla tr').each(function (index) {
                    subtotal = parseFloat($(this).find('#v_valormodificacion').val());
                    tipo_iva = $(this).find('#v_tipoiva').val();
                    iva = parseFloat($(this).find('#v_iva').val());
                    total = parseFloat($(this).find('#v_total').val());
                    ice=0;

                    subtotal_sinimpuestos = subtotal_sinimpuestos + subtotal;
                    if (tipo_iva == 0) {
                        subtotal_cero = subtotal_cero + subtotal;
                    } else if (tipo_iva == 6) {
                        subtotal_noobjeto = subtotal_noobjeto + subtotal;
                    } else if (tipo_iva == 7) {
                        subtotal_exento = subtotal_exento + subtotal;
                    } else if (tipo_iva == 2) {
                        subtotal_doce = (subtotal_doce + ice) + subtotal;
                    }

                    acumulador_ice = acumulador_ice + ice;
                    //   acumulador_iva = acumulador_iva + iva;
                });

                totaliva12 = subtotal_doce * 12 / 100;

                acumulador_valortotal = subtotal_sinimpuestos + totaliva12 + acumulador_ice;
                $('#totales_subtotal_sinimpuestos').val(redondear2(subtotal_sinimpuestos));
                $('#totales_subtotal_iva12').val(redondear2(subtotal_doce));
                $('#totales_subtotal_ivacero').val(redondear2(subtotal_cero));
                $('#totales_subtotal_exento').val(redondear2(subtotal_exento));
                $('#totales_subtotal_noobjeto').val(redondear2(subtotal_noobjeto));
                $('#totales_ice').val(redondear2(acumulador_ice));
                $('#totales_iva12').val(redondear2(totaliva12));
                $('#totales_valortotal').val(redondear2(acumulador_valortotal));


                var totaldeformas = 0;
                $('.tabladatosformadepago tr').each(function (index) {
                    totaldeformas = totaldeformas + 1;
                });
                if (totaldeformas <= 1) {
                    $('#totalformapago').val(redondear2(acumulador_valortotal));
                }

            }

            function enviar() {
                actualizartotales();
                var cabecera = {
                    "fecha": $("#fecha").val(),
                    "tipo": $("#tipo_tecnologia").val(),
                    "id_empresa": "1",
                    "id_usuario": "1",
                    "id_vendedor": "3",
                    "id_establecimiento": "1",
                    "id_puntoemision": $("#punto_emision").val(),
                    "secuencial": $("#secuencial").val(),
                    "ambiente": $("#ambiente").val(),
                    "editar": "NO",
                };


                var cliente = {
                    "tipoidentificacion": $('#tipoidentificacion').val(),
                    "identificacion": $("#identificacion").val(),
                    "nombrerazonsocial": $("#nombrerazonsocial").val(),
                    "direccion": $("#direccion").val(),
                    "telefono": $("#telefono").val(),
                    "celular": $("#celular").val(),
                    "correo": $("#correo").val(),
                    "nombresucursal": $('#sucursal').val(),
                    "codigosucursal": $('#codigosucursal').val(),
                    "id_provincia": $('#provincia').val(),
                    "id_ciudad": $('#ciudad').val(),
                    "id_cliente": $('#id_cliente').val(),
                    "id_sucursal": $('#id_sucursal').val(),
                    "tipo_evento_cliente": $("#tipo_evento_cliente").val(),
                    "demo": "1",
                };

                var elementos = [];
                var v_razonmodificacion, v_valormodificacion, v_iva, v_tipoiva;
                $('#detalletabla tr').each(function (index) {
                    v_razonmodificacion = $(this).find('#v_razonmodificacion').val();
                    v_valormodificacion = $(this).find('#v_valormodificacion').val();
                    v_iva = $(this).find('#v_iva').val();
                    v_tipoiva = $(this).find('#v_tipoiva').val();
                    elementos.push({
                        "razonmodificacion": v_razonmodificacion,
                        "valormodificacion": v_valormodificacion,
                        "iva": v_iva,
                        "tipoiva": v_tipoiva,
                    });
                });


                var totales = {
                    "subtotal_sinimpuestos": $('#totales_subtotal_sinimpuestos').val(),
                    "subtotal_iva12": $('#totales_subtotal_iva12').val(),
                    "subtotal_ivacero": $('#totales_subtotal_ivacero').val(),
                    "subtotal_exento": $('#totales_subtotal_exento').val(),
                    "subtotal_noobjeto": $('#totales_subtotal_noobjeto').val(),
                    "totales_ice": $('#totales_ice').val(),
                    "totales_iva12": $('#totales_iva12').val(),
                    "totales_valortotal": $('#totales_valortotal').val(),
                };

                var sustento = {
                    "tipocomprobante": $('#sustento_tipocomprobante').val(),
                    "fecha": $('#sustento_fecha').val(),
                    "secuencial": $('#sustento_secuencial').val(),
                };

                if ($("#sustento_fecha").val() == "") {
                    aviso("error", "Documento Sustento", ["La Fecha no puede  estar Vacia"], "");
                    $("#sustento_fecha").focus();
                    return;
                }


                if ($("#sustento_secuencial").val() == null || $("#sustento_secuencial").val() == "" || $("#sustento_secuencial").val().length != 17) {
                    aviso("error", "Documento Sustento", ["El Documento sustento no Puede estar Vacio", "Debe Completar 15 digitos 3 establecimiento 3 punto emision y 9 secuencial"], "");
                    $("#sustento_secuencial").focus();
                    return;
                }


                var formasdepagos = [];
                var formadepago, plazo, tiempo, totalformapago;
                var acumuladorformadepago = 0;
                $('.tabladatosformadepago tr').each(function (index) {
                    formadepago = $(this).find('#formadepago').val();
                    plazo = $(this).find('#plazo').val();
                    if(plazo<0){
                        aviso("error", "ERROR", ["El plazo no puede ser negativo"], "");
                        return;
                    }
                    tiempo = $(this).find('#tiempo').val();
                    totalformapago = parseFloat($(this).find('#totalformapago').val());
                    if (totalformapago > 0) {
                        formasdepagos.push({
                            "formadepago": formadepago,
                            "plazo": plazo,
                            "tiempo": tiempo,
                            "totalformapago": totalformapago,
                        });
                        acumuladorformadepago = (acumuladorformadepago + totalformapago);
                    }
                });



                var adicionales = [];
                var nombre, detalle;
                $('#tabladatosadicianales tr').each(function (index) {
                    nombre = $(this).find('#nombre').val();
                    detalle = $(this).find('#detalle').val();
                    adicionales.push({
                        "nombre": nombre,
                        "detalle": detalle,
                    });
                });

                if (($('#totales_valortotal').val() > 200 && $('#tipo_evento_cliente').val() == "final")
                    || ($('#totales_valortotal').val() > 200 && $("#identificacion").val() == "9999999999999")
                    || ($('#totales_valortotal').val() > 200 && $("#identificacion").val() == "")) {
                    aviso("error", "ERROR", ["Las Notas de Debito Mayores a 200 dólares no pueden ser a Consumidor Final"], "");
                } else if (elementos.length == 0) {
                    aviso("error", "ERROR", ["No Puede estar Vacía la Nota de Debito"], "");
                } else {

                    swal({
                            title: "Esta Seguro que desea Procesar la Nota de Debito",
                            text: "Guardar, Firmar, Enviar",
                            type: "info",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                        },
                        function () {
                            $.ajax({
                                type: 'POST',
                                url: "https://azur.com.ec/plataforma/guardarnotadedebito",
                                data: {
                                    cabecera: cabecera,
                                    cliente: cliente,
                                    detalles: elementos,
                                    totales: totales,
                                    sustento: sustento,
                                    adicionales: adicionales,
                                    formasdepagos: formasdepagos,
                                    api_key2: "API_1_2_5a4492f2d5137"
                                },
                                success: function (resp) {

                                    if (resp.respuesta == true) {
                                        swal({title: "Excelente!", text: "Guardado y Procesando", type: "success"});
                                        location.href = "https://azur.com.ec/plataforma/enviados";
                                    } else if (resp.respuesta == false) {
                                        console.log(resp);
                                        swal({title: "Oops!", text: resp.error, type: "error"});
                                        aviso("error", "ERROR", [resp.error], ["Contacte con Soporte Tecnico"]);
                                    } else {
                                        console.log(resp);
                                        swal({
                                            title: "Oops!",
                                            text: "Error en Guardado Vuelva a Intentarlo",
                                            type: "error"
                                        });
                                    }
                                },
                                error : function(xhr) {

                                    erroresenajax(xhr);

                                },
                            })
                        });

                }
            }



            $('#fecha').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });
            $('#sustento_fecha').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $(function () {
                $('#sustento_secuencial').mask("000-000-000000000", {placeholder: "___-___-_________"});
            })

            $("#fecha").focusout(function () {
                var fechadelanotadedebito = document.getElementById("fecha").value;
                swal({
                        title: "Esta seguro que desea cambiar la fecha de la Nota de Debito?",
                        text: "Recuerde que la fecha no puede ser mayor a 30 días , tampoco ser una fecha futura.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, Cambiar Fecha!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({title: "Excelente!", text: "Fecha del Comprobante Cambiado: ", type: "success", timer: 4000});
                    })
            })


        </script>
    </section>
    <!-- /.content -->

</div>

@endsection
@section('scripts')
    @include('invoices.partials._invoices_js')
@endsection
