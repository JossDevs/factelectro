@extends('app')
@section('content')



    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Nueva Compra en AZUR FACTURACIÓN ELECTRÓNICA Codigo Estab. 001

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Azur Facturación Electronica </a>
            </li>
            <li class="active">Compra</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">




        <b>Compra</b>
        <div class="box" style="margin-bottom: 5px;">
            <div class="box-body">
                <div class="row">

                    <div class="col-md-2">
                        <b>Fecha</b>
                        <input type="text" class="form-control" id="fecha" disabled="" value="2021-04-19">
                    </div>
                    <div class="col-md-2">
                        <b>Tipo</b>

                        <select class="form-control" name="tipo_tecnologia" id="tipo_tecnologia">
                            <option value="0">Físico</option>
                            <option selected="true" value="1">Electrónica</option>
                        </select>


                    </div>
                    <div class="col-md-2">
                        <b>Punto Emisión</b>
                        <select class="form-control" disabled="true" id="punto_emision" name="punto_emision"><option value="1">003-Otros</option><option selected="true" value="16">002-Caja 2</option><option value="35">001-General</option><option value="1505">900-Caja 2</option><option value="2752">901-901</option></select>
                    </div>
                    <div class="col-md-2">
                        <b>Código Interno</b>
                        <input class="form-control validador_numeroentero" type="number" value="" id="secuencial">
                    </div>

                    <div class="col-md-2">
                        <b>Ambiente</b>
                        <select class="form-control" name="ambiente" id="ambiente"><option selected="true" value="1">Pruebas</option></select>
                    </div>

                </div>
                <div class="row">




                    <div class="col-md-3">
                        <b>Sustento Tributario </b>
                        <select id="sustento_comprobante" name="sustento_comprobante" disabled="" class="form-control ">

                            <option value="vacio">Seleccionar  Sustento</option>
                            <option selected="" value="01" data-tipo="1,3,4,5,11,12,21,41,43,47,48,294,344">01 Crédito Tributario
                                para declaración de IVA (servicios y bienes distintos de inventarios y activos fijos)
                            </option>
                            <option value="02" data-tipo="1,2,3,4,5,9,11,12,15,19,20,21,41,43,47,48,294,344">02 Costo
                                o Gasto para declaración de IR (servicios y bienes distintos de inventarios y activos
                                fijos)
                            </option>
                            <option value="03" data-tipo="1,3,4,5,41,47,48,294,344">03 Activo Fijo - Crédito
                                Tributario para declaración de IVA
                            </option>
                            <option value="04" data-tipo="1,2,3,4,5,15,41,47,48,294,344">04 Activo Fijo - Costo o
                                Gasto para declaración de IR
                            </option>
                            <option value="05" data-tipo="1,2,3,4,5,11,15,41,294,344">05 Liquidación Gastos de Viaje,
                                hospedaje y alimentación Gastos IR (a nombre de empleados y no de la empresa)
                            </option>
                            <option value="06" data-tipo="1,3,4,5,41,43,47,48,294,344">06 Inventario - Crédito
                                Tributario para declaración de IVA
                            </option>
                            <option value="07" data-tipo="1,2,3,4,5,15,41,43,47,48,294,344">07 Inventario - Costo o
                                Gasto para declaración de IR
                            </option>
                            <option value="08" data-tipo="1,2,3,4,5,21,294,344">08 Valor pagado para solicitar
                                Reembolso de Gasto (intermediario)
                            </option>
                            <option value="09" data-tipo="1,4,5,45">09 Reembolso por Siniestros</option>
                            <option value="10" data-tipo="19">10 Distribución de Dividendos, Beneficios o
                                Utilidades
                            </option>
                            <option value="11" data-tipo="12">11 Convenios de débito o recaudación para IFI´s
                            </option>
                            <option value="14" data-tipo="1,2,3,4,5">14 Valores facturados por socios a operadoras de transporte (que no constituyen gasto de dicha operadora)
                            </option>
                            <option value="0" data-tipo="0">00 Sin sustento tributario</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <b>Tipo Comprobante</b>
                        <select id="tipo_comprobante" disabled="" name="tipo_comprobante" class="form-control">
                            <option value="vacio">Seleccionar  Comprobante</option>
                            <option id="tipo_1" value="01" style="">01 Factura</option>
                            <option id="tipo_2" value="02" style="display: none;">02 Nota o boleta de venta</option>
                            <option id="tipo_3" value="03" selected="" style="">03 Liquidación de compra de Bienes o Prestación de servicios
                            </option>
                            <option id="tipo_4" value="04" style="">04 Nota de crédito</option>
                            <option id="tipo_5" value="05" style="">05 Nota de débito</option>
                            <option id="tipo_0" value="00" style="display: none;">00 Nota de pedido</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <b>Es Gasto</b><br>
                        <input type="radio" id="es_gasto_si" name="es_gasto" value="1"> <label for="es_gasto_si">SI </label>&nbsp;&nbsp;
                        <input type="radio" id="es_gasto_no" selected="" name="es_gasto" value="0" checked=""> <label for="es_gasto_no">NO</label>
                    </div>


                    <div class="col-md-2">
                        <b>IVA no Crédito Tributario</b><br>
                        <input disabled="" type="radio" id="iva_credito_tributario_si" name="iva_credito_tributario" value="1">
                        <label for="iva_credito_tributario_si">SI </label>&nbsp;&nbsp;
                        <input disabled="" type="radio" id="iva_credito_tributario_no" selected="" name="iva_credito_tributario" value="0" checked=""> <label for="iva_credito_tributario_no">NO</label>
                    </div>
                    <div class="col-md-2">
                        <b>Ingreso de Mercaderia</b><br>
                        <input type="radio" id="ingreso_mercaderia_si" selected="" name="ingreso_mercaderia" value="1">
                        <label for="ingreso_mercaderia_si">SI </label>&nbsp;&nbsp;
                        <input type="radio" id="ingreso_mercaderia_no" name="ingreso_mercaderia" value="0" checked=""> <label for="ingreso_mercaderia_no">NO</label>
                    </div>
                </div>

            </div>
        </div>
        <b>Documento</b>
        <div class="box">
            <div class="box-body">


                <div class="row">
                    <div class="col-md-2">
                        <b>Fecha Comprobante</b>
                        <input class="form-control " type="text" value="2021-04-19" id="fecha_documento">
                    </div>
                    <div class="col-md-1">
                        <b>Estab.</b>
                        <input type="text" disabled="" value="001" maxlength="3" class="form-control" id="codigo_establecimiento_documento">
                    </div>
                    <div class="col-md-1">
                        <b>P.Emisión</b>
                        <input type="text" disabled="" value="002" maxlength="3" class="form-control" id="codigo_puntoemision_documento">
                    </div>
                    <div class="col-md-2">
                        <b>Secuencial</b>
                        <input class="form-control validador_numeroentero" type="number" value="" maxlength="9" id="secuencial_documento">
                    </div>


                    <div class="col-md-4">
                        <b>Autorización SRI</b>
                        <input class="form-control " type="number" value="" id="autorizacion_sri">
                    </div>

                    <div class="col-md-2">
                        <b>Fecha Caducidad Doc. </b>
                        <input class="form-control " type="text" value="" id="fecha_documento_caducidad">
                    </div>

                    <div class="col-md-6">
                        <b>Descripción </b>
                        <input class="form-control " type="text" value="" id="descripcion">
                    </div>
                    <div class="col-md-2">
                        <b>Vencimiento </b>
                        <input class="form-control " type="text" value="" id="vencimiento">
                    </div>
                    <div class="col-md-2">
                        <b>Fecha ingreso Bodega </b>
                        <input class="form-control " type="text" value="2021-04-19" id="fecha_ingreso_bodega">
                    </div>

                    <div class="col col-md-2">
                        Bodega
                        <select class="form-control" name="bodega_compra" id="bodega_compra"><option value="1">Principal</option><option value="27">Prueba bodega</option><option value="28">oTRA BODEGA</option><option value="29">Keyla</option><option value="30">Nadia Perez</option><option value="31">Bodega Manta</option></select>
                    </div>

                </div>


            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Datos del Proveedor</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col col-md-3">
                        Proveedor
                        <div class="input-group">

                            <input id="criterioproveedor" name="criterioproveedor" class="form-control" type="text" value="" placeholder="Ingrese su Busqueda">
                            <span class="input-group-btn">
                            <button id="botonbuscarextraboton" type="button" class="btn btn-primary btn-flat"><i class="fa  fa-search"></i></button>
                          </span>
                        </div>

                    </div>
                    <div class="col col-md-3">
                        Seleccione un Resultado
                        <select class="form-control" id="proveedores">
                        </select>
                    </div>
                    <div class="col col-md-3">
                        Sucursal
                        <select class="form-control" id="sucursalesproveedores">
                        </select>
                    </div>
                    <div class="col col-md-3">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-sm" id="botonnuevoproveedor"><i class="fa fa-plus-square"></i> &nbsp;Nuevo
                            </button>
                            <button type="button" disabled="disabled" class="btn btn-warning btn-sm" id="botoneditarproveedor">
                                <i class="fa fa-edit"></i>&nbsp;Editar
                            </button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" id="contenedorproveedor">
                <div class="col col-md-12">
                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <input type="hidden" id="tipo_evento_proveedor" value="final">
                            <input type="hidden" id="id_proveedor" value="">
                            <input type="hidden" id="id_sucursal" value="">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Tipo Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control requerido" name="tipoidentificacion" id="tipoidentificacion" disabled="disabled">
                                    <option selected="" value="05">Cedula</option>
                                    <option value="04">Ruc</option>
                                    <option value="06">Pasaporte</option>
                                    <option value="08">Identificacion del Exterior</option>
                                    <option value="09">Placa</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label class="form-control-label " for="l0">Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <input name="identificacion" type="text" class="form-control requerido " placeholder="9999999999999" id="identificacion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Nombres/Razon Social</label>
                            </div>
                            <div class="col-md-4">
                                <input name="nombrerazonsocial" type="text" class="form-control requerido mayuscula" placeholder="Consumidor Final" id="nombrerazonsocial" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Dirección</label>
                            </div>
                            <div class="col-md-4">
                                <input name="direccion" type="text" class="form-control requerido mayuscula" placeholder="N/D" id="direccion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">


                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Telefono</label>
                            </div>
                            <div class="col-md-2">
                                <input name="telefono" type="text" class="form-control requerido" placeholder="N/D" id="telefono" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Celular</label>
                            </div>
                            <div class="col-md-2">
                                <input name="celular" type="text" class="form-control" placeholder="N/D" id="celular" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Correo</label>
                            </div>
                            <div class="col-md-4">
                                <input name="correo" type="text" class="form-control requerido" placeholder="N/D" id="correo" disabled="disabled">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <div class="col-md-1">
                                <label class="form-control-label">Sucursal</label>
                            </div>
                            <div class="col-md-2  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="sucursal" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label">Codigo Sucursal</label>
                            </div>
                            <div class="col-md-1  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="codigosucursal" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Provincia</label>
                            </div>
                            <div class="col-md-2">

                                <select class="form-control" id="provincia" name="provincia" disabled="disabled">
                                    <option value="1064">Azuay</option>
                                    <option value="1061">Bolivar</option>
                                    <option value="1063">Cañar</option>
                                    <option value="1056">Carchi</option>
                                    <option value="1062">Chimborazo</option>
                                    <option value="1059">Cotopaxi</option>
                                    <option value="1055">El Oro</option>
                                    <option value="1053">Esmeraldas</option>
                                    <option value="888">Exterior</option>
                                    <option value="1072">Galapagos</option>
                                    <option value="1050">Guayas</option>
                                    <option value="1057">Imbabura</option>
                                    <option value="1065">Loja</option>
                                    <option value="1051">Los Rios</option>
                                    <option value="1052">Manabi</option>
                                    <option value="1070">Morona Santiago</option>
                                    <option value="1067">Napo</option>
                                    <option value="1068">Orellana</option>
                                    <option value="1069">Pastaza</option>
                                    <option value="1058">Pichincha</option>
                                    <option value="1054">Santa Elena</option>
                                    <option value="1074">Santo Domingo</option>
                                    <option selected="true" value="999">Sin Especificar</option>
                                    <option value="1066">Sucumbios</option>
                                    <option value="1060">Tungurahua</option>
                                    <option value="1071">Zamora Chinchipe</option>
                                </select>

                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Ciudad</label>
                            </div>
                            <div class="col-md-2">
                                <select name="ciudad" class="form-control" id="ciudad" disabled="disabled">
                                    <option selected="true" value="999">Sin Especificar</option>
                                </select>

                            </div>
                        </div>
                    </div>



                    <script>
                        $('#provincia').change(function(){
                            buscarciudad();
                        })
                        function buscarciudad(){
                            var provincia=$('#provincia').val();
                            $.ajax({
                                type: 'POST',
                                url:"https://azur.com.ec/plataforma/ciudades",
                                data: {
                                    id_provincia:provincia,
                                },
                                success: function(resp) {
                                    if (resp.respuesta == true) {
                                        $('#ciudad').empty();
                                        $(resp.datos).each(function(i,v){
                                            $('#ciudad').append(' <option value='+v.id+'>'+v.nombre+'</option>');
                                        });
                                    }
                                }
                            })
                        }

                    </script>
                </div>
                <div class="col-md-12">
                    &nbsp; *Si desea Crear Otro Sucursal Primero Crearlo en Menú-&gt;proveedores-&gt;Editar Sucursales.
                </div>
            </div>
        </div>


        <script>
            $(document).ready(function () {
                bloquearproveedor();
                $('#botonnuevoproveedor').click(function () {
                    nuevoproveedor();
                    if ($("#criterioproveedor").val() != "" && isNaN($("#criterioproveedor").val()) == false) {
                        $("#identificacion").val($("#criterioproveedor").val());
                        if ($('#identificacion').val().length == 10) {
                            $('#tipoidentificacion').val("05");
                        } else if ($('#identificacion').val().length == 13) {
                            $('#tipoidentificacion').val("04");
                        }
                        buscardatosensri($("#identificacion").val());
                    }
                    $("#criterioproveedor").val("");
                });
                $('#botoneditarproveedor').click(function () {
                    editarproveedor();
                });






                var inputcriterioproveedor = document.getElementById("criterioproveedor"),
                    intervaloproveedor;
                inputcriterioproveedor.addEventListener("keyup", function () {
                    clearInterval(intervaloproveedor);
                    intervaloproveedor = setInterval(function () { //Y vuelve a iniciar
                        limpiarproveedor();
                        buscarproveedor();
                        clearInterval(intervaloproveedor); //Limpio el intervalo
                    }, 600);
                }, false);


                // $('#criterioproveedor').keyup(function () {
                //     limpiarproveedor();
                //     if($(this).val().length>=3){
                //         buscarproveedor();
                //     }
                // })

                $('#botonbuscarextraboton').click(function () {
                    var criterio = $('#criterioproveedor').val();
                    if (criterio != "") {
                        buscarproveedor();
                    } else {
                        aviso("error", "Debe Ingresar por lo menos un digito para buscar.", "", "");
                    }
                });


                function buscarproveedor() {
                    $("#tipo_evento_proveedor").val("");
                    var criterio = $('#criterioproveedor').val();
                    bloquearproveedor();
                    $.ajax({
                        type: 'get',
                        url: "https://azur.com.ec/plataforma/listadoproveedores",
                        data: {criterio: criterio, api_key2: "API_1_2_5a4492f2d5137", demo: "1"},
                        success: function (resp) {
                            if (resp.respuesta == true) {
                                $('#proveedores').empty();
                                $(resp.datos).each(function (i, v) {
                                    $('#proveedores').append('<option value=' + v.id + '>' + v.nombrerazonsocial + '</option>');
                                });
                                buscarsucursalproveedor();
                            }
                        }
                    })
                }

                $('#proveedores').change(function () {
                    limpiarproveedor();
                    bloquearproveedor();
                    buscarsucursalproveedor();
                });
                $('#sucursalesproveedores').change(function () {
                    buscardatosproveedor();
                });

                $('#identificacion').keyup(function () {
                    if ($('#identificacion').val().length == 10) {
                        $('#tipoidentificacion').val("05");
                    } else if ($('#identificacion').val().length == 13) {
                        $('#tipoidentificacion').val("04");
                    }
                });

                $('#identificacion').change(function () {
                    var cedula = $(this).val();
                    var tipoidentificacion = $("#tipoidentificacion").val();

                    if (tipoidentificacion == "05") {
                        //Preguntamos si la cedula consta de 10 digitos
                        if (cedula.length == 10) {

                            //Obtenemos el digito de la region que sonlos dos primeros digitos
                            var digito_region = cedula.substring(0, 2);

                            //Pregunto si la region existe ecuador se divide en 24 regiones
                            if (digito_region >= 1 && digito_region <= 24) {

                                // Extraigo el ultimo digito
                                var ultimo_digito = cedula.substring(9, 10);

                                //Agrupo todos los pares y los sumo
                                var pares = parseInt(cedula.substring(1, 2)) + parseInt(cedula.substring(3, 4)) + parseInt(cedula.substring(5, 6)) + parseInt(cedula.substring(7, 8));

                                //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
                                var numero1 = cedula.substring(0, 1);
                                var numero1 = (numero1 * 2);
                                if (numero1 > 9) {
                                    var numero1 = (numero1 - 9);
                                }

                                var numero3 = cedula.substring(2, 3);
                                var numero3 = (numero3 * 2);
                                if (numero3 > 9) {
                                    var numero3 = (numero3 - 9);
                                }

                                var numero5 = cedula.substring(4, 5);
                                var numero5 = (numero5 * 2);
                                if (numero5 > 9) {
                                    var numero5 = (numero5 - 9);
                                }

                                var numero7 = cedula.substring(6, 7);
                                var numero7 = (numero7 * 2);
                                if (numero7 > 9) {
                                    var numero7 = (numero7 - 9);
                                }

                                var numero9 = cedula.substring(8, 9);
                                var numero9 = (numero9 * 2);
                                if (numero9 > 9) {
                                    var numero9 = (numero9 - 9);
                                }

                                var impares = numero1 + numero3 + numero5 + numero7 + numero9;

                                //Suma total
                                var suma_total = (pares + impares);

                                //extraemos el primero digito
                                var primer_digito_suma = String(suma_total).substring(0, 1);

                                //Obtenemos la decena inmediata
                                var decena = (parseInt(primer_digito_suma) + 1) * 10;

                                //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
                                var digito_validador = decena - suma_total;

                                //Si el digito validador es = a 10 toma el valor de 0
                                if (digito_validador == 10)
                                    var digito_validador = 0;

                                //Validamos que el digito validador sea igual al de la cedula
                                if (digito_validador == ultimo_digito) {
                                    aviso("ok", "Cedula Correcta", "", "");
                                    if (proveedornoexiste(cedula) == true) {
                                        buscardatosensri(cedula);
                                    }

                                    // alert("la Cedula es correcta");
                                } else {
                                    //  alert("la cedula es incorrecta");
                                }

                            } else {
                                // imprimimos en consola si la region no pertenece
                                // console.log('Esta cedula no pertenece a ninguna region');
                            }
                        } else {
                            //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
                            //  alert("faltan digitos en la cedula");
                        }
                    } else if (tipoidentificacion == "04") {
                        if (cedula.length == 13) {
                            var number = cedula;
                            var dto = cedula.length;
                            var valor;
                            var acu = 0;

                            for (var i = 0; i < dto; i++) {
                                valor = number.substring(i, i + 1);
                                if (valor == 0 || valor == 1 || valor == 2 || valor == 3 || valor == 4 || valor == 5 || valor == 6 || valor == 7 || valor == 8 || valor == 9) {
                                    acu = acu + 1;
                                }
                            }
                            if (acu == dto) {
                                while (number.substring(10, 13) != "001") {
                                    alert('Los tres últimos dígitos no tienen el código del RUC 001.');
                                    return;
                                }
                                while (number.substring(0, 2) > 24) {
                                    alert('Los dos primeros dígitos no pueden ser mayores a 24.');
                                    return;
                                }

                                var porcion1 = number.substring(2, 3);
                                if (porcion1 < 6) {
                                }
                                else {
                                    if (porcion1 == 6) {
                                    }
                                    else {
                                        if (porcion1 == 9) {
                                        }
                                    }
                                }
                            }

                            if (proveedornoexiste(cedula) == true) {
                                buscardatosensri(cedula);
                            }

                            //alert("Ruc correcto");
                        } else {
                            //   alert("faltan digitos en el ruc");
                        }
                    }

                })


                function proveedornoexiste(dato) {
                    if (dato != "") {
                        var tipo = $("#tipoidentificacion").val();
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/acciones/proveedornoexiste",
                            data: {identificacion: dato, tipo: tipo, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    return true;
                                } else if (resp.respuesta == "existe") {
                                    $.notify({
                                        title: "<strong>Error</strong> <br>",
                                        message: "<ul>" +
                                            "<li>El proveedor ya Existe</li>" +
                                            "<li>Si desea crear una sucursal lo puede hacer en la opcion proveedores</li>" +
                                            "</ul>",
                                    }, {
                                        type: 'error',
                                        mouse_over: 'pause'
                                    });
                                    limpiarproveedor();
                                    bloquearproveedor();
                                    $("#criterioproveedor").val(dato);
                                    buscarproveedor();
                                    return false;
                                } else {
                                    return false;
                                }
                            }
                        })
                    } else {
                        return false;
                    }
                }

                function buscardatosensri(dato) {
                    if (dato != "") {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/acciones/datosdelgobierno",
                            data: {ruc: dato, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {

                                    if ($("#nombrerazonsocial").val() == "") {
                                        $("#nombrerazonsocial").val(resp.datos.razon_social);
                                    }

                                }
                            }
                        })
                    }
                }

                function buscarsucursalproveedor() {
                    $("#tipo_evento_proveedor").val("");
                    var aux = $('#proveedores').val();
                    if (aux != 0) {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/listadoproveedoressucursales",
                            data: {criterio: aux, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    $('#sucursalesproveedores').empty();
                                    $(resp.datos).each(function (i, v) {
                                        if (v.defecto == true) {
                                            $('#sucursalesproveedores').append(' <option selected="true" value=' + v.id + '>' + v.nombresucursal + '</option>');
                                        } else {
                                            $('#sucursalesproveedores').append(' <option value=' + v.id + '>' + v.nombresucursal + '</option>');
                                        }
                                    });
                                    buscardatosproveedor();
                                }
                            }
                        })
                    }
                }


                function buscarciudadproveedor(id_ciudad) {
                    var provincia = $('#provincia').val();
                    $.ajax({
                        type: 'POST',
                        url: "https://azur.com.ec/plataforma/ciudades",
                        data: {
                            id_provincia: provincia,
                        },
                        success: function (resp) {
                            if (resp.respuesta == true) {
                                $('#ciudad').empty();
                                $(resp.datos).each(function (i, v) {
                                    if (v.id == id_ciudad) {
                                        $('#ciudad').append(' <option selected="true" value=' + v.id + '>' + v.nombre + '</option>');
                                    } else {
                                        $('#ciudad').append(' <option value=' + v.id + '>' + v.nombre + '</option>');
                                    }

                                });
                            }
                        }
                    });
                }

                function buscardatosproveedor() {
                    $("#contenedorproveedor").LoadingOverlay("show");
                    var aux = $('#proveedores').val();
                    var aux2 = $('#sucursalesproveedores').val();
                    if (aux != 0 && aux2 != 0) {
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/datosdelproveedor",
                            data: {id_proveedor: aux, id_sucursal: aux2, api_key2: "API_1_2_5a4492f2d5137"},
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    limpiarproveedor();
                                    $("#botoneditarproveedor").removeAttr("disabled");
                                    $('#id_sucursal').val(aux2);
                                    $('#id_proveedor').val(aux);
                                    $('#tipoidentificacion').val(resp.datos.tipoidentificacion);
                                    $("#identificacion").val(resp.datos.identificacion);
                                    $("#nombrerazonsocial").val(resp.datos.nombrerazonsocial);
                                    $("#direccion").val(resp.datos.direccion);
                                    $("#telefono").val(resp.datos.telefono);
                                    $("#celular").val(resp.datos.celular);
                                    $("#correo").val(resp.datos.correo);
                                    $('#sucursal').val(resp.datos.nombresucursal);
                                    $('#codigosucursal').val(resp.datos.codigosucursal);
                                    $('#provincia').val(resp.datos.id_provincia);
                                    buscarciudadproveedor(resp.datos.id_ciudad);

                                    $("#contenedorproveedor").LoadingOverlay("hide");

                                } else {
                                    limpiarproveedor();
                                    $("#contenedorproveedor").LoadingOverlay("hide");
                                }
                            }
                        })
                    } else {
                        limpiarproveedor();
                    }
                }

                function nuevoproveedor() {
                    $("#tipo_evento_proveedor").val("nuevo");
                    $("#botoneditarproveedor").attr("disabled", "disabled");
                    $('#proveedores').empty();
                    limpiarproveedor();
                    desbloquearproveedor();
                    $('#identificacion').val("");
                    $('#identificacion').focus();
                }

                function editarproveedor() {
                    $("#tipo_evento_proveedor").val("editar");
                    $("#botoneditarproveedor").attr("disabled", "disabled");
                    desbloquearproveedor();
                    $('#identificacion').focus();
                }


                function limpiarproveedor() {
                    $("#id_proveedor").val('');
                    $("#id_sucursal").val('');
                    $('#tipoidentificacion').val('05');
                    $("#identificacion").val('');
                    $("#nombrerazonsocial").val('');
                    $("#direccion").val('');
                    $("#telefono").val('');
                    $("#celular").val('');
                    $("#correo").val('');
                    $('#sucursal').val('Matriz');
                    $('#codigosucursal').val('001');
                    $('#provincia').val('999');
                    $('#ciudad').empty();
                    $('#ciudad').append('<option selected="true" value="999">Sin Especificar</option>');
                }

                function desbloquearproveedor() {

                    $('#tipoidentificacion').removeAttr('disabled');
                    $("#identificacion").removeAttr('disabled');
                    $("#nombrerazonsocial").removeAttr('disabled');
                    $("#direccion").removeAttr('disabled');
                    $("#telefono").removeAttr('disabled');
                    $("#celular").removeAttr('disabled');
                    $("#correo").removeAttr('disabled');
                    $('#sucursal').removeAttr('disabled');
                    $('#codigosucursal').removeAttr('disabled');
                    $('#provincia').removeAttr('disabled');
                    $('#ciudad').removeAttr('disabled');
                }

                function bloquearproveedor() {

                    $('#tipoidentificacion').attr('disabled', 'disabled');
                    $("#identificacion").attr('disabled', 'disabled');
                    $("#nombrerazonsocial").attr('disabled', 'disabled');
                    $("#direccion").attr('disabled', 'disabled');
                    $("#telefono").attr('disabled', 'disabled');
                    $("#celular").attr('disabled', 'disabled');
                    $("#correo").attr('disabled', 'disabled');
                    $('#sucursal').attr('disabled', 'disabled');
                    $('#codigosucursal').attr('disabled', 'disabled');
                    $('#provincia').attr('disabled', 'disabled');
                    $('#ciudad').attr('disabled', 'disabled');
                }
            });
        </script>


        <div class="box">
            <div class="box-header with-border">

                <h3 class="box-title">Productos y Servicios</h3>
                <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-productos">
                    <i class="fa fa-search"></i> Buscar / Añadir
                </button>
                <div class="modal fade" id="modal-productos" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Productos y Servicios</h4>

                            </div>
                            <div class="modal-body">
                                <div class="col col-md-12">
                                    <div class="row efectobus">

                                        <div class="col col-md-3">
                                            Producto
                                            <div class="input-group">
                                                <input id="criterioproducto" class="form-control" type="text" value="" placeholder="Ingrese su Busqueda">
                                                <span class="input-group-btn">
                            <button id="botonbuscarextrabotonproducto" type="button" class="btn btn-primary btn-flat"><i class="fa  fa-search"></i></button>
                          </span>
                                            </div>


                                        </div>
                                        <div class="col col-md-3">
                                            Seleccione un Resultado
                                            <select class="form-control" id="productos">
                                            </select>
                                        </div>

                                        <div class="col col-md-3">
                                            Bodega
                                            <select class="form-control" name="bodegas" id="bodegas"><option value="1">Principal</option><option value="27">Prueba bodega</option><option value="28">oTRA BODEGA</option><option value="29">Keyla</option><option value="30">Nadia Perez</option><option value="31">Bodega Manta</option></select>
                                        </div>
                                        <div class="col col-md-3">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-success" onclick="" id="botonnuevoproducto">Nuevo Producto/Servicio
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12  ">
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Codigo</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input name="producto_id" type="hidden" class="form-control " placeholder="" id="producto_id">
                                            <input name="producto_unidad" type="hidden" class="form-control " placeholder="" id="producto_unidad">
                                            <input name="producto_codigoauxiliar" type="hidden" class="form-control " placeholder="" id="producto_codigoauxiliar">
                                            <input name="producto_codigo" type="text" class="form-control requerido" placeholder="" id="producto_codigo" disabled="disabled">
                                            <input name="producto_icedefecto" type="hidden" class="form-control " placeholder="" id="producto_icedefecto">
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                <span class="input-group-addon">
                    <b>Precio</b> &nbsp;
                    <input type="radio" checked="" value="1" name="producto_opcionprecio" id="producto_opcionprecio1"></span>
                                                <input name="producto_precio1" type="text" class="form-control requerido validador_numero5 usd5" placeholder="" id="producto_precio1">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Nombre</label>
                                        </div>
                                        <div class="col-md-5">
            <textarea class="form-control requerido validador_texto300" name="producto_nombre" id="producto_nombre" disabled="disabled">
            </textarea>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Descripción</label>
                                        </div>
                                        <div class="col-md-5">
             <textarea class="form-control validador_texto300" name="producto_descripcion" id="producto_descripcion" disabled="disabled">
            </textarea>
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Tipo Producto</label>
                                        </div>
                                        <div class="col-md-1">

                                            <select class="form-control requerido" name="producto_tipoproducto" id="producto_tipoproducto" disabled="disabled">
                                                <option value="1" selected="">Bien</option>
                                                <option value="2">Servicio</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Tipo Iva</label>
                                        </div>
                                        <div class="col-md-1">
                                            <select class="form-control requerido" name="producto_tipoiva" id="producto_tipoiva" disabled="disabled">
                                                <option value="2" selected=""> 12% </option>
                                                <option value="0"> 0% </option>
                                                <option value="6"> No Objeto de Impuesto </option>
                                                <option value="7"> Exento de Iva </option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">ICE</label>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="producto_tipoice" id="producto_tipoice" disabled="disabled"><option value="0">Seleccione Ice</option><option value="3011">3011 - Cigarrillos rubio </option><option value="3021">3021 - Cigarrillos negros </option><option value="3023">3023 - Productos del tabaco y suced&nbsp;n...</option><option value="3031">3031 - Bebidas alcoh¢licas, distintas...</option><option value="3041">3041 - Cerveza Industrial </option><option value="3043">3043 - Cerveza artesanal</option><option value="3053">3053 - Bebidas Gaseosas con alto cont...</option><option value="3054">3054 - Bebidas Gaseosas con bajo cont...</option><option value="3072">3072 - Camionetas, furgonetas, camion...</option><option value="3073">3073 - Veh¡culos motorizados cuyo pre...</option><option value="3074">3074 - Veh¡culos motorizados, excepto...</option><option value="3075">3075 - Veh¡culos motorizados, cuyo pr...</option><option value="3077">3077 - Veh¡culos motorizados, cuyo pr...</option><option value="3078">3078 - Veh¡culos motorizados cuyo pre...</option><option value="3079">3079 - Veh¡culos motorizados cuyo pre...</option><option value="3080">3080 - Veh¡culos motorizados cuyo pre...</option><option value="3081">3081 - Aviones, avionetas y helic¢pte...</option><option value="3092">3092 - Servicios de televisi¢n pagada</option><option value="3093">3093 - Servicios de Telefon¡a </option><option value="3101">3101 - Bebidas energizantes</option><option value="3111">3111 - Bebidas no alcoh¢licas </option><option value="3171">3171 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3172">3172 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3173">3173 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3174">3174 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3175">3175 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3176">3176 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3531">3531 - Bebidas  alcoh¢licas SENAE</option><option value="3541">3541 - Cerveza Industrial SENAE</option><option value="3542">3542 - Cigarrillos rubio SENAE</option><option value="3543">3543 - Cigarrillos negros SENAE</option><option value="3545">3545 - Cerveza artesanal SENAE</option><option value="3552">3552 - Bebidas Gaseosas con alto cont...</option><option value="3553">3553 - Bebidas Gaseosas con bajo cont...</option><option value="3601">3601 - Bebidas energizantes SENAE</option><option value="3602">3602 - Bebidas no alcoh¢licas SENAE</option><option value="3610">3610 - Perfumes y aguas de tocador</option><option value="3620">3620 - Videojuegos </option><option value="3630">3630 - Armas de fuego, armas deportiv...</option><option value="3640">3640 - Focos incandescentes excepto a...</option><option value="3650">3650 - Servicios de casinos, salas de...</option><option value="3660">3660 - Las cuotas, membres¡as, afilia...</option><option value="3670">3670 - Cocinas, calefones y otros de...</option><option value="3680">3680 - Ice Fundas Plásticas</option><option value="3770">3770 - Cocinas, calefones y otros de...</option></select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Porc Ice</label>
                                        </div>
                                        <div class="col-md-1">
                                            <input name="producto_porcice" type="number" class="form-control" placeholder="0" id="producto_porcice" disabled="disabled">
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Irbpnr</label>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="producto_irbpnr" id="producto_irbpnr" disabled="disabled"><option value="0">Seleccione Irbpnr</option><option value="1">5001 - Botellas Plásticas No Retornab...</option></select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar
                                            </button>
                                            <button type="button" id="botonguardarproductoyanadirarticulo" class="btn btn-primary" disabled="disabled">
                                                Añadir al Carro
                                            </button>
                                            <button type="button" id="botonanadiralcarrito" class="btn btn-primary"> Añadir
                                                y
                                                Continuar
                                            </button>
                                            <button type="button" id="botonanadiralcarritoycerrar" class="btn btn-success">
                                                Añadir y
                                                Cerrar
                                            </button>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">


                                        </div>
                                        <div class="col-md-1" style="font-size: 15px;">
                                            Cantidad
                                        </div>
                                        <div class="col-md-2">
                                            <input name="cantidad_carro" type="text" class="form-control validador_numero5 usd5" placeholder="" id="cantidad_carro" value="1">
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead style="width:100%;">
                            <tr>
                                <th style="width:130px;">Código</th>
                                <th>Nombre</th>
                                <th>Detalle</th>
                                <th style="width:70px;">Cantidad</th>
                                <th style="width:90px;">P.Unitario</th>
                                <th style="width:75px;">Desc</th>
                                <th style="width:75px;">Subtotal</th>
                                <th style="width:35px;">Ice</th>
                                <th style="width:75px;">Iva</th>
                                <th style="width:90px;">Total</th>
                                <th style="width:20px;"><i class="fa fa-times-circle anadir"></i></th>
                            </tr>

                            </thead>
                            <tbody id="detalleventa">


                            </tbody>
                        </table>
                    </div>
                </div>


                <script>


                    $(document).ready(function () {
                        function revisarsiestagregado(id) {
                            var cantidad = 0;
                            $('#detalleventa tr').each(function (index) {
                                id_producto = $(this).find('#id').val();
                                if (id_producto == id) {
                                    cantidad = cantidad + 1;
                                }
                            });
                            if (cantidad == 0) {
                                return false;
                            } else {
                                return true;
                            }
                        }

                        bloqueararticulo();
                        $("#botonanadiralcarrito").click(function () {
                            if (anadirarticulo() == true) {
                                limpiararticulo();
                                $('#criterioproducto').val("");
                                $('#criterioproducto').focus();
                                swal({
                                    type: 'success',
                                    title: 'Agregado',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }

                        });
                        $("#botonanadiralcarritoycerrar").click(function () {
                            if (anadirarticulo() == true) {
                                limpiararticulo();
                                $('#criterioproducto').val("");
                                $('#modal-productos').modal('toggle');
                                swal({
                                    type: 'success',
                                    title: 'Agregado',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        });
                        $("#botonnuevoproducto").click(function () {
                            limpiararticulo();
                            $('#criterioproducto').val("");
                            $('#productos').empty();
                            desbloqueararticulo();
                            $('#producto_codigo').focus();
                        });
                        $("#botonguardarproductoyanadirarticulo").click(function () {
                            if (validadorarticulo() == true) {
                                guardarproducto();
                                $('#criterioproducto').val("");
                            }
                        });






                        var input = document.getElementById("criterioproducto"),
                            intervaloproducto;
                        input.addEventListener("keyup", function () {
                            clearInterval(intervaloproducto);
                            intervaloproducto = setInterval(function () { //Y vuelve a iniciar
                                limpiararticulo();
                                buscarproductos();
                                clearInterval(intervaloproducto); //Limpio el intervalo
                            }, 600);
                        }, false);

                        // $('#criterioproducto').keyup(function () {
                        //     limpiararticulo();
                        //     if ($(this).val().length >= 3) {
                        //         buscarproductos();
                        //     }
                        //
                        // });

                        $('#botonbuscarextrabotonproducto').click(function () {
                            var criterio = $('#criterioproducto').val();
                            if (criterio != "") {
                                buscarproductos();
                            } else {
                                aviso("error", "Debe Ingresar por lo menos un digito para buscar.", "", "");
                            }
                        });

                        /*
                        $('#criterioproducto').change(function () {
                            buscarproductos();
                        })
                        */

                        function validadorarticulo() {
                            var cantidaderrores = 0;
                            var errores = [];
                            var codigo = $('#producto_codigo').val();
                            if (codigo == "" || codigo == null) {
                                errores.push("El Codigo no puede Estar Vacio");
                                cantidaderrores = cantidaderrores + 1;
                            }
                            var producto_opcionprecio = $('input:radio[name=producto_opcionprecio]:checked').val();
                            var producto_precio1 = $('#producto_precio1').val();
                            var producto_precio2 = $('#producto_precio2').val();
                            var producto_precio3 = $('#producto_precio3').val()
                            var producto_nombre = $('#producto_nombre').val()
                            var producto_descripcion = $('#producto_descripcion').val()
                            if (producto_opcionprecio == 1) {
                                if (producto_precio1 == "" || producto_precio1 == null) {
                                    errores.push("El Precio Seleccionado no puede Estar Vacio");
                                    cantidaderrores = cantidaderrores + 1;
                                }
                            } else if (producto_opcionprecio == 2) {
                                if (producto_precio2 == "" || producto_precio2 == null) {
                                    errores.push("El Precio Seleccionado no puede Estar Vacio");
                                    cantidaderrores = cantidaderrores + 1;
                                }
                            } else if (producto_opcionprecio == 3) {
                                if (producto_precio3 == "" || producto_precio3 == null) {
                                    errores.push("El Precio Seleccionado no puede Estar Vacio");
                                    cantidaderrores = cantidaderrores + 1;
                                }
                            }

                            if (producto_nombre == "") {
                                errores.push("El Nombre no puede Estar Vacio");
                                cantidaderrores = cantidaderrores + 1;
                            }


                            if (cantidaderrores == 0) {
                                return true;

                            } else {
                                var mensajehtml = "";
                                mensajehtml += "<ul>";
                                $.each(errores, function (key, value) {
                                    mensajehtml += "<li>" + value + "</li>";
                                });
                                mensajehtml += "</ul>";
                                $.notify({
                                    title: "<strong>Errores</strong> ",
                                    message: mensajehtml,
                                }, {
                                    type: 'danger',
                                    mouse_over: 'pause'
                                });

                                return false;
                            }
                        }

                        function anadirarticulo() {

                            if (validadorarticulo() == true) {

                                var producto_id = $('#producto_id').val();
                                var producto_codigo = $('#producto_codigo').val();
                                var producto_codigoauxiliar = $('#producto_codigoauxiliar').val();
                                var producto_opcionprecio = $('input:radio[name=producto_opcionprecio]:checked').val();
                                var producto_precio1 = parseFloat($('#producto_precio1').val());
                                var producto_nombre = $('#producto_nombre').val();
                                var producto_descripcion = $('#producto_descripcion').val();
                                var producto_tipoproducto = $('#producto_tipoproducto').val();
                                var producto_tipoiva = $('#producto_tipoiva').val();
                                var producto_tipoice = $('#producto_tipoice').val();
                                var producto_porcice = $('#producto_porcice').val();
                                var producto_icedefecto = $('#producto_icedefecto').val();
                                var producto_irbpnr = $('#producto_irbpnr').val();
                                var producto_unidad = $('#producto_unidad').val();

                                var cantidad_carro = parseFloat($('#cantidad_carro').val());
                                if (cantidad_carro == 0 || cantidad_carro == "") {
                                    cantidad_carro = 1;
                                }

                                var precio = 0;








                                if (producto_opcionprecio == 1) {
                                    if (producto_precio1 != "" && producto_precio1 != null) {
                                        precio = producto_precio1;
                                    } else {
                                        precio = 0;
                                    }
                                }

                                var producto_cantidad = cantidad_carro;
                                var descuento = 0;


                                var subantesdescuento = (producto_cantidad * precio);

                                var subtotal = subantesdescuento - descuento;

                                if (producto_porcice == "") {
                                    var ice = 0;
                                } else {
                                    var ice = subtotal * (producto_porcice / 100);
                                }
                                var opcionice = '';
                                if ((producto_porcice == "" || producto_porcice == "0") && producto_tipoice != "") {
                                    opcionice = '';
                                } else {
                                    opcionice = 'disabled="true"';
                                }

                                var porc_iva = funcion_tipoiva(producto_tipoiva);

                                var iva = (subtotal + ice) * (porc_iva / 100);

                                var total = (subtotal + ice + iva);


                                $("#detalleventa").append('<tr>' +
                                    '<td>' +
                                    '<input type="hidden"  value="' + producto_id + '" id="id">' +
                                    '<input type="hidden"  value="' + producto_unidad + '" id="id_unidad">' +
                                    '<input type="hidden"  value="' + producto_codigoauxiliar + '" id="codigo_adicional">' +
                                    '<input type="hidden"  value="' + producto_icedefecto + '" id="iceunitario">' +
                                    '<input type="hidden"  value="' + producto_tipoproducto + '" id="tipo_producto_item">' +
                                    '<input style="width:130px;" type="text" disabled="true" class="form-control" value="' + producto_codigo + '" id="codigo">' +
                                    '</td>' +
                                    '<td><textarea class="form-control validador_texto300" id="nombre" disabled>' + producto_nombre + '</textarea></td>' +
                                    '<td><textarea class="form-control validador_texto300" id="detalle" >' + producto_descripcion + '</textarea></td>' +
                                    '<td><input style="width:70px;"type="text" class="form-control validador_numero5 usd5" value="' + producto_cantidad + '" id="cantidad"></td>' +
                                    '<td><input style="width:90px;" type="text" class="form-control validador_numero5 usd5"  value="' + precio + '" id="precio"></td>' +
                                    '<td><input type="text" style="width:75px;" class="form-control validador_numero2 usd2" value="' + redondear(descuento) + '" id="descuento"></td>' +
                                    '<td><input type="text" style="width:75px;" disabled="true" class="form-control" value="' + redondear(subtotal) + '" id="subtotal"></td>' +
                                    '<td>' +
                                    '<div class="input-group">' +
                                    '   <input style="width:35px;" type="hidden" disabled="true"  class="form-control" value="' + producto_tipoice + '" id="codigo_ice"> ' +
                                    '   <span class="input-group-addon"><input style="width:35px;" type="text" disabled="true"  class="form-control" value="' + producto_porcice + '" id="porc_ice"></span> ' +
                                    '   <input style="width:65px;" type="text" disabled="true"  ' + opcionice + '   class="form-control  validador_numero2 usd2" value="' + redondear(ice) + '" id="ice">' +
                                    '</div>' +
                                    '</td>' +
                                    '<td>' +
                                    '<div class="btn-group">' +
                                    '   <input style="width:35px;" type="hidden" disabled="true"  class="form-control" value="' + producto_tipoiva + '" id="tipo_iva"> ' +
                                    '   <input style="width:35px;" type="hidden" disabled="true"  class="form-control" value="' + porc_iva + '" id="porc_iva"> ' +
                                    '   <input style="width:75px;" type="text" disabled="true"  class="form-control" value="' + redondear(iva) + '" id="iva">' +
                                    '</div>' +
                                    '</td>' +
                                    '<td><input style="width:90px;" type="text" disabled="true" class="form-control" value="' + redondear2(total) + '" id="total"></td>' +
                                    '<td><a id="eliminaritem"><i class="fa fa-times-circle anadir"></i></a></td>' +
                                    '</tr>');

                                $('#productos').empty();
                                bloqueararticulo();
                                limpiararticulo();
                                actualizartotales();

                                return true;
                            } else {
                                return false;
                            }
                        }


                        function bloqueararticulo() {
                            $('#producto_codigo').attr('disabled', 'disabled');
                            $('#producto_nombre').attr('disabled', 'disabled');
                            $('#producto_descripcion').attr('disabled', 'disabled');
                            $('#producto_tipoproducto').attr('disabled', 'disabled');
                            $('#producto_tipoiva').attr('disabled', 'disabled');
                            $('#producto_tipoice').attr('disabled', 'disabled');
                            $('#producto_porcice').attr('disabled', 'disabled');
                            $('#producto_irbpnr').attr('disabled', 'disabled');
                            $('#botonguardarproductoyanadirarticulo').attr('disabled', 'disabled');
                            $('#producto_categoria').attr('disabled', 'disabled');
                            $('#botonanadiralcarrito').removeAttr('disabled');
                            $('#botonanadiralcarritoycerrar').removeAttr('disabled');

                        }

                        function desbloqueararticulo() {
                            $('#producto_codigo').removeAttr('disabled');
                            $('#producto_nombre').removeAttr('disabled');
                            $('#producto_descripcion').removeAttr('disabled');
                            $('#producto_tipoproducto').removeAttr('disabled');
                            $('#producto_tipoiva').removeAttr('disabled');
                            $('#producto_tipoice').removeAttr('disabled');
                            $('#producto_porcice').removeAttr('disabled');
                            $('#producto_irbpnr').removeAttr('disabled');
                            $('#producto_categoria').removeAttr('disabled');
                            $('#botonguardarproductoyanadirarticulo').removeAttr('disabled');
                            $('#botonanadiralcarrito').attr('disabled', 'disabled');
                            $('#botonanadiralcarritoycerrar').attr('disabled', 'disabled');
                        }

                        function limpiararticulo() {
                            $('#producto_id').val("");
                            $('#producto_unidad').val("");
                            $('#producto_icedefecto').val("");

                            $('#cantidad_carro').val(1);
                            $('#producto_codigo').val("");
                            $('#producto_codigoauxiliar').val("");
                            $('#producto_icedefecto').val("");
                            $('#producto_precio1').val("");
                            $('#producto_unidad').val("");
                            $('#producto_nombre').val("");
                            $('#producto_descripcion').val("");
                            $('#producto_tipoproducto').val(1);
                            $('#producto_tipoiva').val(2);
                            $('#producto_tipoice').val(0);
                            $('#producto_porcice').val("");
                            $('#producto_irbpnr').val(0);
                        }

                        function buscarproductos() {
                            var criterio = $('#criterioproducto').val();
                            var id_bodega = $('#producto_bodega').val();
                            bloqueararticulo();
                            $.ajax({
                                type: 'get',
                                url: "https://azur.com.ec/plataforma/listadoproductos",
                                data: {
                                    criterio: criterio,
                                    api_key2: "API_1_2_5a4492f2d5137",
                                    id_bodega: id_bodega,
                                    demo: "1"
                                },
                                success: function (resp) {
                                    if (resp.respuesta == true) {
                                        $('#productos').empty();
                                        $(resp.datos).each(function (i, v) {

                                            $('#productos').append(' <option value=' + v.id + '>' + v.nombre  + '</option>');
                                        });
                                        $('#bodegas').empty();
                                        $('#bodegas').append(' <option selected="true" value="1">Principal</option>');
                                        buscardatosproductos();
                                    }
                                }
                            })
                        }

                        $('#productos').change(function () {
                            buscardatosproductos();
                        })

                        function buscardatosproductos() {

                            var aux = $('#productos').val();
                            var aux2 = $('#bodegas').val();

                            bloqueararticulo();
                            if (aux != 0 && aux2 != 0) {
                                $.ajax({
                                    type: 'POST',
                                    url: "https://azur.com.ec/plataforma/datosdelproducto",
                                    data: {id_producto: aux, id_bodega: aux2, api_key2: "API_1_2_5a4492f2d5137"},
                                    success: function (resp) {
                                        if (resp.respuesta == true) {
                                            limpiararticulo();
                                            $('#producto_id').val(resp.datos.id);
                                            $('#producto_codigo').val(resp.datos.codigo);
                                            $('#producto_codigoauxiliar').val(resp.datos.codigoauxiliar);
                                            $('#producto_icedefecto').val(resp.datos.icedefecto);
                                            $("#producto_precio1").val(resp.datos.costo);
                                            $("#producto_nombre").val(resp.datos.nombre);
                                            $('#producto_tipoice').val(resp.datos.ice);
                                            $('#producto_porcice').val(resp.datos.tarifa_ice);
                                            $('#producto_descripcion').val(resp.datos.detalle1);
                                            $('#producto_irbpnr').val(resp.datos.irbpnr);
                                            $('#producto_tipoiva').val(resp.datos.tipoiva);
                                            $('#producto_tipoproducto').val(resp.datos.tipoproducto);
                                            $('#producto_unidad').val(resp.datos.unidad);
                                        }
                                    }
                                })
                            } else {
                                limpiararticulo();
                            }
                        }


                        function guardarproducto() {
                            var producto = {
                                "producto_id": $('#producto_id').val(),
                                "producto_codigo": $('#producto_codigo').val(),
                                "producto_precio1": $('#producto_precio1').val(),
                                "producto_nombre": $('#producto_nombre').val(),
                                "producto_descripcion": $('#producto_descripcion').val(),
                                "producto_tipoproducto": $('#producto_tipoproducto').val(),
                                "producto_tipoiva": $('#producto_tipoiva').val(),
                                "producto_tipoice": $('#producto_tipoice').val(),
                                "producto_porcice": $('#producto_porcice').val(),
                                "producto_irbpnr": $('#producto_irbpnr').val(),
                            };

                            anadirarticulo();
                            bloqueararticulo();
                            $('#modal-productos').modal('toggle');

                        }

                        $('#detalleventa').on('change', 'input', function () {

                            var fila = $("input").parents('tr');

                            $(fila).each(function (index) {

                                // ubicamos en la sigueinte posicion porq enlasamos a un padre y pasa algo q lo manda desde el segundo

                                cantidad = parseFloat($(this).find('#cantidad').val());
                                if (cantidad < 0) {
                                    cantidad = 1;
                                    $(this).find('#cantidad').val(cantidad);
                                    aviso("error", "ERROR", ["La cantidad no puede ser negativa"], "");
                                }
                                precio = parseFloat($(this).find('#precio').val());
                                if (precio < 0) {
                                    precio = 0;
                                    $(this).find('#precio').val(precio);
                                    aviso("error", "ERROR", ["El precio no puede ser negativo"], "");
                                }
                                descuento = parseFloat($(this).find('#descuento').val());
                                if (descuento < 0) {
                                    descuento = 0;
                                    $(this).find('#descuento').val(descuento);
                                    aviso("error", "ERROR", ["El descuento no puede ser negativo"], "");
                                }
                                if ((cantidad * precio) >= descuento) {
                                } else {
                                    aviso("error", "ERROR", ["El descuento no puede ser mayor al subtotal"], "");
                                    descuento = 0;
                                    $(this).find('#descuento').val(descuento);
                                }

                                var subantesdescuento = (cantidad * precio);

                                var subtotal = subantesdescuento - descuento;


                                $(this).find('#subtotal').val(redondear(subtotal));

                                codigo_ice = $(this).find('#codigo_ice').val();
                                ice_manual = parseFloat($(this).find('#ice').val());
                                porc_ice = $(this).find('#porc_ice').val();
                                if ((porc_ice == 0 || porc_ice == "") && codigo_ice != "") {
                                    var ice = ice_manual;
                                } else {
                                    if (codigo_ice == "") {
                                        var ice = 0;
                                    } else {
                                        var ice = subtotal * porc_ice / 100;
                                        $(this).find('#ice').val(redondear(ice));
                                    }
                                }


                                porc_iva = $(this).find('#porc_iva').val();
                                var iva = (subtotal + ice) * porc_iva / 100;
                                $(this).find('#iva').val(redondear(iva));
                                var total = (subtotal + ice + iva);
                                $(this).find('#total').val(redondear2(total));
                                if (total < 0) {
                                    $.notify({
                                        title: "<strong>Error</strong><br> ",
                                        message: "Verifique el total de la factura no puede ser Negativo",
                                    }, {
                                        type: 'danger',
                                        mouse_over: 'pause'
                                    });
                                }

                            })
                            actualizartotales();
                        });


                        $('#detalleventa').on('click', '#eliminaritem', function () {
                            var fila = $(this).parents('tr');
                            fila.remove();
                            actualizartotales();
                        })


                    })
                </script>        </div>


        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Forma de Pago</h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" onclick="anadir_formapago()">
                            <i class="fa fa-plus"></i> Añadir
                        </button>
                    </div>
                    <div class="box-body">
                        <table>
                            <tbody class="tabladatosformadepago">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Forma de Pago</b>
                                        </div>
                                        <div class="col-md-8">
                                            <select id="formadepago" class="form-control" name="formadepago">
                                                <option value="01">Sin utilizacion del sistema financiero</option>
                                                <option value="16">Tarjetas de Debito</option>
                                                <option value="17">Dinero Electronico</option>
                                                <option value="18">Tarjeta Prepago</option>
                                                <option value="19">Tarjeta de Credito</option>
                                                <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>
                                                <option value="21">Endoso de Titulos</option>
                                                <option value="15">COMPENSACIÓN DE DEUDAS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Plazo</b>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input id="plazo" name="plazo" type="number" class="form-control" placeholder="Ejm: 30">
                                                </div>
                                                <div class="col-md-6">
                                                    <select id="tiempo" class="form-control" name="tiempo">
                                                        <option selected="" value="ninguno">ninguno</option>
                                                        <option value="dias">Dias</option>
                                                        <option value="meses">Meses</option>
                                                        <option value="anios">Años</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Valor</b>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control usd2" value="0.00" type="text" placeholder="" id="totalformapago">
                                        </div>
                                    </div>
                                </td>
                                <td><a id="eliminar_formapago"><i class="fa fa-times-circle anadir"></i></a></td>
                            </tr>
                            </tbody>

                        </table>


                    </div>
                </div>

                <script>
                    function anadir_formapago(){
                        $(".tabladatosformadepago").append('<tr><td>'+
                            '  <div class="row">\n' +
                            '                    <div class="col-md-4">\n' +
                            '                        <b>Forma de Pago</b>\n' +
                            '                    </div>\n' +
                            '                    <div class="col-md-8">\n' +
                            '                        <select id="formadepago" class="form-control" name="formadepago">\n' +
                            '\n' +
                            '                            <option value="01">Sin utilizacion del sistema financiero</option>\n' +
                            '                            <option value="16">Tarjetas de Debito</option>\n' +
                            '                            <option value="17">Dinero Electronico</option>\n' +
                            '                            <option value="18">Tarjeta Prepago</option>\n' +
                            '                            <option value="19">Tarjeta de Credito</option>\n' +
                            '                            <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>\n' +
                            '                            <option value="21">Endoso de Titulos</option>\n' +
                            '<option value="15">COMPENSACIÓN DE DEUDAS</option>\n'+
                            '                        </select>\n' +
                            '                    </div>\n' +
                            '                </div>\n' +
                            '                <div class="row">\n' +
                            '                    <div class="col-md-4">\n' +
                            '                        <b>Plazo</b>\n' +
                            '                    </div>\n' +
                            '\n' +
                            '                    <div class="col-md-8">\n' +
                            '                        <div class="row">\n' +
                            '                            <div class="col-md-6">\n' +
                            '                                <input id="plazo" name="plazo" type="number" class="form-control"\n' +
                            '                                       placeholder="Ejm: 30 dias">\n' +
                            '                            </div>\n' +
                            '                            <div class="col-md-6">\n' +
                            '                                <select id="tiempo" class="form-control" name="tiempo">\n' +
                            '                                    <option selected="" value="ninguno">ninguno</option>\n' +
                            '                                    <option value="dias">Dias</option>\n' +
                            '                                    <option value="meses">Meses</option>\n' +
                            '                                    <option value="anios">Años</option>\n' +
                            '                                </select>\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '\n' +
                            '                </div>\n' +
                            '                <div class="row">\n' +
                            '                    <div class="col-md-4">\n' +
                            '                        <b>Valor</b>\n' +
                            '                    </div>\n' +
                            '                    <div class="col-md-8">\n' +
                            '                        <input class="form-control usd2"  value="0.00" type="text"\n' +
                            '                               placeholder="" id="totalformapago">\n' +
                            '                    </div>\n' +
                            '                </div>'+
                            '</td>' +
                            '<td><a id="eliminar_formapago"><i class="fa fa-times-circle anadir"></i></a></td>'+
                            '</tr>');
                    }

                    $('.tabladatosformadepago').on('click','#eliminar_formapago', function() {
                        var fila = $(this).parents('tr');
                        fila.remove();
                    })
                </script>            <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Datos adicionales (Opcional) </h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-default1">
                            <i class="fa fa-plus"></i> Añadir
                        </button>
                    </div>
                    <div class="box-body ">
                        <table class=" table ">
                            <thead>
                            <tr><th>Nombre</th>
                                <th>Detalle</th>
                            </tr></thead>
                            <tbody id="tabladatosadicianales" class="tabladatosadicianales">

                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="modal-default1" style="display: none;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Datos Adicionales</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-2"><b>Nombre</b></div>
                                        <div class="col-md-4">
                                            <input id="nombre_adicional" name="nombre_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: Orden">
                                        </div>
                                        <div class="col-md-2"><b>Descripción</b></div>
                                        <div class="col-md-4">
                                            <input id="detalle_adicional" name="detalle_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: ABCD123">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

                                    <button class="btn  btn-primary " id="botonanadiracional"> Añadir Adicional</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    $( document ).ready(function() {
                        $("#botonanadiracional").click(function (){
                            var nombre=$("#nombre_adicional").val();
                            var detalle=$("#detalle_adicional").val();

                            var contador=0;
                            $('#tabladatosadicianales tr').each(function (index) {
                                contador=contador+1;
                            });

                            if(contador >9){
                                swal({
                                    type: 'error',
                                    title: 'Maximo se puede añadir 10 datos adicionales',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                return ;
                            }

                            if(nombre=="" && detalle==""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar Llenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else if(nombre=="" && detalle!=""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar LLenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else if(nombre!="" && detalle==""){
                                swal({
                                    type: 'error',
                                    title: 'Los dos Campos Nombre y Detalle Deben Estar LLenos',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }else{
                                anadir_informacionadicional(nombre,detalle);
                                swal({
                                    type: 'success',
                                    title: 'Correcto',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                limpiar_informacionadicional();
                                $('#modal-default1').modal('toggle');
                            }
                        })

                        function limpiar_informacionadicional(){
                            $("#nombre_adicional").val("");
                            $("#detalle_adicional").val("");
                        }

                        function anadir_informacionadicional(nombre,detalle){


                            $(".tabladatosadicianales").append('<tr>'+
                                '<td><input type="text" disabled="true" class="form-control" value="'+ nombre +'" id="nombre"></td>'+
                                '<td><input type="text" disabled="true" class="form-control" value="'+ detalle +'" id="detalle"></td>'+
                                '<td><a id="eliminar_informacionadicional"><i class="fa fa-times-circle anadir"></i></a></td>'+
                                '</tr>');




                        }

                        $('.tabladatosadicianales').on('click','#eliminar_informacionadicional', function() {
                            var fila = $(this).parents('tr');
                            fila.remove();
                        })
                    });
                </script>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Totales</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal sin
                                        Impuestos</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_sinimpuestos"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">


                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal Iva
                                        12%</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_iva12"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal 0%</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_ivacero"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal no objeto
                                        de iva</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_noobjeto"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal Exento de
                                        iva</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_exento"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Total
                                        descuento</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_descuento"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor ICE</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_ice"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor irbpnr</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_irbpnr"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Iva 12%</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_iva12"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor Total</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_valortotal"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>







        <center>
            <button type="button" class="btn btn-primary " onclick="enviar()"><i class="fa fa-save"></i> Guardar
            </button>
        </center>

        <script>


            secuencialpuntoemisionliquidacion();
            var secuencialliquidacion_puntoemision = setInterval('secuencialpuntoemisionliquidacion()', 60000);



            var auxcodestab = "001";
            var auxcodpunto = "002";
            $("#tipo_comprobante").change(function () {
                if ($("#tipo_comprobante").val() == "03" && $("#tipo_tecnologia").val() == "1") {
                    $("#codigo_establecimiento_documento").val(auxcodestab);
                    $("#codigo_puntoemision_documento").val(auxcodpunto);
                    $("#autorizacion_sri").prop('disabled', true);
                    secuencialpuntoemisionliquidacion();
                    var secuencialliquidacion_puntoemision = setInterval('secuencialpuntoemisionliquidacion()', 60000);
                }else{
                    $("#autorizacion_sri").prop('disabled', false);
                }
                if ($("#tipo_tecnologia").val() == "0") {
                    $("#codigo_establecimiento_documento").val("");
                    $("#codigo_puntoemision_documento").val("");
                    $("#secuencial_documento").val("");
                    $("#autorizacion_sri").prop('disabled', false);
                }
            });

            $("#tipo_tecnologia").change(function () {
                if ($("#tipo_comprobante").val() == "03" && $("#tipo_tecnologia").val() == "1") {
                    $("#codigo_establecimiento_documento").val(auxcodestab);
                    $("#codigo_puntoemision_documento").val(auxcodpunto);
                    $("#autorizacion_sri").prop('disabled', true);
                    secuencialpuntoemisionliquidacion();
                    var secuencialliquidacion_puntoemision = setInterval('secuencialpuntoemisionliquidacion()', 60000);
                }else{
                    $("#autorizacion_sri").prop('disabled', false);
                }
                if ($("#tipo_tecnologia").val() == "0") {
                    $("#codigo_establecimiento_documento").val("");
                    $("#codigo_puntoemision_documento").val("");
                    $("#secuencial_documento").val("");
                    $("#autorizacion_sri").prop('disabled', false);
                }
            });


            function secuencialpuntoemisionliquidacion() {
                var tipo = $("#tipo_tecnologia").val();
                var id_empresa = "1";
                var id_establecimiento = "1";
                var id_puntoemision = "16";
                var ambiente = "1";
                var secuencial = $("#secuencial_documento").val();
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/acciones/secuencialpuntoemision",
                    data: {
                        tipo: tipo,
                        id_empresa: id_empresa,
                        id_establecimiento: id_establecimiento,
                        id_puntoemision: id_puntoemision,
                        ambiente: ambiente,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {

                        if (resp.respuesta == true) {
                            $(resp.datos).each(function (index, element) {
                                var secuencialenbase = "";
                                if (ambiente == 1) {
                                    secuencialenbase = element.secuencia_liquidacion + 1;
                                } else if (ambiente == 2) {
                                    secuencialenbase = element.secuencia_liquidacion2 + 1;
                                }

                                if (secuencial == secuencialenbase) {

                                } else {
                                    if (secuencial == "" || secuencial == null) {
                                        $("#secuencial_documento").val(secuencialenbase);
                                    } else {
                                        if (secuencial > secuencialenbase) {
                                            $("#secuencial_documento").val(secuencialenbase);
                                            $.notify({
                                                title: "<strong>Cambio de Secuencial</strong> <br>",
                                                message: "<ul>" +
                                                    "<li>El Secuencial Ingresado no puede ser mayor al Registrado en la base de datos, Le asignamos un nuevo secuencial.</li>" +
                                                    "<li>Para Cambiar el Secuencial Inicial debe ir a Configuracion->Empresas </li>" +
                                                    "</ul>",
                                            }, {
                                                type: 'warning',
                                                mouse_over: 'pause'
                                            });
                                        } else {
                                            // $("#secuencial").val(secuencialenbase);
                                            // $.notify({
                                            //     title: "<strong>Cambio de Secuencial</strong> <br>",
                                            //     message: "<ul>" +
                                            //     "<li>El Secuencial Ingresado ya fue Ocupado o no Lo puede Ocupar, Le asignamos un nuevo secuencial.</li>" +
                                            //     "<li>Para Cambiar el Secuencial Inicial debe ir a Configuracion->Empresas </li>" +
                                            //     "</ul>",
                                            // }, {
                                            //     type: 'warning',
                                            //     mouse_over: 'pause'
                                            // });
                                        }


                                    }
                                }
                            });
                        } else {
                            console.log(resp);
                        }
                    }
                })
            }


            secuencialpuntoemision();

            function secuencialpuntoemision() {
                var tipo = $("#tipo_tecnologia").val();
                var id_empresa = "1";
                var id_establecimiento = "1";
                var id_puntoemision = "16";
                var ambiente = "1";
                var secuencial = $("#secuencial").val();
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/acciones/secuencialpuntoemision",
                    data: {
                        tipo: tipo,
                        id_empresa: id_empresa,
                        id_establecimiento: id_establecimiento,
                        id_puntoemision: id_puntoemision,
                        ambiente: ambiente,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {

                        if (resp.respuesta == true) {
                            $(resp.datos).each(function (index, element) {
                                var secuencialenbase = "";
                                if (ambiente == 1) {
                                    secuencialenbase = element.secuencia_compra + 1;
                                } else if (ambiente == 2) {
                                    secuencialenbase = element.secuencia_compra2 + 1;
                                }

                                if (secuencial == secuencialenbase) {

                                } else {
                                    if (secuencial == "" || secuencial == null) {
                                        $("#secuencial").val(secuencialenbase);
                                    } else {
                                        if (secuencial > secuencialenbase) {
                                            $("#secuencial").val(secuencialenbase);
                                            $.notify({
                                                title: "<strong>Cambio de Secuencial</strong> <br>",
                                                message: "<ul>" +
                                                    "<li>El Secuencial Ingresado no puede ser mayor al Registrado en la base de datos, Le asignamos un nuevo secuencial.</li>" +
                                                    "<li>Para Cambiar el Secuencial Inicial debe ir a Configuracion->Empresas </li>" +
                                                    "</ul>",
                                            }, {
                                                type: 'warning',
                                                mouse_over: 'pause'
                                            });
                                        } else {
                                            // $("#secuencial").val(secuencialenbase);
                                            // $.notify({
                                            //     title: "<strong>Cambio de Secuencial</strong> <br>",
                                            //     message: "<ul>" +
                                            //     "<li>El Secuencial Ingresado ya fue Ocupado o no Lo puede Ocupar, Le asignamos un nuevo secuencial.</li>" +
                                            //     "<li>Para Cambiar el Secuencial Inicial debe ir a Configuracion->Empresas </li>" +
                                            //     "</ul>",
                                            // }, {
                                            //     type: 'warning',
                                            //     mouse_over: 'pause'
                                            // });
                                        }


                                    }
                                }
                            });
                        } else {
                            console.log(resp);
                        }
                    }
                })
            }

            var secuencial_puntoemision = setInterval('secuencialpuntoemision()', 60000);


            function actualizartotales() {
                var subtotal_sinimpuestos = 0;
                var subtotal_cero = 0;
                var subtotal_noobjeto = 0;
                var subtotal_exento = 0;
                var subtotal_doce = 0;
                var acumulador_descuento = 0;
                var acumulador_ice = 0;
                var acumulador_irbpnr = 0;
                var acumulador_iva = 0;
                var acumulador_valortotal = 0;
                var totaliva12 = 0;

                var acumulador_iva12 = 0;
                var acumulador_iva0 = 0;


                $('#detalleventa tr').each(function (index) {
                    id = $(this).find('#codigo').val();
                    nombre = $(this).find('#nombre').val();
                    detalle = $(this).find('#detalle').val();
                    cantidad = parseFloat($(this).find('#cantidad').val());
                    precio = parseFloat($(this).find('#precio').val());
                    descuento = parseFloat($(this).find('#descuento').val());
                    subtotal = parseFloat($(this).find('#subtotal').val());
                    codigo_ice = $(this).find('#codigo_ice').val();
                    porc_ice = $(this).find('#porc_ice').val();
                    ice = parseFloat($(this).find('#ice').val());
                    tipo_iva = $(this).find('#tipo_iva').val();
                    iva = parseFloat($(this).find('#iva').val());
                    total = parseFloat($(this).find('#total').val());

                    subtotal_sinimpuestos = subtotal_sinimpuestos + parseFloat(redondear(subtotal));

                    if (tipo_iva == 0) {
                        subtotal_cero = subtotal_cero + parseFloat(redondear(subtotal));
                    } else if (tipo_iva == 6) {
                        subtotal_noobjeto = subtotal_noobjeto + parseFloat(redondear(subtotal));
                    } else if (tipo_iva == 7) {
                        subtotal_exento = subtotal_exento + parseFloat(redondear(subtotal));
                    } else if (tipo_iva == 2) {
                        subtotal_doce = (subtotal_doce + ice) + parseFloat(redondear(subtotal));
                    }
                    acumulador_descuento = acumulador_descuento + parseFloat(redondear(descuento));
                    acumulador_ice = acumulador_ice + ice;
                    // acumulador_iva = acumulador_iva + iva;
                });


                totaliva12 = subtotal_doce * 12 / 100;


                acumulador_valortotal = parseFloat(subtotal_sinimpuestos) + parseFloat(acumulador_ice) + parseFloat(totaliva12);

                $('#totales_subtotal_sinimpuestos').val(redondear(subtotal_sinimpuestos));
                $('#totales_subtotal_iva12').val(redondear(subtotal_doce));
                $('#totales_subtotal_ivacero').val(redondear(subtotal_cero));
                $('#totales_subtotal_exento').val(redondear(subtotal_exento));
                $('#totales_subtotal_noobjeto').val(redondear(subtotal_noobjeto));
                $('#totales_descuento').val(redondear(acumulador_descuento));
                $('#totales_ice').val(redondear(acumulador_ice));
                $('#totales_iva12').val(redondear(totaliva12));
                $('#totales_irbpnr').val(redondear(acumulador_irbpnr));
                $('#totales_valortotal').val(redondear2(acumulador_valortotal));

                var totaldeformas = 0;
                $('.tabladatosformadepago tr').each(function (index) {
                    totaldeformas = totaldeformas + 1;
                });
                if (totaldeformas <= 1) {
                    $('#totalformapago').val(redondear2(acumulador_valortotal));
                }


            }


            function enviar() {
                actualizartotales();


                var reembolso = 0;
                var totalreembolso = 0;
                if ($('#reembolso').prop('checked')) {
                    reembolso = 1;
                    var auxacure = 0;
                    $('#tablareembolsos tr').each(function (index) {
                        auxacure = auxacure + 1;
                    });
                    if (auxacure == 0) {
                        aviso("error", "ERROR", ["Tiene marcado que aplica reembolso y no tiene agregado ninguno"], "");
                        return;

                    }
                    totalreembolso = $('#totales_reembolso').val();
                } else {
                    reembolso = 0;
                }


                if ($("#sustento_comprobante").val() == "vacio" || $("#sustento_comprobante").val() == "") {
                    aviso("error", "ERROR", ["El sustento tributario  no puede estar vacio"], "");
                    $("#sustento_comprobante").focus();
                    return;
                }

                if ($("#tipo_comprobante").val() == "vacio" || $("#tipo_comprobante").val() == "") {
                    aviso("error", "ERROR", ["El tipo de comprobante de sustento no puede estar vacio"], "");
                    $("#tipo_comprobante").focus();
                    return;
                }

                if ($("#tipo_tecnologia").val() == "0") {
                    var conteoautorizacion = $("#autorizacion_sri").val().length;
                    if (conteoautorizacion == 10 || conteoautorizacion == 37 || conteoautorizacion == 49) {
                    } else {
                        aviso("error", "ERROR", ["Numero de autorizacion es Invalido"], "");
                        $("#tipo_tecnologia").focus();
                        return;
                    }
                }


                if ($("#tipo_tecnologia").val() == "1" && $("#autorizacion_sri").val() != "") {
                    var conteoautorizacion = $("#autorizacion_sri").val().length;
                    if (conteoautorizacion == 10 || conteoautorizacion == 37 || conteoautorizacion == 49) {
                    } else {
                        aviso("error", "ERROR", ["Numero de autorizacion es Invalido"], "");
                        return;
                    }
                }

                if ($("#secuencial_documento").val() <= "0") {
                    aviso("error", "ERROR", ["El secuencial no puede ser 0 o negativo"], "");
                    $("#secuencial_documento").focus();
                    return;
                }
                if ($("#secuencial_documento").val().length > 9) {
                    aviso("error", "ERROR", ["El secuencial no puede tener mas de 9 digitos"], "");
                    $("#secuencial_documento").focus();
                    return;
                }



                if ($("#tipo_comprobante").val() == "03" && $('#tipoidentificacion').val() == "04") {
                    aviso("error", "ERROR", ["Las Liquidaciones de Compra no pueden ser emitidas a para personas con RUC"], "");
                    $("#tipo_comprobante").focus();
                    return;
                }

                if ($("#tipo_tecnologia").val() == "0") {
                    if ($("#autorizacion_sri").val() == "") {
                        aviso("error", "ERROR", ["El numero de Autorización es obligatorio para las facturas físicas"], "");
                        $("#autorizacion_sri").focus();
                        return;
                    }
                }

                var cabecera = {
                    "fecha": $("#fecha").val(),
                    "tipo": $("#tipo_tecnologia").val(),
                    "id_empresa": "1",
                    "id_vendedor": "3",
                    "id_usuario": "1",
                    "id_establecimiento": "1",
                    "id_puntoemision": $("#punto_emision").val(),
                    "secuencial_compra": $("#secuencial").val(),
                    "ambiente": $("#ambiente").val(),
                    "bodega_compra": $("#bodega_compra").val(),
                    "tipo_comprobante": $("#tipo_comprobante").val(),
                    "fecha_documento": $("#fecha_documento").val(),
                    "codigo_establecimiento_documento": $("#codigo_establecimiento_documento").val(),
                    "codigo_puntoemision_documento": $("#codigo_puntoemision_documento").val(),
                    "secuencial_documento": $("#secuencial_documento").val(),
                    "descripcion": $("#descripcion").val(),
                    "sustento_comprobante": $("#sustento_comprobante").val(),
                    "autorizacion_sri": $("#autorizacion_sri").val(),
                    "fecha_documento_caducidad": $("#fecha_documento_caducidad").val(),
                    "vencimiento": $("#vencimiento").val(),
                    "fecha_ingreso_bodega": $("#fecha_ingreso_bodega").val(),
                    "es_gasto": $('input:radio[name=es_gasto]:checked').val(),
                    "iva_credito_tributario": $('input:radio[name=iva_credito_tributario]:checked').val(),
                    "ingreso_mercaderia": $('input:radio[name=ingreso_mercaderia]:checked').val(),

                    "editar": "NO",
                    "reembolso": reembolso,
                };


                var proveedor = {
                    "tipoidentificacion": $('#tipoidentificacion').val(),
                    "identificacion": $("#identificacion").val(),
                    "nombrerazonsocial": $("#nombrerazonsocial").val(),
                    "direccion": $("#direccion").val(),
                    "telefono": $("#telefono").val(),
                    "celular": $("#celular").val(),
                    "correo": $("#correo").val(),
                    "nombresucursal": $('#sucursal').val(),
                    "codigosucursal": $('#codigosucursal').val(),
                    "id_provincia": $('#provincia').val(),
                    "id_ciudad": $('#ciudad').val(),
                    "id_proveedor": $('#id_proveedor').val(),
                    "id_sucursal": $('#id_sucursal').val(),
                    "tipo_evento_proveedor": $("#tipo_evento_proveedor").val(),
                    "demo": "1",
                };

                var elementos = [];
                var id, id_bodega, codigo, codigoauxiliar, nombre, detalle, cantidad, precio, descuento, codigo_ice,
                    porc_ice, ice,
                    tipo_iva, iva, total, subtotal, tipo_producto;
                var vendedor_producto;

                $('#detalleventa tr').each(function (index) {

                    id = $(this).find('#id').val();
                    codigo = $(this).find('#codigo').val();
                    codigoauxiliar = $(this).find('#codigo_adicional').val();
                    nombre = $(this).find('#nombre').val();
                    detalle = $(this).find('#detalle').val();
                    cantidad = $(this).find('#cantidad').val();
                    subtotal = $(this).find('#subtotal').val();
                    precio = $(this).find('#precio').val();
                    descuento = $(this).find('#descuento').val();
                    codigo_ice = $(this).find('#codigo_ice').val();
                    porc_ice = $(this).find('#porc_ice').val();
                    ice = $(this).find('#ice').val();
                    tipo_iva = $(this).find('#tipo_iva').val();
                    iva = $(this).find('#iva').val();
                    total = $(this).find('#total').val();
                    tipo_producto = $(this).find('#tipo_producto_item').val();

                    elementos.push({
                        "id": id,
                        "codigo": codigo,
                        "codigoauxiliar": codigoauxiliar,
                        "nombre": nombre,
                        "detalle": detalle,
                        "cantidad": cantidad,
                        "precio": precio,
                        "descuento": descuento,
                        "codigo_ice": codigo_ice,
                        "porc_ice": porc_ice,
                        "ice": ice,
                        "tipo_iva": tipo_iva,
                        "iva": iva,
                        "total": total,
                        "subtotal": subtotal,
                        "tipoproducto": tipo_producto,
                    });
                });


                var totales = {
                    "subtotal_sinimpuestos": $('#totales_subtotal_sinimpuestos').val(),
                    "subtotal_iva12": $('#totales_subtotal_iva12').val(),
                    "subtotal_ivacero": $('#totales_subtotal_ivacero').val(),
                    "subtotal_exento": $('#totales_subtotal_exento').val(),
                    "subtotal_noobjeto": $('#totales_subtotal_noobjeto').val(),
                    "descuento": $('#totales_descuento').val(),
                    "totales_ice": $('#totales_ice').val(),
                    "totales_iva12": $('#totales_iva12').val(),
                    "totales_irbpnr": $('#totales_irbpnr').val(),
                    "totales_valortotal": $('#totales_valortotal').val(),
                    "totales_reembolso": totalreembolso,
                };


                // var formapago={
                //     "formadepago":$("#formadepago").val(),
                //     "plazo":$("#plazo").val(),
                //     "tiempo":$("#tiempo").val(),
                //     "totalformapago":$("#totalformapago").val(),
                // };
                var formasdepagos = [];
                var formadepago, plazo, tiempo, totalformapago;
                var acumuladorformadepago = 0;
                $('.tabladatosformadepago tr').each(function (index) {
                    formadepago = $(this).find('#formadepago').val();
                    plazo = $(this).find('#plazo').val();
                    if (plazo < 0) {
                        aviso("error", "ERROR", ["El plazo no puede ser negativo"], "");
                        return;
                    }
                    tiempo = $(this).find('#tiempo').val();
                    totalformapago = parseFloat($(this).find('#totalformapago').val());
                    if (totalformapago > 0) {
                        formasdepagos.push({
                            "formadepago": formadepago,
                            "plazo": plazo,
                            "tiempo": tiempo,
                            "totalformapago": totalformapago,
                        });
                        acumuladorformadepago = (acumuladorformadepago + totalformapago);
                    }
                });

                var adicionales = [];
                var nombre, detalle;
                $('#tabladatosadicianales tr').each(function (index) {
                    nombre = $(this).find('#nombre').val();
                    detalle = $(this).find('#detalle').val();
                    adicionales.push({
                        "nombre": nombre,
                        "detalle": detalle,
                    });
                });

                var reembolsos = [];
                var reembolso_id_proveedor, reembolso_codigoice, reembolso_porcice, reembolso_baseice, reembolso_proveedor,
                    reembolso_tipocomprobante, reembolso_secuencial;
                var reembolso_fechaemision, reembolso_autorizacion, reembolso_base0, reembolso_base12, reembolso_valorice,
                    reembolso_total;

                if (reembolso == 1) {
                    $('#tablareembolsos tr').each(function (index) {

                        reembolso_id_proveedor = $(this).find('#reembolso_id_proveedor').val();
                        reembolso_codigoice = $(this).find('#reembolso_codigoice').val();
                        reembolso_porcice = $(this).find('#reembolso_porcice').val();
                        reembolso_baseice = $(this).find('#reembolso_baseice').val();
                        reembolso_proveedor = $(this).find('#reembolso_proveedor').val();
                        reembolso_tipocomprobante = $(this).find('#reembolso_tipocomprobante').val();
                        reembolso_secuencial = $(this).find('#reembolso_secuencial').val();
                        reembolso_fechaemision = $(this).find('#reembolso_fechaemision').val();
                        reembolso_autorizacion = $(this).find('#reembolso_autorizacion').val();
                        reembolso_base0 = $(this).find('#reembolso_base0').val();
                        reembolso_base12 = $(this).find('#reembolso_base12').val();
                        reembolso_valorice = $(this).find('#reembolso_valorice').val();
                        reembolso_total = $(this).find('#reembolso_total').val();


                        reembolsos.push({
                            "id_proveedor": reembolso_id_proveedor,
                            "codigo_ice": reembolso_codigoice,
                            "porc_ice": reembolso_porcice,
                            "base_ice": reembolso_baseice,
                            "proveedor": reembolso_proveedor,
                            "tipo_comprobante": reembolso_tipocomprobante,
                            "secuencial": reembolso_secuencial,
                            "fechaemision": reembolso_fechaemision,
                            "autorizacion": reembolso_autorizacion,
                            "base0": reembolso_base0,
                            "base12": reembolso_base12,
                            "valorice": reembolso_valorice,
                            "total": reembolso_total,
                        });
                    });
                }


                if ($("#totales_valortotal").val() != redondear2(acumuladorformadepago)) {
                    aviso("error", "ERROR", ["El total de las Formas de Pago tiene q se igual al Total de la Compra"], "");
                } else if (($('#totales_valortotal').val() > 200 && $('#tipo_evento_proveedor').val() == "final")
                    || ($('#totales_valortotal').val() > 200 && $("#identificacion").val() == "9999999999999")
                    || ($('#totales_valortotal').val() > 200 && $("#identificacion").val() == "")) {
                    aviso("error", "ERROR", ["Las Compras Mayores a 200 dólares no pueden ser a Consumidor Final"], "");
                } else if (elementos.length == 0) {
                    aviso("error", "ERROR", ["No Puede estar Vacía la Compra"], "");
                } else {


                    swal({
                            title: "Esta Seguro que desea Procesar la Compra",
                            text: "Guardar",
                            type: "info",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                        },
                        function () {
                            $.ajax({
                                type: 'POST',
                                url: "https://azur.com.ec/plataforma/guardarcompra",
                                data: {
                                    cabecera: cabecera,
                                    proveedor: proveedor,
                                    productos: elementos,
                                    totales: totales,
                                    formasdepagos: formasdepagos,
                                    adicionales: adicionales,
                                    reembolsos: reembolsos,
                                    api_key2: "API_1_2_5a4492f2d5137"
                                },
                                success: function (resp) {
                                    if (resp.respuesta == true) {
                                        swal({title: "Excelente!", text: "Guardado y Procesando", type: "success"});
                                        location.href = "https://azur.com.ec/plataforma/compra/vertodos?id=" + resp.id;
                                    } else if (resp.respuesta == false) {
                                        console.log(resp);
                                        swal({title: "Oops!", text: resp.error, type: "error"});
                                        aviso("error", "ERROR", [resp.error], ["Contacte con Soporte Tecnico"]);
                                    } else {
                                        console.log(resp);
                                        swal({
                                            title: "Oops!",
                                            text: "Error en Guardado Vuelva a Intentarlo",
                                            type: "error"
                                        });
                                    }
                                },
                                error : function(xhr) {
                                    erroresenajax(xhr);
                                },
                            })
                        });


                }
            }


            $('#vencimiento').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $('#fecha_documento_caducidad').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $('#fecha_documento').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $('#fecha_ingreso_bodega').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $('#fecha').datetimepicker({

                widgetPositioning: {
                    horizontal: 'left'
                },
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'YYYY-MM-DD'
            });

            $("#fecha_documento").focusout(function () {
                var fechadelafactura = document.getElementById("fecha").value;
                swal({
                        title: "Esta seguro que desea cambiar la fecha de la Compra ?",
                        text: "Recuerde que la fecha no puede ser mayor a 30 días , tampoco ser una fecha futura.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, Cambiar Fecha!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({title: "Excelente!", text: "Fecha del Comprobante Cambiado: ", type: "success", timer: 4000});
                    })
            });


            $('#es_gasto_si').change(function (e) {
                $('#iva_credito_tributario_si').removeAttr("disabled");
                $('#iva_credito_tributario_no').removeAttr("disabled");
                $('#fecha_ingreso_bodega').attr("disabled","true");
                $('#bodega_compra').attr("disabled","true");

                $('#ingreso_mercaderia_si').attr("disabled","true");
                $('#ingreso_mercaderia_no').attr("disabled","true");
            });
            $('#es_gasto_no').change(function (e) {
                $('#iva_credito_tributario_si').attr("disabled","true");
                $('#iva_credito_tributario_no').attr("disabled","true");

                $('#fecha_ingreso_bodega').removeAttr("disabled");
                $('#bodega_compra').removeAttr("disabled");

                $('#ingreso_mercaderia_si').removeAttr("disabled");
                $('#ingreso_mercaderia_no').removeAttr("disabled");
            });

            $('#sustento_comprobante').change(function (e) {
                e.preventDefault();
                var elemento = $(this).find('option:selected');
                var myTag = elemento.attr("data-tipo");
                var opciones= myTag.split(',');


                $("#tipo_comprobante option").each(function (index, element) {
                    if($(this).val()=="vacio"){

                    }else{
                        $(this).css("display", "none");
                    }
                });

                $(opciones).each(function (index, element) {
                    if(element!=null){
                        $('#tipo_'+element).css("display", "");
                    }
                });
                $('#tipo_comprobante').val("vacio");


            });


        </script>
    </section>
    <!-- /.content -->

</div>

    @endsection
    @section('scripts')
        @include('invoices.partials._invoices_js')
    @endsection
