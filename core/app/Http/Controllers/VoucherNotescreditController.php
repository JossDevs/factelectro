<?php namespace App\Http\Controllers;
use App\Http\Requests\ClientFormRequest;
use App\Invoicer\Repositories\Contracts\ClientInterface as Client;
use App\Invoicer\Repositories\Contracts\InvoiceInterface as Invoice;
use App\Invoicer\Repositories\Contracts\EstimateInterface as Estimate;
use App\Invoicer\Repositories\Contracts\NumberSettingInterface as Number;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;
use Laracasts\Flash\Flash;
use Yajra\DataTables\DataTables;

class VoucherNotescreditController extends Controller {


    public function showNotes()
    {
        return view('voucherelectronic.notescredit.create');
    }
    public function showSee()
    {
        return view('voucherelectronic.notescredit.see_all');
    }
}

