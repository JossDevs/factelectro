<?php namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\UuidModel;
use Illuminate\Notifications\Notifiable;

class Transporter extends Model{
    use UuidModel;
    use Notifiable;
   
    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
  
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function estimates(){
        return $this->hasMany(Estimate::class, 'transporter_id');
    }
 
    public function scopeOrdered($query){
        return $query->orderBy('created_at', 'desc')->get();
    } 
}
