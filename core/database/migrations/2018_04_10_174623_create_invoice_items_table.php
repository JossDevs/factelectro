<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoiceItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_items', function(Blueprint $table)
		{
			$table->string('uuid', 36)->primary();
			$table->string('invoice_id', 36)->index('invoice_items_invoice_id_foreign');
			$table->string('item_name');
			$table->text('item_description', 65535);
			$table->float('quantity');
			$table->float('price', 15);
			$table->double('discount', 15, 8);
			$table->double('total_iva12', 15, 8);
			$table->string('code_ice');
			$table->double('porc_ice', 15, 8);
			$table->double('ice', 15, 8);
			$table->double('iva_type', 15, 8);
			$table->double('total', 15, 8);
			$table->double('subtotal', 15, 8);
			$table->string('tax_id', 36)->nullable()->index('invoice_items_tax_id_foreign');
			$table->integer('item_order');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_items');
	}

}
