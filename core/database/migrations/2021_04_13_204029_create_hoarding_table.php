<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHoardingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoarding', function (Blueprint $table) {
            $table->string('uuid', 36)->primary();  
            $table->string('client_id', 36)->index('hoarding_client_id_foreign');
            $table->string('branch_id', 36)->index('hoarding_branch_id_foreign');
            $table->string('company_id', 36)->index('hoarding_company_id_foreign');
            $table->string('user_id', 36)->index('hoarding_user_id_foreign');
            $table->string('establishment_id', 36)->index('hoarding_establishment_id_foreign');
            $table->string('emissionpoint_id', 36)->index('hoarding_emissionpoint_id_foreign');
            $table->string('user_id', 36)->index('hoarding_user_id_foreign');
            $table->string('seller_id', 36)->index('hoarding_seller_id_foreign');
            $table->string('transport_id', 36)->index('hoarding_transport_id_foreign');
            $table->string('number');
            $table->date('date');
            $table->string('environment');
            $table->double('discount', 15, 8);
            $table->double('total_ice', 15, 8);
            $table->double('total_renta', 15, 8);
            $table->double('total_isd', 15, 8);
            $table->double('total_hoarding', 15, 8);
            $table->double('total_totalvalue', 15, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoarding');
    }
}
