<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferralGuideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_guide', function (Blueprint $table) {
             $table->string('uuid', 36)->primary();  
            $table->string('client_id', 36)->index('referral_guide_client_id_foreign');
            $table->string('branch_id', 36)->index('referral_guide_branch_id_foreign');
            $table->string('company_id', 36)->index('referral_guide_company_id_foreign');
            $table->string('user_id', 36)->index('referral_guide_user_id_foreign');
            $table->string('establishment_id', 36)->index('referral_guide_establishment_id_foreign');
            $table->string('emissionpoint_id', 36)->index('referral_guide_emissionpoint_id_foreign');
            $table->string('user_id', 36)->index('referral_guide_user_id_foreign');
            $table->string('seller_id', 36)->index('referral_guide_seller_id_foreign');
            $table->string('transport_id', 36)->index('referral_guide_transport_id_foreign');
            $table->string('number');
            $table->date('date');
            $table->string('environment');
            $table->date('transport_init_date');
            $table->date('final_transport_date');
            $table->date('date_emision_support_document');
            $table->string('init_point');
            $table->string('destination_point');
            $table->string('reason');
            $table->string('route');
            $table->string('number_support_document');
            $table->string('establishment_support_document');
            $table->string('autorizacion_support_document');
            $table->string('customs_document');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_guide');
    }
}
