<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseSettlementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_settlements', function (Blueprint $table) {
            $table->string('uuid', 36)->primary();
            $table->string('client_id', 36)->index('creditnote_client_id_foreign');
            $table->string('branch_id', 36)->index('creditnote_branch_id_foreign');
            $table->string('company_id', 36)->index('creditnote_company_id_foreign');
            $table->string('establishment_id', 36)->index('creditnote_establishment_id_foreign');
            $table->string('emissionpoint_id', 36)->index('creditnote_emissionpoint_id_foreign');
            $table->string('user_id', 36)->index('creditnote_user_id_foreign');
            $table->string('seller_id', 36)->index('creditnote_seller_id_foreign');
            $table->date('date');
            $table->string('number');
            $table->string('warehouse_sell');
            $table->string('receipt_type');
            $table->date('document_date');
            $table->string('document_establishment_code');
            $table->string('code_point_issuance_document');
            $table->string('sequential_document');
            $table->string('description');
            $table->string('support_voucher');
            $table->string('autorizacion_sri');
            $table->string('merchandise_entry');
            $table->string('refund');
            $table->date('document_expiration_date');
            $table->date('winery_entry_date');
            $table->string('expiration');
            $table->double('vat_tax_credit', 15, 8);          
            $table->double('subtotal_withouttax', 15, 8);
            $table->double('subtotal_tax12', 15, 8);
            $table->double('subtotal_cerotax', 15, 8);
            $table->double('subtotal_exempttax', 15, 8);
            $table->double('subtotal_noobject', 15, 8);
            $table->double('total_ice', 15, 8);
            $table->double('total_iva12', 15, 8);
            $table->double('total_irbpnr', 15, 8);
            $table->double('total_propine', 15, 8);
            $table->double('total_totalvalue', 15, 8);
            $table->double('total_refund', 15, 8);       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_settlements');
    }
}
